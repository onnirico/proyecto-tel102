SEMINARIO DE PROGRAMACIÓN [TEL102] 2023 - 02

Profesor Patricio Olivares

Ayudantes Ignacio Araya y Pablo Sánchez

Paralelo 201

Fecha de entrega: 21 de noviembre de 2023 [3er Hito]


“Conversor de Unidades y Calculadora de Energías” - Grupo Nº 7


Integrantes: Eduardo Palma ROL: 202130512 - 2

Matías Bahamondes ROL: ¿? (Integrante ausente, no responde) 

Alexia Leal ROL: ¿? (Integrante ausente, no responde)

Gabriel Lara ROL: 202330510 - 3


I. INTRODUCCIÓN


Para poder solucionar problemas de conversión de unidades con una mayor facilidad para los estudiantes que requieran hacer uso de un conversor de unidades, junto con el cálculo de energías físicas como lo pueden ser los de energía mecánica, se desarrolla el siguiente programa. Se espera que este permita al usuario realizar conversiones de unidades en una cantidad de opciones limitadas dispuestas en un submenú y como calculadora física a través de la solicitud de datos por una ventana según la unidad de trabajo.


II. OBJETIVOS


[gral.] La finalidad del programa realizado es facilitar al usuario la conversión de diversos tipos de datos a su elección conjunto a integrar una calculadora física que permita determinar energía cinética, elástica, potencial gravitacional y mecánica, junto con un formulario de energias, de un sistema por medio de la solicitud de datos al usuario por ventanas (I/O).


Para llevar a cabo lo anterior, se establecen tres objetivos específicos [esp.] que irán a la par con las entregas de los hitos. Por tanto:


[1era Entrega] Creación de un menú principal de acceso para determinar si desea ingresar al convertidor de unidades o a la calculadora física, donde solo se encontrará accesible la primera. En caso de seleccionarse esta opción, se podrá acceder a un menú con ocho sub - menús disponibles, siendo: longitud, área, volumen, temperatura, velocidad, tiempo, presión y energía. Cada uno de estos estará disponible para su acceso y a través de la solicitud de datos por terminal poder retornar la conversión correspondiente a la selección. Asimismo, el programa una vez realizada la conversión, preguntará si desea realizar otra, en caso de ingresar “S” o “s”, volverá al menú principal para poder seleccionar otra conversión nuevamente. En caso de que desee volver al menú principal de elección, puede realizarlo. Análogamente, si seleccionase la calculadora física, aparecerá un mensaje de error y le pedirá que reintente.


[2da Entrega] Adicional a la primera entrega, se integrará el desarrollo de una calculadora de energía mecánica, considerando: cinética, potencial y gravitacional, habilitado en la selección del menú principal, posibilitando la elección de una de las tres dispuestas en el programa para, según los datos que ingrese el usuario, poder realizar el cálculo directo de la energía implicada. Al entregar el resultado, le preguntará al usuario si desea realizar otro cálculo en la calculadora, volviendo al menú anterior y posibilitando además, regresar al menú principal. Sin embargo, también se desarollará la base gráfica del programa en QT, utilizando las bibliotecas correspondientes en C++, acá tendremos habilitados una serie de submenús entre los cuales se podrá navegar libremente, pudiendo cerrar las ventanas generadas siempre que el usuario lo desee. Es importante destacar que para esta entrega, como es la base del código desarrollado en QT, solamente se tendr´ñan habilitados algunos botones para poder realizar las conversiones a través de los widgets, estando disponibles en el Conversor de Unidades los botones para las conversiones de longitud, área y volúmenes. Se reitera que el código por terminal se encuentra completo y funcional según lo deseado en la primera entrega, con todas las funcionalidades implementadas. Se esperará poder añadir las demás funciones de los botones del programa antes de la entrega del 3er Hito.


[3era Entrega] Para la última entrega de nuestro proyecto, se tendrá disponible de forma completa el programa con las herramientas de QT, entregandole una interfaz gráfica al usuario donde se le presentarán ambas opciones de submenús en froma de botón para poder acceder al menú de conversiones o físico, accediendo a sus respectivos submenús que los guiarán por el programa para poder realizar las conversiones por ventanas de dialogo con el usuario. Además de esto, las ventanas podrán cerrarse con un botón dispuesto en cada una para volver atrás, o en su defecto cerrar el programa. Es importante mencionar que en el submenú de energías se deberá ingresar los valores en las unidades correspondientes como se indíca en cada una de las ventanas. El programa tendrá corregido los diálogos de cada ventana sin errores de tipeo y además, se vereficarán las conversiones específicas usadas para cada transformación y que entreguen el resultado correcto de la transformación o del cálculo. La ventanas se crean con la herramienta de QT Creator.


Para el desarrollo de los objetivos específicos planteados para cada una de las entregas dispuestas, se establecerán las actividades a realizar para cada estudiante. Que se encuentran disponibles en GitLab, pestaña Issues. Tendrán, al menos, tres tareas asignadas que serán comentadas con la resolución de cada una e indicando el archivo subido/actualizado relacionado a su asignación. Se encontrará disponible además un diagrama de componentes en la carpeta de la primera entrega dispuesta en GitLab.

El desarrollo de los objetivos especifico tendrá entonces la asignación de roles para cada una de las entregas que serán:

[1] Eduardo Palma: [1era. entrega]: Desarrollo completo de las implementaciones generales del primer submenú del programa, junto con respositorio y vídeo. [2da. entrega]: Integración de diagrama UML, subida del código de QT, realización de vídeo. [3era. entrega]: Finalización de programa con QT Creator, realización de vídeo y corrección general de los archivos de README.md y changelog.md.

[2] Gabriel Lara: [1era. entrega]: estudiante ausente [2da. entrega]: corrección de errores en el código fuente del programa (por terminal) y habilitación de ambos submenús (de conversiones y de energías).  [3era. entrega]: modularización de los códigos del programa por terminal y subida de src_directory

[3] Alexia Leal: Estudiante ausente (todas las entregas)

[4] Matias Bahamondes: Estudiante ausente (todas las entregas)

La finalidad del programa desarrollado es combinar dos herramientas útiles aplicables en el ámbito académico en Física Mecánica, Química y Matemática para cualquier estudiante que requiera hacer uso de un programa con las funcionalidades disponibles.


III. REPOSITORIO


Con el respoitorio finalizado, en 'Tercer Hito' se encontrarán dos directorios, el primero con los archivos del programa en QT Creator y el segundo con los códigos modulados del programa compilable y ejecutable por terminal, donde además estará dispuesto el diagrama UML del programa. Es importante destacar que según sugerencias de las entregas pasadas se reducen el número de directorios en la última entrega para así no tener problemas generales en compilación y/o ejecución del código fuente por terminal.


El repositorio se encuentra disponible en: [https://gitlab.com/onnirico/proyecto-tel102.git]


IV. REQUISITOS DE COMPILACIÓN


Para compilar el programa es importante recordar que el archivo sourceCode.cpp y los archivos de cabecera como calculator.h, conversions.h, style_sheet.h y submenus.h deberán de estar en el mismo directorio al ser los constructores del programa, o de lo contrarió ocurrirá un error en la compilación.


Se deberá ingresar desde terminal a la carpeta donde se encuentran los archivos del repositorio, previamente clonado, con el comando:

cd /ruta/hacia/el/directorio


Luego, cuando estemos en la carpeta, utilizaremos:


g++ sourceCode.cpp -o ConversorCalculadora


Donde ConversorCalculadora será el nombre del archivo ejecutable resultante, que una vez compilado podremos abrir con:


./ ConversorCalculadora


V. INSALACIÓN


Como compilar y ejecutar el programa creado con la herramienta de QT Creator se deberán seguir una serie de pasos para que aparezcan las ventanas de dialogo con el usuario:

1. Descargar el repositorio. 

2. Descargar el programa QT Creator (en caso de no tenerlo). 

3. Dirigirse al directorio donde se encuentre el codigo.

4. Seguido a esto, abrir QT y ejecutar el código para que se vizualice.

5. Seguir las indicaciones del programa, las cuales son: 

	5.1) Seleccionar el menú al cual desea acceder (transformador de unidades o calculadora física)

	5.2) Seleccionar el submenú de la elección anterior que desea realizar.

	5.3) Digitar los datos solicitados en la pantalla, para realizar la transformación o cálculo correspondiente.

	5.4) Ya puede visualizar en pantalla la transformación correctamente.

	5.5) Para poder cambiar de módulo, debe de cerrar la ventana actual del programa con los botones 'Volver atrás' dispuesta en cada uno de los dialogos con el usuario.

	5.6) Una vez ya realizadas todas las transformaciones que necesite, cierre QT para finalizar el uso del programa o seleccione 'Cerrar programa' en la primera ventana del programa.


VI. TRACKLIST

El TrackList de nuestro proyecto se organizará según las asignaciones correspondientes a cada uno de los estudiantes individualizados anteriormente.

Disponible en: [https://gitlab.com/onnirico/proyecto-tel102/-/issues]


VII. REFERENCIAS


· Link Vídeo Expositivo [Hito 1]: DISPONIBLE EN AULA (FORO HITO)

· Link Vídeo Expositivo [Hito 2]: DISPONIBLE EN AULA (FORO HITO)

· Link Vídeo Expositivo [Hito 3]: DSIPONIBLE EN AULA (FORO HITO) 

· Brian Kernighan, Dennis Ritchie. "The C programming lenguage" (1978). Prentice Hall, ISBN 9780131101630.

· The Qt Company. (2023). Signals & Slots. Qt Documentation. https://doc.qt.io/qt-6/signalsandslots.html

· Usuario: QtLearner. (2017). How to connect signal and slot in different classes in Qt?. Stack Overflow. https://stackoverflow.com/questions/48095653/how-to-connect-signal-and-slot-in-different-classes-in-qt

· Canal Programación ATS (Recuperado el 9 de octubre de 2022), Lista de reproducción Programación en C++ || Programación ATS. Youtube (https://www.youtube.com/watch?v=dJzLmjSJc2c&list=PLWtYZ2ejMVJlUu1rEHLC0i_oibctkl0Vh)
