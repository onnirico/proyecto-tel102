## Version 1.2 - “Conversor de Unidades y Calculadora de Energías”


SEMINARIO DE PROGRAMACIÓN [TEL102] 2023 - 02

Profesor Patricio Olivares

Ayudantes Ignacio Araya y Pablo Sánchez

Paralelo 201

Fecha: 21 de noviembre de 2023


Integrantes: Eduardo Palma ROL: 202130512 - 2

Matías Bahamondes ROL: ¿? (Estudiante ausente, no responde) 

Alexia Leal ROL: ¿? (Estudiante ausente, no responde)

Gabriel Lara ROL: 202330510 - 3


Para la entrega de la tercera y útlima versión de nuestro conversar de unidades y calculadora de energías físicas tendremos disponibles las funcionalidades descritas a continuación:


Funcionalidades básicas


[1] Habilitación de un menú principal de elección al usuario: Se podrá ingresar en el submenú de conversar de unidades en el ejecutable por terminal. Ahora también se podrá acceder a la calculadora física desde la terminal.

[2] Habilitación de menú de conversiones: Se dispondrán ocho opciones diferentes para seleccionar por el usuario según el tipo de conversión que desea realizar.

[3] Factores de conversión: Se corrigen, calculan y revisan que los factores de conversión en cada una de las funciones disponibles se encuentren correctos.

[4] Habilitación de menú de calculos físicos: Se podrá acceder desde terminal por el usuario al menú físico y realizar todas las conversiones correspondientes.

[5] Interfáz gráfica: se tendrá una interfaz gráfica para el usuario con botones, donde podrá acceder a todas las funciones del programa e interactuar abriendo nuevas o cerrando en la que se encuentra.

[6] Se dispondrán ambos mneús completamente habilitados para el programa por terminal así como el creado con le herramienta de QT Creator, con una interacción entre ventanas de diálogo con el usuario (I/O) para los datos de sus conversions y/o cálculos físicos.

[7] Agregación de formularios de energías en submenú físico

[8] Disponibilidad completa de opciones terminal y ventana de dialogos.

Mejoras en la Interfaz de usuario


[1] Se utilizará una línea de caracteres debajo de la petición de ingreso de datos al usuario para representar un “cambio de pantalla”.

[2] Mantención y revisión de los datos entregados por pantalla en el mismo formato en todas las funciones.

[3] Utilización de textos de colores en programa compilado por terminal (lila).

[4] Intefraz gráfica completamente funcional para todos los botones dispuestos en las ventanas de dialogo de QT Creator.

[5] Redefinición de interfaz gráfica con mejor integración de los elementos dispuesto junto con los cuadros de diálogo.


Correcciones de errores

[1] Corrección de errores de transformación por factor de conversión

[2] Corrección de error de transformación en las conversiones de tiempo. Estimaciones mal realizadas para ciertas conversiones.

[3] Correción de errores en transformaciones de energía.

[4] Corrección de problemas con presentaciones de ventanas de diálogo según seleccion de usuario.


Próximas actualizaciones


Como el programa se encuentra completamente funcional para ambos códigos, de terminal y de ventana de dialogos de QT, no habrán próximas actualizaciones según lo abarcado en el proyecto y las implementaciones realizadas a lo largo de las tres entregas. Sin embargo, también se realizará la presentación del último hito el 21 de noviembre.

¡Gracias por utilizar el Conversar de unidades y Calculadora Física! Esperamos que el programa pueda ayudarte.
