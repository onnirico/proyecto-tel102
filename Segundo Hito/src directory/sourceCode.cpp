#include "functions.h" /*Incluiremos el archivo de cadecera donde definimos las funciones incluídas en el código del main.cpp*/

int main(int argc, char* argv[]){

    int optionMain;

    /* Main windows */

    while (true){
        
        cout << "\033[1;35m¡Bienvenido al Conversor de Unidades y Calucladora de Energías Físicas!\033[0m" << endl;
        cout << "Por favor selecciona una opción a continuación:\n" << endl;
 
        cout << "\t[0] Salir del programa" << endl;
        cout << "\t[1] Conversor de Unidades" << endl; /*Entrega Hito Nº1 [✓]*/
        cout << "\t[2] Calculadora de Energía Física\n" << endl; /*Entrega Hito Nº2 [✓]*/
        

        cout << "Ingrese su Selección: ";
        cin >> optionMain;
        mostrarRecuadro(30, 1); 

        switch (optionMain){

            case 0:
                cout << "\n¡Gracias por utilizar el programa!" << endl;
                break;

            case 1:
                cout << "\n... Iniciando \033[1;35mConversor de Unidades\033[0m ...\n" << endl;
                menuConversion();
                break;
            
            case 2:
                cout << "\n... Iniciando \033[1;35mCalucladora de Energía Física\033[0m ...\n" << endl;
                menuFisico(); 
                
                break;

            default:
                cout << "\nOpción no válida. Intente de nuevo.\n" << endl;
                break;
            }

        if (optionMain == 0){ /*Salida completa del programa*/
            return 0;

        }
    }
}