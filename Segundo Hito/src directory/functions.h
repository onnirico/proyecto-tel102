#include <iostream>
#include <math.h> /*Necesaria para las ecuaciones matemáticas*/
#include <string> /*Necesaria para las variables*/
#include <cmath> /*Necesaria para los cálculos físicos*/

using namespace std; /*Omitir n en cada cout*/

/*Global Variables*/

int Conversion;

/*Output Design*/

void mostrarRecuadro(int ancho, int alto){ /*Utilizado para ordenar el código :)*/
    for (int i = 0; i < alto; ++i) {
        for (int j = 0; j < ancho; ++j){
            if (i == 0 || i == alto - 1){
                if (j == 0 || j == ancho - 1){
                    cout << '+';
                } else {
                    cout << '-';
                }
            } else{
                if (j == 0 || j == ancho - 1){
                    cout << '|';
                } else {
                    cout << ' ';
                }
            }
        }
        cout << endl;
    }
}

/*Continue Function*/

char askContinue(){
    char continuar; /*Creación del caracter para que se pueda ingresar como "S" o "s"*/
    
    cout << "¿Desea realizar otra conversión? (s/n): "; /*Consulta al usuario. En caso de decir no, el programa volverá a la ventana ppal.*/
    cin >> continuar;
    mostrarRecuadro(30, 1); /*Cada mostrarRecuadro() que se encuentra en el código estará ubicado después del ingreso de datos por usuario.*/

    return continuar;
}

/*Crearemos otras consultas con diferente texto para los otros casos*/

char askContinueFisico(){
    char continuar; /*Creación del caracter para que se pueda ingresar como "S" o "s"*/
    
    cout << "¿Desea realizar otro cálculo? (s/n): "; /*Consulta al usuario. En caso de decir no, el programa volverá a la ventana ppal.*/
    cin >> continuar;
    mostrarRecuadro(30, 1); /*Cada mostrarRecuadro() que se encuentra en el código estará ubicado después del ingreso de datos por usuario.*/

    return continuar;
}

char askContinueForm(){
    char continuar; /*Creación del caracter para que se pueda ingresar como "S" o "s"*/
    
    cout << "¿Desea seguir en el menú físico? (s/n): "; /*Consulta al usuario. En caso de decir no, el programa volverá a la ventana ppal.*/
    cin >> continuar;
    mostrarRecuadro(30, 1); /*Cada mostrarRecuadro() que se encuentra en el código estará ubicado después del ingreso de datos por usuario.*/

    return continuar;
}

/*Length Functions.*/

/*Todas las funciones del código funcionarán bajo la misma lógica de interacción entre ventanas. Asimismo, las declaraciones
de sus funciones se realizarán acorde a las transformaciones, con un valor asociado que será el "coeficiente de trnasformación"
para cada uno de los casos. Los nombres de las funciones indican qué se está transformando.*/

double metersToFoot(double Valor){
    const double footPerMeter = 3.28084;
    return Valor * footPerMeter;
}

double footToMeters(double Valor){
    const double metersPerFoot = 0.3048;
    return Valor * metersPerFoot;
}

double kilometersToMiles(double Valor){
    const double milesPerKilometer = 0.621371;
    return Valor * milesPerKilometer;
}

double milesToKilometers(double Valor){
    const double kilometersPerMiles = 1.60934;
    return Valor * kilometersPerMiles;
}

double centimeterToInches(double Valor){
    const double inchesPerCentimeters = 0.393701;
    return Valor * inchesPerCentimeters;
}

/*Length Submenu*/

void submenuLength(){
    char continuar; /*Se creará esta variable char para poder ingresar la respuesta a la consulta del realizar otra conversión-*/

    do{
        int Opcion;
        double Valor;

        cout << "\nConversor de longitud\n" << endl;
        cout << "\t[0] Volver al menú principal" << endl;
        cout << "\t[1] Metros [m] a pies [ft]" << endl;
        cout << "\t[2] Pies [ft] a metros [m]" << endl;
        cout << "\t[3] Kilometros [km] a millas [mi]" << endl;
        cout << "\t[4] Millas [mi] a kilometros [km]" << endl;
        cout << "\t[5] Centimetros [cm] a pulgadas [in]\n" << endl;

        cout << "\nIngrese su Selección: ";
        cin >> Opcion;
        mostrarRecuadro(30, 1); 

        switch (Opcion){
            case 0:
                cout << "...Volviendo al \033[1;35mmenú principal\033[0m ...\n" << endl;
                break;

            case 1:
                cout << "\n\nIngrese metros [m]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m metros [m] serán \033[0m"
                << metersToFoot(Valor) << "\033[1;35m pies [ft].\033[0m" << endl;
                break;

            case 2:
                cout << "\n\nIngrese pies [ft]:" << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pies [ft] serán \033[0m"
                << footToMeters(Valor) << "\033[1;35m metros [m].\033[0m" << endl;
                break;

            case 3:
                cout << "\n\nIngrese Kilometros [km]:" << endl;
                cin >> Valor;
                cout << "\033[1;35m\n[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m kilometros [km] serán \033[0m"
                << kilometersToMiles(Valor) << "\033[1;35m millas [mi].\033[0m";
                break;

            case 4:
                cout << "\n\nIngrese Millas [mi]:" << endl;
                cin >> Valor;
                cout << "\033[1;35m\n[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m millas [mi] serán \033[0m"
                << milesToKilometers(Valor) << "\033[1;35m kilometros [km].\033[0m";
                break;

            case 5:
                cout << "\n\nIngrese Centimetros [cm]:" << endl;
                cin >> Valor;
                cout << "\033[1;35m\n[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m centimetros [cm] serán \033[0m"
                << milesToKilometers(Valor) << "\033[1;35m pulgadas [in].\033[0m";
                break;

            default:
                cout << "\n\nOpción no valida. Por favor ingrese una opción válida." << endl;
                break;
        }

        if (Opcion == 0){ /*Cuando la opción seleccionada sea cero, se volverá a la mainWindows*/
            return;
        }

        continuar = askContinue(); /*A continuar le entregaremos el valor retornado por la funcion askContinue()*/
    
    } while (continuar == 's' || continuar == 'S');
}

/*Area Functions*/

double squareMetersToSquarefoot(double Valor){
    const double squarefootPerSquareMeter = 10.7639;
    return Valor * squarefootPerSquareMeter;
}

double squarefootToSquareMeters(double Valor){
    const double squareMetersPerSquareFoot = 0.092903;
    return Valor * squareMetersPerSquareFoot;
}

double squareKilometersToAcre(double Valor){
    const double acresPerSquareKilometer = 247.105;
    return Valor * acresPerSquareKilometer;
}

double acreToHectare(double Valor){
    const double hectarePerAcre = 0.404686;
    return Valor * hectarePerAcre;
}

double squareMilesToSquareKilometers(double Valor) {
    const double squareKilometersPerSquareMile = 2.58999;
    return Valor * squareKilometersPerSquareMile;
}

double squareInchesToSquareCentimeters(double Valor) {
    const double squareCentimetersPerSquareInch = 6.4516;
    return Valor * squareCentimetersPerSquareInch;
}

double squareYardsToSquareMeters(double Valor) {
    const double squareMetersPerSquareYard = 0.836127;
    return Valor * squareMetersPerSquareYard;
}

/*Area Submenu*/

void submenuArea(){
    char continuar;

    do{
        int Opcion;
        double Valor;

        cout << "\nConversor de área\n" << endl;
        cout << "\t[0] Volver al menú principal" << endl;
        cout << "\t[1] Metros cuadrados [m²] a pies cuadrados [ft²]" << endl;
        cout << "\t[2] Pies cuadrados [ft²] a metros cuadrados [m²]" << endl;
        cout << "\t[3] Kilometros cuadrados [km²] a Acre [ac]" << endl;
        cout << "\t[4] Acres [ac] a hectáreas [ha]" << endl;
        cout << "\t[5] Millas cuadradas [mi²] a Kilometros cuadrados [km²]" << endl;
        cout << "\t[6] Pulgadas cuadradas [in²] a Centímetros cuadrados [cm²]" << endl;
        cout << "\t[7] Yardas cuadradas [yd²] a Metros cuadrados [m²]\n" << endl;

        cout << "\nIngrese su Selección: ";
        cin >> Opcion;

        mostrarRecuadro(30, 1); 

        switch (Opcion){
            case 0:
                cout << "...Volviendo al \033[1;35mmenú principal\033[0m ...\n" << endl;
                break;

            case 1:
                cout << "\n\nIngrese metros cuadrados [m²]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m metros cuadrados[m²] serán \033[0m"
                << squareMetersToSquarefoot(Valor) << "\033[1;35m pies cuadrados [ft²].\033[0m" << endl;
                break;

            case 2:
                cout << "\n\nIngrese pies cuadrados [ft²]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pies cuadrados [ft²] serán \033[0m"
                << squarefootToSquareMeters(Valor) << "\033[1;35m metros cuadrados [m²].\033[0m" << endl;
                break;

            case 3:
                cout << "\n\nIngrese Kilometros cuadrados [km²]: " << endl;
                cin >> Valor;
                cout << "\033[1;35m\n[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m kilometros cuadrados [km²] serán \033[0m"
                << squareKilometersToAcre(Valor) << "\033[1;35m acre [ac].\033[0m";
                break;

            case 4:
                cout << "\n\nIngrese Acre [ac]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m acre [ac] serán \033[0m"
                << acreToHectare(Valor) << "\033[1;35m hectareas [ha].\033[0m";
                break;

            case 5:
                cout << "\n\nIngrese millas cuadradas [mi²]:" << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m millas cuadradas [mi²] serán \033[0m"
                << squareMilesToSquareKilometers(Valor) << "\033[1;35m kilometros cuadrados [km²]].\033[0m";
                break;

            case 6:
                cout << "\n\nIngrese pulgadas cuadradas [in²]:" << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pulgadas cuadradas [in²] serán \033[0m"
                << squareInchesToSquareCentimeters(Valor) << "\033[1;35m centimetros cuadrados [cm²]].\033[0m";
                break;

            case 7:
                cout << "\n\nIngrese yardas cuadradas [yd²]:" << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m yardas cuadradas [yd²] serán \033[0m"
                << squareYardsToSquareMeters(Valor) << "\033[1;35m metros cuadrados [m²]].\033[0m";
                break;

            default:
                cout << "\n\nOpción no valida. Por favor ingrese una opción válida." << endl;
                break;
        }

        if (Opcion == 0){
            return;
        }

        continuar = askContinue();

    } while (continuar == 's' || continuar == 'S');
}

/*Volume Functions*/

double literToMililiter(double Valor){
    const double mililitrosPorLitro = 1000.0;
    return Valor * mililitrosPorLitro;
}

double mililiterToLiter(double Valor){
    const double litersPerMililiter = 0.001;
    return Valor * litersPerMililiter;
}

double gallonsToLiters(double Valor){
    const double litersPerGallon = 3.78541;
    return Valor * litersPerGallon;
}

double litersToGallons(double Valor){
    const double gallonPerLitters = 0.264172;
    return Valor * gallonPerLitters;
}

double cubicFootToCubicMeter(double Valor){
    const double cubicMeterPerCubicFoot = 0.0283168;
    return Valor * cubicMeterPerCubicFoot;
}

double cubicMeterToCubicFoot(double Valor){
    const double cubicFootPerCubicMeter = 35.3147;
    return Valor * cubicFootPerCubicMeter;
}

double mililiterToOunce(double Valor){
    const double ouncesPerMililiter = 29.5735;
    return Valor * ouncesPerMililiter;
}

double ounceToMililiter(double Valor){
    const double mililitersPerOunce = 29.5735;
    return Valor * mililitersPerOunce;
}

/*Volume Submenu*/

void submenuVolume(){
    char continuar;

    do{
        int Opcion;
        double Valor;

        cout << "\nConversor de Volumenes\n" << endl;
        cout << "\t[0] Volver al menú principal" << endl;
        cout << "\t[1] Litros [L] a mililitros [mL]" << endl;
        cout << "\t[2] Mililitros [L] a litros [mL]" << endl;
        cout << "\t[3] Galones [gal] a litros [L]" << endl;
        cout << "\t[4] Litros [L] a galones [gal]" << endl;
        cout << "\t[5] Pies cúbicos [ft³] a metros cúbicos [m³]" << endl;
        cout << "\t[6] Metro cúbico [m³] a pie cúbico [ft³]" << endl;
        cout << "\t[7] Mililitro [mL] a Onzas fluidas [fl oz]" << endl;
        cout << "\t[8] Onzas fluidas [fl oz] a mililitros [mL]\n" << endl;

        cout << "\nIngrese su Selección: ";
        cin >> Opcion;
        mostrarRecuadro(30, 1); 

        switch (Opcion) {
            case 0:
                cout << "...Volviendo al \033[1;35mmenú principal\033[0m ...\n" << endl;
                break;

            case 1:
                cout << "\n\nIngrese Litros [L]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m litros [L] serán \033[0m"
                << literToMililiter(Valor) << "\033[1;35m mililitros [mL].\033[0m" << endl;
                break;

            case 2:
                cout << "\n\nIngrese mililitros [mL]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m mililitros [mL] serán \033[0m"
                << mililiterToLiter(Valor) << "\033[1;35m litros [L].\033[0m" << endl;
                break;

            case 3:
                cout << "\n\nIngrese galones [gal]: "<< endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m galones [gal] serán \033[0m"
                << gallonsToLiters(Valor) << "\033[1;35m litros [L].\033[0m";
                break;

            case 4:
                cout << "\n\nIngrese litros [L]: "<< endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m litros [L] serán \033[0m"
                << litersToGallons(Valor) << "\033[1;35m galones [gal].\033[0m";
                break;

            case 5:
                cout << "\n\nIngrese pies cúbicos [ft³]: "<< endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pies cúbicos [ft³] serán \033[0m"
                << cubicFootToCubicMeter(Valor) << "\033[1;35m metros cúbicos[m³].\033[0m";
                break;

            case 6:
                cout << "\n\nIngrese metro cúbico [m³]:"<< endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m metro cúbico [m³] serán \033[0m"
                << cubicMeterToCubicFoot(Valor) << "\033[1;35m pie cúbico [ft³]].\033[0m";
                break;

            case 7:
                cout << "\n\nIngrese mililitro[mL]: "<< endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m mililitro [mL] serán \033[0m"
                << mililiterToOunce(Valor) << "\033[1;35m onzas fluidas [fl oz].\033[0m";
                break;

            case 8:
                cout << "\n\nIngrese onzas fluidas [fl oz]: "<< endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m onzas fluidas [fl oz] serán \033[0m"
                << ounceToMililiter(Valor) << "\033[1;35m mililitros [mL].\033[0m";
                break;

            default:
                cout << "\n\nOpción no valida. Por favor ingrese una opción válida." << endl;
                break;
        }

        if (Opcion == 0){
            return;
        }

        continuar = askContinue();

    } while (continuar == 's' || continuar == 'S');
}

/*Tempreature Functions*/

double celsiusToFahrenheit(double Valor){
    return (Valor * 9.0 / 5.0) + 32.0;
}

double fahrenheitToCelcius(double Valor) {
    return (Valor - 32.0) * 5.0 / 9.0;
}

double celsiusToKelvin(double Valor) {
    return Valor + 273.15;
}

double kelvinToCelcius(double Valor){
    return Valor - 273.15;
}

double kelvinToFahrenheit(double Valor){
    return (Valor - 273.15) * 9/5 + 32;
}

double fahrenheitToKelvin(double Valor){
    return (Valor - 32) * 5/9 + 273.15;

}

void submenuTemperature(){
    char continuar;

    do{
        int Opcion;
        double Valor;

        cout << "\nConversor de Temperatura\n" << endl;
        cout << "\t[0] Volver al menú principal" << endl;
        cout << "\t[1] Celsius [Cº] a Fahrenheit [F]" << endl;
        cout << "\t[2] Fahrenheit [F] a Celsius [Cº]" << endl;
        cout << "\t[3] Celsius [C] a Kelvin [K]" << endl;
        cout << "\t[4] Kelvin [K] a Celsius [Cº]" << endl;
        cout << "\t[5] Fahrenheit [F] a Kelvin [K]" << endl;
        cout << "\t[6] Kelvin [K] a Fahrenheit [F]\n" << endl;

        cout << "\nIngrese su Selección: ";
        cin >> Opcion;
        mostrarRecuadro(30, 1); 

        switch(Opcion){
            case 0:
                cout << "...Volviendo al \033[1;35mmenú principal\033[0m ...\n" << endl;
                break;

            case 1:
                cout << "\n\nIngrese Tº en Celsius [Cº]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m celsius [Cº] serán \033[0m" 
                << celsiusToFahrenheit(Valor) << "\033[1;35m Fahrenheit [F].\033[0m" << endl;
            break;

            case 2:
                cout << "\n\nIngrese Tº en Fahrenheit [F]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m fahrenheit [Fº] serán \033[0m" 
                << fahrenheitToCelcius(Valor) << "\033[1;35m Celsius [Cº].\033[0m" << endl;
            break;

            case 3:
                cout << "\n\nIngrese Tº en Celsius [Cº]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m celsius [Cº] serán \033[0m" 
                << celsiusToKelvin(Valor) << "\033[1;35m Kelvin [K].\033[0m" << endl;
            break;

            case 4:
                cout << "\n\nIngrese Tº en Kelvin [K]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m Klevin [K] serán \033[0m" 
                << kelvinToCelcius(Valor) << "\033[1;35m Celsius [Cº].\033[0m" << endl;
            break;

            case 5:
                cout << "\n\nIngrese Tº en Fahrenheit [F]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m fahrenheit [F] serán \033[0m" 
                << fahrenheitToKelvin(Valor) << "\033[1;35m Kelvin [K].\033[0m" << endl;
            break;

            case 6:
                cout << "\n\nIngrese Tº en Fahrenheit [F]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m celsius [Cº] serán \033[0m" 
                << fahrenheitToCelcius(Valor) << "\033[1;35m Celsius [Cº].\033[0m" << endl;
            break;

            default:
                cout << "\n\nOpción no valida. Por favor ingrese una opción válida." << endl;
            break;
        
        }

        if (Opcion == 0){
            return;
        }

        continuar = askContinue();

    } while (continuar == 's' || continuar == 'S');
}

/*Velocity Functions*/

double kilometerPerHourToMeterPerSecond(double Valor){
    return Valor * 1000 / 3600;
}

double meterPerSecondToKilometermPerHour(double Valor){
    return Valor * 3600 / 1000;
}

double kilometerPerHourToMilesPerHour(double Valor){
    return Valor / 1.60934;
}

double milesPerHourToKilometerPerHour(double Valor){
    return Valor * 1.60934;
}

double kilometerPerHourToKnots(double Valor){
    return Valor / 1.852;
}

double knotsToKilometerPerHour(double Valor){
    return Valor * 1.852;
}

double meterPerSecondToFeetPerSecond(double velocity) {
    return velocity * 3.28084;
}

double feetPerSecondToMeterPerSecond(double velocity) {
    return velocity * 0.3048;
}

void submenuVelocity(){
    char continuar;

    do{
        int Opcion;
        double Valor;

        cout << "\nConversor de velocidades\n" << endl;
        cout << "\t[0] Volver al menú principal" << endl;
        cout << "\t[1] Kilómetros por hora [km/h] a Metros por segundo [m/s]" << endl;
        cout << "\t[2] Metros por segundo [m/s] a Kilómetros por hora [km/h]" << endl;
        cout << "\t[3] Kilómetros por hora [km/h] a Millas por hora [mph]" << endl;
        cout << "\t[4] Millas por hora [mph] a Kilómetros por hora [km/h]" << endl;
        cout << "\t[5] Kilómetros por hora [km/h] a Nudos [knots]" << endl;
        cout << "\t[6] Nudos [knots] a Kilmetros por hora [km/h]" << endl;
        cout << "\t[7] Metros por segundo [m/s] a Pies por segundo [ft/s]" << endl;
        cout << "\t[8] Pies por segundo [ft/s] a Metros por segundo [m/s]\n" << endl;

        cout << "\nIngrese su Selección: ";
        cin >> Opcion;
        mostrarRecuadro(30, 1); 

        switch(Opcion){
            case 1:
                cout << "\n\nIngrese Velocidad en Kilometro por hora [km/h]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m kilometros por hora [km/h] serán \033[0m" 
                << kilometerPerHourToKnots(Valor) << "\033[1;35m metros por segundo [m/s].\033[0m" << endl;
            break;

            case 2:
                cout << "\n\nIngrese Velocidad en metro por segundo [m/s]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m metros por segundo [m/s] serán \033[0m" 
                << meterPerSecondToKilometermPerHour(Valor) << "\033[1;35m kilometros por hora [km/h].\033[0m" << endl;
            break;


            case 3:
                cout << "\n\nIngrese Velocidad en Kilometro por hora [km/h]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m kilometros por hora [km/h] serán \033[0m" 
                << kilometerPerHourToMilesPerHour(Valor) << "\033[1;35m milla por hora [mph].\033[0m" << endl;
            break;

            case 4:
                cout << "\n\nIngrese Velocidad en millas por hora [mph]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m millas por hora [mph] serán \033[0m" 
                << milesPerHourToKilometerPerHour(Valor) << "\033[1;35m kilometros por hora [km/h].\033[0m" << endl;
            break;

            case 5:
                cout << "\n\nIngrese Velocidad en kilometros por hora [km/h]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m kilometros por hora [km/h] serán \033[0m" 
                << kilometerPerHourToKnots(Valor) << "\033[1;35m nudos [knot].\033[0m" << endl;
            break;

            case 6:
                cout << "\n\nIngrese Velocidad en nudos [knot]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m nudos [knot] serán \033[0m" 
                << knotsToKilometerPerHour(Valor) << "\033[1;35m kilometros por hora [km/h].\033[0m" << endl;
            break;

            case 7:
                cout << "\n\nIngrese Velocidad en metros por segundos [m/s]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m metros por segundos [m/s] serán \033[0m" 
                << meterPerSecondToFeetPerSecond(Valor) << "\033[1;35m pie por segundos [ft/s].\033[0m" << endl;
            break;

            case 8:
                cout << "\n\nIngrese Velocidad pie por segundos [ft/s]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pie por segundos [ft/s] serán \033[0m" 
                << feetPerSecondToMeterPerSecond(Valor) << "\033[1;35m metro por segundos [m/s].\033[0m" << endl;
            break;

            case 0:
                cout << "Volviendo al menú principal" << endl;
                return;

            default:
                cout << "\n\nOpción no valida. Por favor ingrese una opción válida." << endl;
            break;
        
        }

        if (Opcion == 0){
            return;
        }

        continuar = askContinue();

    } while (continuar == 's' || continuar == 'S');
}

/*Time Submenu*/

int hoursToMinutes(int Valor) {
    return Valor * 60;
}

int minutesToSeconds(int Valor) {
    return Valor* 60;
}

int daysToHours(int Valor) {
    return Valor * 24;
}

int weeksToDays(int Valor) {
    return Valor * 7;
}

int yearsToDays(int Valor) {
    return Valor * 365;
}

int hoursToSeconds(int Valor) {
    return Valor * 3600;
}

/*Problemas de precisión [Solucionado]*/

long long minutesToMilliseconds(int Valor) { /*Utilizacón de long long para multiplicación*/
    return static_cast<long long>(Valor) * 60000; /*Perdida de datos de precisión. Uso de static_cast y long long (LL)*/
}

long long secondsToMilliseconds(int Valor) { /*Utilizacón de long long para multiplicación*/
    return static_cast<long long>(Valor) * 1000; /*Perdida de datos de precisión. Uso de static_cast y long long (LL)*/
}

double daysToWeeks(int Valor) {
    return static_cast<double>(Valor) / 7.0; /*Perdida de datos de precisión. Uso de static_cast y long long (LL)*/
}

double weeksToMonths(double Valor) {
    return Valor / 4.34812;
}

double daysToYears(int Valor) {
    return static_cast<double>(Valor) / 365.0; /*Perdida de datos de precisión. Uso de static_cast y long long (LL)*/
}

/*------ Fin problemas de precisión ------*/

void submenuTime(){
    char continuar;

    do{
        int Opcion;
        double Valor;

        cout << "\nConversor de Tiempo\n" << endl;
        cout << "\t[0] Volver al menú principal" << endl;
        cout << "\t[1] Horas [hrs] a minutos [min] " << endl;
        cout << "\t[2] Minutos [min] a segundos [seg]" << endl;
        cout << "\t[3] Días [días] a horas [hrs]" << endl;
        cout << "\t[4] Semanas [semanas] a días [días]" << endl;
        cout << "\t[5] Años [años] a días [días]" << endl;
        cout << "\t[6] Horas [hrs] a segundos [seg]" << endl;
        cout << "\t[7] Minutos [min] a milisegundos [ms]" << endl;
        cout << "\t[8] Segundos [s] a milisegundos [ms]" << endl;
        cout << "\t[9] días [días] a semanas [semanas]" << endl;
        cout << "\t[10] Semanas [semanas] a meses [meses]" << endl;
        cout << "\t[11] días [días] a años [años]" << endl;
    

        cout << "\nIngrese su Selección: ";
        cin >> Opcion;
        mostrarRecuadro(30, 1);

        switch(Opcion){
            case 1:
                cout << "\n\nIngrese las horas: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m horas [hrs] serán \033[0m" 
                << hoursToMinutes(Valor) << "\033[1;35m minutos [min].\033[0m" << endl;
            break;

            case 2:
                cout << "\n\nIngrese los minutos: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m minutos [min] serán \033[0m" 
                << minutesToSeconds(Valor) << "\033[1;35m segundos [seg].\033[0m" << endl;
            break;

            case 3:
                cout << "\n\nIngrese los días: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m días [días] serán \033[0m" 
                << daysToHours(Valor) << "\033[1;35m horas [hrs].\033[0m" << endl;
            break;

            case 4:
                cout << "\n\nIngrese los semanas: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m semanas [semanas] serán \033[0m" 
                << weeksToDays(Valor) << "\033[1;35m días [días].\033[0m" << endl;
            break;

            case 5:
                cout << "\n\nIngrese los años: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m años [años] serán \033[0m" 
                << yearsToDays(Valor) << "\033[1;35m días [días].\033[0m" << endl;
            break;

            case 6:
                cout << "\n\nIngrese las horas: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m horas [horas] serán \033[0m" 
                << hoursToSeconds(Valor) << "\033[1;35m segundos [segundos].\033[0m" << endl;
            break;

            case 7:
                cout << "\n\nIngrese los minutos: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m minutos [min] serán \033[0m" 
                << hoursToSeconds(Valor) << "\033[1;35m milisegundos [ms].\033[0m" << endl;
            break;

            case 8:
                cout << "\n\nIngrese las segundos: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m segundos [s] serán \033[0m" 
                << hoursToSeconds(Valor) << "\033[1;35m milisegundos [ms].\033[0m" << endl;
            break;

            case 9:
                cout << "\n\nIngrese las días: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m días [días] serán \033[0m" 
                << hoursToSeconds(Valor) << "\033[1;35m semanas [semanas].\033[0m" << endl;
            break;

            case 10:
                cout << "\n\nIngrese las semanas: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m semanas [semanas] serán \033[0m" 
                << hoursToSeconds(Valor) << "\033[1;35m meses [meses].\033[0m" << endl;
            break;

            case 11:
                cout << "\n\nIngrese las días: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m días [días] serán \033[0m" 
                << hoursToSeconds(Valor) << "\033[1;35m años [años].\033[0m" << endl;
            break;

            case 0:
                cout << "Volviendo al menú principal" << endl;
                return;

            default:
                cout << "\n\nOpción no valida. Por favor ingrese una opción válida." << endl;
            break;
        
        }

        if (Opcion == 0){
            return;
        }

        continuar = askContinue();

    } while (continuar == 's' || continuar == 'S');
}

/*Pressure Submenu*/

double pascalesToAtmosphere(double Valor){
    return Valor * 0.00000986923;
}

double pascalesToBares(double Valor){
    return Valor * 0.00001;
}

double pascalesToPsi(double Valor){
    return Valor * 0.00014503773773375;
}

double pascalesToMmHg(double Valor){
    return Valor * 0.00750062;
}

double pascalesToKilopascales(double Valor){
    return Valor / 1000.0;
}

double atmosphereToPascales(double Valor) {
    return Valor * 101325.0;
}

double atmosphereToBares(double Valor) {
    return Valor * 1.01325;
}

double mmHgToPascales(double Valor) {
    return Valor * 133.322;
}

void submenuPressure(){
    char continuar;

    do{
        int Opcion;
        double Valor;

        cout << "\nConversor de presión\n" << endl;
        cout << "\t[0] Volver al menú principal" << endl;
        cout << "\t[1] Pascales [Pa] a Atmosferas [atm]" << endl;
        cout << "\t[2] Pascales [Pa] a Bares [bar]" << endl;
        cout << "\t[3] Pascales [Pa] a Libra sobre pulgada cuadrada [psi]" << endl;
        cout << "\t[4] Pascales [Pa] a Milimetros de mercurio [mmHg]" << endl;
        cout << "\t[5] Atmosferas [atm] a Pascales [Pa]" << endl;
        cout << "\t[6] Atmosferas [Pa] a Bares [bar]" << endl;
        cout << "\t[7] Milimetros de mercurio [mmHg] a Pascales [Pa]\n" << endl;

        cout << "\nIngrese su Selección: ";
        cin >> Opcion;
        mostrarRecuadro(30, 1); 

        switch(Opcion){
            case 1:
                cout << "\n\nIngrese presión en Pascales [Pa]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pascales [Pa] serán \033[0m" 
                << pascalesToAtmosphere(Valor) << "\033[1;35m atmosferas [atm].\033[0m" << endl;
            break;

            case 2:
                cout << "\n\nIngrese presión en Pascales [Pa]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pascales [Pa] serán \033[0m" 
                << pascalesToBares(Valor) << "\033[1;35m bares [bar].\033[0m" << endl;
            break;

            case 3:
                cout << "\n\nIngrese presión en Pascales [Pa]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pascales [Pa] serán \033[0m" 
                << pascalesToPsi(Valor) << "\033[1;35m libras sobre pulgada cuadrada [psi].\033[0m" << endl;
            break;

            case 4:
                cout << "\n\nIngrese presión en Pascales [Pa]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pascales [Pa] serán \033[0m" 
                << pascalesToMmHg(Valor) << "\033[1;35m milimetros de mercurio [mmHg].\033[0m" << endl;
            break;

            case 5:
                cout << "\n\nIngrese presión en Atmosferas [atm]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m atmosferas [atm] serán \033[0m" 
                << atmosphereToPascales(Valor) << "\033[1;35m pascales [pas].\033[0m" << endl;
            break;

            case 6:
                cout << "\n\nIngrese presión en Atmosferas [atm]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pascales [Pa] serán \033[0m" 
                << atmosphereToBares(Valor) << "\033[1;35m bares [bar].\033[0m" << endl;
            break;

            case 7:
                cout << "\n\nIngrese presión en Milimetros de mercurio [mmHg]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m milimetros de mercurio [mmHg] serán \033[0m" 
                << mmHgToPascales(Valor) << "\033[1;35m pascales [Pa].\033[0m" << endl;
            break;

            case 0:
                cout << "Volviendo al menú principal" << endl;
                return;

            default:
                cout << "\n\nOpción no valida. Por favor ingrese una opción válida." << endl;
            break;
        
        }

        if (Opcion == 0){
            return;
        }

        if (Opcion == 0){
            return;
        }

        continuar = askContinue();

    } while (continuar == 's' || continuar == 'S');
}

/*Energy Functions*/

double joulesToCalories(double Valor){
    const double factorConversion = 0.239006; 
    return Valor/factorConversion;
}

double joulesToKilojoules(double Valor) {
    return Valor / 1000.0;
}

double joulesToElectronvoltios(double Valor) {
    const double factorConversion = 6.242e12;
    return Valor * factorConversion;
}

double caloriesToJoules(double Valor) {
    const double factorConversion = 4.184;
    return Valor * factorConversion;
}

double kilocaloriesToJoules(double Valor) {
    const double factorConversion = 4184.0;
    return Valor * factorConversion;
}

void submenuEnergy(){
    char continuar;

    do{
        int Opcion;
        double Valor;

        cout << "\nConversor de Energía\n" << endl;
        cout << "\t[0] Volver al menú principal" << endl;
        cout << "\t[1] Julios [J] a Calorías [cal]" << endl;
        cout << "\t[2] Julios [J] a Kilojulios [KJ]" << endl;
        cout << "\t[3] Julios [J] a Electronvoltios [eV]" << endl;
        cout << "\t[4] Calorias [cal] a Julios [J]" << endl;
        cout << "\t[5] Kilocalorias [kCal] a Julios [J]\n" << endl;

        cout << "\nIngrese su Selección: ";
        cin >> Opcion;
        mostrarRecuadro(30, 1); 

        switch(Opcion){
            case 1:
                cout << "\n\nIngrese energía en Julios [J]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m julios [J] serán \033[0m" 
                << joulesToCalories(Valor) << "\033[1;35m calorias [cal].\033[0m" << endl;
                break;

            case 2:
                cout << "\n\nIngrese energía en Julios [J]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m julios [J] serán \033[0m" 
                << joulesToKilojoules(Valor) << "\033[1;35m kilojulios [KJ].\033[0m" << endl;
                break;

            case 3:
                cout << "\n\nIngrese energía en Julios [J]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m julios [J] serán \033[0m" 
                << joulesToElectronvoltios(Valor) << "\033[1;35m electronvoltios [eV].\033[0m" << endl;
                break;

            case 4:
                cout << "\n\nIngrese energía en calorias [cal]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m julios [J] serán \033[0m" 
                << caloriesToJoules(Valor) << "\033[1;35m julios [J].\033[0m" << endl;
                break;

            case 5:
                cout << "\n\nIngrese energía en kiloCalorias [kCal]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m kiloCalorias [kCal] serán \033[0m" 
                << kilocaloriesToJoules(Valor) << "\033[1;35m julios [J].\033[0m" << endl;
                break;

            case 0:
                cout << "Volviendo al menú principal" << endl;
                break;

            default:
                cout << "\n\nOpción no valida. Por favor ingrese una opción válida." << endl;
                break;
        
        }

        if (Opcion == 0){
            return;
        }

        continuar = askContinue();

    } while (continuar == 's' || continuar == 'S');
}

class FormulaEnergiaCinetica{ /*Crearemos las clases con sus constructores correspondientes*/
    private:
        double masa; /*Haremos privadas a masa y a velocidad. Esto cambiará dependiendo de qué pida la fórmula*/
        double velocidad;
    
    public:
        FormulaEnergiaCinetica(){
            masa = 0.0; /*Seteamos en ceros en caso de no ingresarse*/
            velocidad = 0.0;

        }

        void ingresarDato(){ /*Ingresar los datos*/

            cout << "Ingrese la \033[1;35mmasa\033[0m del objeto en Kilogramos \033[1;35m[kg]\033[0m: " << endl;
            cin >> masa;

            cout << "Ingrese la \033[1;35mvelocidad\033[0m del objeto en metros por segundos \033[1;35m[m/s]\033[0m: " << endl; 
            cin >> velocidad;

        }

        double calcularEnergiaCinetica(){ /*Utilización de la formula*/

            return 0.5 * masa * pow(velocidad, 2);

        }

        void mostrarResultado(){ /*Mostrar datos*/

            double energiaCinetica = calcularEnergiaCinetica();
            cout << "La \033[1;35menergía cinética\033[0m del objeto es de " << energiaCinetica << " julios\n" << endl;

        }
};

class FormulaEnergiaPotencialGravitatoria{ /*Misma lógica seguida de las clases definidas*/
    private:
        double masa;
        double altura;
        long double gravedad;
    
    public:
        FormulaEnergiaPotencialGravitatoria(){
            masa = 0.0;
            altura = 0.0;
            gravedad = 9.80665;
        }

        void ingresarDato(){

            cout << "Ingrese la \033[1;35mmasa\033[0m del objeto en Kilogramos \033[1;35m[kg]\033[0m: " << endl;
            cin >> masa;

            cout << "Ingrese la \033[1;35maltura\033[0m del objeto en metros  \033[1;35m[m]\033[0m (revisar sistema de referencia): " << endl; 
            cin >> altura;

        }

        double calcularEnergiaPotenicalGravitacional(){

            return masa * gravedad * altura;

        }

        void mostrarResultado(){

            double energiapotencialgravitatoria = calcularEnergiaPotenicalGravitacional();
            cout << "La \033[1;35menergía potencial gravitatoria\033[0m del objeto es de " << energiapotencialgravitatoria << " julios\n" << endl;

        }
};

class FormulaEnergiaPotencialElastica{
    private:
        double cteElastica;
        double deformacion;
    
    public:
        FormulaEnergiaPotencialElastica(){
            cteElastica = 0.0;
            deformacion = 0.0;
        }

        void ingresarDato(){

            cout << "Ingrese la \033[1;35m constante elástica\033[0m del objeto\033[1;35m[kg]\033[0m: " << endl;
            cin >> cteElastica;

            cout << "Ingrese la \033[1;35mdeformación sufrida por el objeto\033[0m del objeto en metros\033[1;35m[m]\033[0m (revisar sistema de referencia): " << endl; 
            cin >> deformacion;

        }

        double calcularEnergiaPotenicalElastica(){

            return 0.5 * cteElastica * pow(deformacion, 2);

        }

        void mostrarResultado(){

            double energiapotencialelastica = calcularEnergiaPotenicalElastica();
            cout << "La \033[1;35menergía potencial elastica\033[0m del objeto es de " << energiapotencialelastica << " julios\n" << endl;

        }
};

class CalculadoraEnergiaTotal{
    private: /*Utilizaremos las clases anteriores para definir esta clase*/
        FormulaEnergiaCinetica cinetica;
        FormulaEnergiaPotencialGravitatoria gravitatoria;
        FormulaEnergiaPotencialElastica elastica;

    public:
        void ingresarDatos(){
            cout << "Cálculo de la energía total del sistema" << endl;
        
            cinetica.ingresarDato();/*Se ingresarán los datos de cada una de las clases para realizar su sumatoria*/
            gravitatoria.ingresarDato();
            elastica.ingresarDato();
        }   

        double calcularEnergiaTotal(){
            double energiaCinetica = cinetica.calcularEnergiaCinetica();
            double energiaPotencialGravitatoria = gravitatoria.calcularEnergiaPotenicalGravitacional();
            double energiaPotencialElastica = elastica.calcularEnergiaPotenicalElastica();

            return energiaCinetica + energiaPotencialGravitatoria + energiaPotencialElastica;
        }  

        void mostrarResultado(){
            double energiaTotal = calcularEnergiaTotal();
            cout << "La \033[1;35menergía total\033[0m del sistema es de " << energiaTotal << " julios" << endl;
        }
    };


void menuFisico(){

    char respuesta;

    /* Option [2] Windows */

    cout << "\n\t\033[1;35m¡Bienvenido a la Calculadora de Energías!\033[0m\n" <<    endl;
    cout << "Seleccione el menú de energía a continuación:\n" << endl;
    
    cout << "\t[0] Volver al menú principal" << endl;
    cout << "\t[1] Energía Cinética" << endl;
    cout << "\t[2] Energía Potencial Gravitatoria" << endl;
    cout << "\t[3] Energía Potencial Elástica" << endl;
    cout << "\t[4] Energía Total de un Sistema" << endl;
    cout << "\t[5] Formularios de Energías" << endl;

    cout << "\nIngrese su selección: ";
    cin >> Conversion;
    mostrarRecuadro(30, 1); 

    /*Functions Used*/

    switch(Conversion){

        case 0:
            cout << "...Volviendo al \033[1;35mmenú principal\033[0m ..." << endl;
            break;

        case 1: /*Llamaremos a la clase y utilizaremos las funciones definidas dentro de esta para utilizarla en el código*/
            do{
                {
                    FormulaEnergiaCinetica calculadora;
                    cout << "\nCalculadora de Energía Cinética\n" << endl;
                    cout << "\nPor favor ingrese los datos solicitados a continuación en las \033[1;35munidades correspondientes.\033[0m\n" << endl;
                    calculadora.ingresarDato();
                    calculadora.mostrarResultado();
                    respuesta = askContinueFisico();

                };
            } while (respuesta == 's' || respuesta == 'S'); /*do-while se podrá romper al terminar de ingresar los datos*/
            break;

        case 2:
            do{
                {
                    FormulaEnergiaPotencialGravitatoria calculadora;
                    cout << "\nCalculadora de Energía Potenical Gravitatoria\n" << endl;
                    cout << "\nPor favor ingrese los datos solicitados a continuación en las \033[1;35munidades correspondientes.\033[0m\n" << endl;
                    calculadora.ingresarDato();
                    calculadora.mostrarResultado();
                    respuesta = askContinueFisico();

                };
            } while (respuesta == 's' || respuesta == 'S');
            break;

        case 3:
            do{
                {
                    FormulaEnergiaPotencialElastica calculadora;
                    cout << "\nCalculadora de Energía Potenical Elastica\n" << endl;
                    cout << "\nPor favor ingrese los datos solicitados a continuación en las \033[1;35munidades correspondientes.\033[0m\n" << endl;
                    calculadora.ingresarDato();
                    calculadora.mostrarResultado();
                    respuesta = askContinueFisico();

                };
            } while (respuesta == 's' || respuesta == 'S');
            break;

        case 4:
            do{
                {
                    CalculadoraEnergiaTotal calculadora;
                    cout << "\nCalculadora de Energía Total de un Sístema\n" << endl;
                    cout << "\nPor favor ingrese los datos solicitados a continuación en las \033[1;35munidades correspondientes.\033[0m\n" << endl;
                    calculadora.ingresarDatos();
                    calculadora.mostrarResultado();
                    respuesta = askContinueFisico();

                };
            } while (respuesta == 's' || respuesta == 'S');
            break;
        
        case 5: /* Solo imprimirá por pantalla los valores, pero quedará en el bucle si sigue seleccionando la opción de seguir.*/
            do{
                {
                    cout << "\nFormulario de Energías\n" << endl;
                    cout << "\nA cotninuación se mostrarán las ecuaciones de cada uno de los cáluclos que se pueden hacer de energía en el programa:\n" << endl;
                    cout << "\t\033[1;35mEnergía Cinetica [Ec]\033[0m = (1/2) * masa * velocidad², unidad: \033[1;35m[J]\033[0m\n"<< endl;
                    cout << "\t\033[1;35mEnergía Potencial Gravitatoria [Ep] \033[0m= masa * gravedad * altura, unidad: \033[1;35m[J]\033[0m\n"<< endl;
                    cout << "\t\033[1;35mEnergía Potencial Elastica [Ee] \033[0m= (1/2) * constante * deformación², unidad: \033[1;35m[J]\033[0m\n"<< endl;
                    cout << "\t\033[1;35mEnergía Total de un Sístema [Em] \033[0m= [Ec] + [Ep] + [Ee], unidad: \033[1;35m[J]\033[0m\n"<< endl;
                    respuesta = askContinueForm();

                };
            } while (respuesta == 's' || respuesta == 'S');
            break;

    }
}

void menuConversion(){

    /* Option [1] Windows */

    cout << "\n\t\033[1;35m¡Bienvenido al Conversor de Unidades!\033[0m\n" <<    endl;
    cout << "Seleccione el menú de conversión a continuación:\n" << endl;
    
    cout << "\t[0] Volver al menú principal" << endl;
    cout << "\t[1] Conversión de Longitud" << endl;
    cout << "\t[2] Conversión de Area" << endl;
    cout << "\t[3] Conversión de Volúmen" << endl;
    cout << "\t[4] Conversión de Temperatura" << endl;
    cout << "\t[5] Conversión de Velocidad" << endl;
    cout << "\t[6] Conversión de Tiempo" << endl;
    cout << "\t[7] Conversión de Presión" << endl;
    cout << "\t[8] Conversión de Energía" << endl;

    cout << "\nIngrese su selección: ";
    cin >> Conversion;
    mostrarRecuadro(30, 1); 

    /*Functions Used*/

    switch(Conversion){ /*Llamados de todas las funciones creadas antes*/

        case 0:
            cout << "...Volviendo al \033[1;35mmenú principal\033[0m ..." << endl;
            break;

        case 1:
            submenuLength();
            break;

        case 2:
            submenuArea();
            break;

        case 3:
            submenuVolume();
            break;

        case 4:
            submenuTemperature();
            break;
        
        case 5:
            submenuVelocity();
            break;
        
        case 6:
            submenuTime();
            break;

        case 7:
            submenuPressure();
            break;

        case 8:
            submenuEnergy();
            break;

        default:
            cout << "\nOpción no válida. Intente de nuevo." << endl;
            break;

    cout << "...Volviendo al \033[1;35mmenú principal\033[0m ..." << endl;

    break;
    }
}