#include <iostream>
#include <math.h>
#include <string>
#include <cmath>


using namespace std;

int Conversion;

/*Print Design*/

void mostrarRecuadro(int ancho, int alto) {
    for (int i = 0; i < alto; ++i) {
        for (int j = 0; j < ancho; ++j) {
            if (i == 0 || i == alto - 1) {
                if (j == 0 || j == ancho - 1) {
                    cout << '+';
                } else {
                    cout << '-';
                }
            } else {
                if (j == 0 || j == ancho - 1) {
                    cout << '|';
                } else {
                    cout << ' ';
                }
            }
        }
        cout << endl;
    }
}

/*Error Declaration*/

void continueFunction(char &Continuar){
    cout << "\n\n¿Desea realizar otra conversión? (S/N): ";
    cin >> Continuar;
}

/*class Declaration*/

class FormulaEnergiaCinetica{ /*Crearemos las clases con sus constructores correspondientes*/
    private:
        double masa; /*Haremos privadas a masa y a velocidad. Esto cambiará dependiendo de qué pida la fórmula*/
        double velocidad;
    
    public:
        FormulaEnergiaCinetica(){
            masa = 0.0; /*Seteamos en ceros en caso de no ingresarse*/
            velocidad = 0.0;

        }

        void ingresarDato(){ /*Ingresar los datos*/

            cout << "Ingrese la \033[1;35mmasa\033[0m del objeto en Kilogramos \033[1;35m[kg]\033[0m: " << endl;
            cin >> masa;

            cout << "Ingrese la \033[1;35mvelocidad\033[0m del objeto en metros por segundos \033[1;35m[m/s]\033[0m: " << endl; 
            cin >> velocidad;

        }

        double calcularEnergiaCinetica(){ /*Utilización de la formula*/

            return 0.5 * masa * pow(velocidad, 2);

        }

        void mostrarResultado(){ /*Mostrar datos*/

            double energiaCinetica = calcularEnergiaCinetica();
            cout << "La \033[1;35menergía cinética\033[0m del objeto es de " << energiaCinetica << " julios\n" << endl;

        }
};

void menuFisico(){

    char respuesta;

    /* Option [2] Windows */

    cout << "\n\t\033[1;35m¡Bienvenido a la Calculadora de Energías!\033[0m\n" <<    endl;
    cout << "Seleccione el menú de energía a continuación:\n" << endl;
    
    cout << "\t[0] Volver al menú principal" << endl;
    cout << "\t[1] Energía Cinética" << endl;
    cout << "\t[2] Energía Potencial Gravitatoria" << endl;
    cout << "\t[3] Energía Potencial Elástica" << endl;
    cout << "\t[4] Energía Total de un Sistema" << endl;
    cout << "\t[5] Formularios de Energías" << endl;

    cout << "\nIngrese su selección: ";
    cin >> Conversion;
    mostrarRecuadro(30, 1); 

    /*Functions Used*/

    switch(Conversion){

        case 0:
            cout << "...Volviendo al \033[1;35mmenú principal\033[0m ..." << endl;
            break;

        case 1: /*Llamaremos a la clase y utilizaremos las funciones definidas dentro de esta para utilizarla en el código*/
            do{
                {
                    FormulaEnergiaCinetica calculadora;
                    cout << "\nCalculadora de Energía Cinética\n" << endl;
                    cout << "\nPor favor ingrese los datos solicitados a continuación en las \033[1;35munidades correspondientes.\033[0m\n" << endl;
                    calculadora.ingresarDato();
                    calculadora.mostrarResultado();
                    respuesta = askContinueFisico();

                };
            } while (respuesta == 's' || respuesta == 'S'); /*do-while se podrá romper al terminar de ingresar los datos*/
            break;
    }
}