#include "generalities.h"

int main(int argc, char* argv[]){

    /* Main windows */

cout << "\n\t\033[1;35m¡Bienvenido a la Calculadora de Energías!\033[0m\n" <<    endl;
    cout << "Seleccione el menú de energía a continuación:\n" << endl;
    
    cout << "\t[0] Volver al menú principal" << endl;
    cout << "\t[1] Energía Cinética" << endl;
    cout << "\t[2] Energía Potencial Gravitatoria" << endl;
    cout << "\t[3] Energía Potencial Elástica" << endl;
    cout << "\t[4] Energía Total de un Sistema" << endl;
    cout << "\t[5] Formularios de Energías" << endl;

    cout << "\nIngrese su selección: ";
    cin >> Conversion;
    mostrarRecuadro(30, 1); 

    /*Functions Used*/

    switch(Conversion){

        case 0:
            cout << "...Volviendo al \033[1;35mmenú principal\033[0m ..." << endl;
            break;

        case 1: /*Llamaremos a la clase y utilizaremos las funciones definidas dentro de esta para utilizarla en el código*/
            do{
                {
                    FormulaEnergiaCinetica calculadora;
                    cout << "\nCalculadora de Energía Cinética\n" << endl;
                    cout << "\nPor favor ingrese los datos solicitados a continuación en las \033[1;35munidades correspondientes.\033[0m\n" << endl;
                    calculadora.ingresarDato();
                    calculadora.mostrarResultado();
                    respuesta = askContinueFisico();

                };
            } while (respuesta == 's' || respuesta == 'S'); /*do-while se podrá romper al terminar de ingresar los datos*/
            break;
}
