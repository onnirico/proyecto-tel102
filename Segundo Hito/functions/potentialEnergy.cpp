/*class FomrulaEnergiaPotenicalGravitatoria*/

class FormulaEnergiaPotencialGravitatoria{
    private:
        double masa;
        double altura;
        long double gravedad;
    
    public:
        FormulaEnergiaPotencialGravitatoria(){
            masa = 0.0;
            altura = 0.0;
            gravedad = 9.80665;
        }

        void ingresarDato(){

            cout << "Ingrese la \033[1;35mmasa\033[0m del objeto en Kilogramos \033[1;35m[kg]\033[0m: " << endl;
            cin >> masa;

            cout << "Ingrese la \033[1;35maltura\033[0m del objeto en metros  \033[1;35m[m]\033[0m (revisar sistema de referencia): " << endl; 
            cin >> altura;

        }

        double calcularEnergiaPotenicalGravitacional(){

            return masa * gravedad * altura;

        }

        void mostrarResultado() {

            double energiapotencialgravitatoria = calcularEnergiaPotenicalGravitacional();
            cout << "La \033[1;35menergía potencial gravitatoria\033[0m del objeto es de " << energiapotencialgravitatoria << " julios\n" << endl;

        }
};