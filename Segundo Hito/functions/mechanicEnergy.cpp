/*class CalculadoraEnergiaTotal*/

class CalculadoraEnergiaTotal{
private:
    FormulaEnergiaCinetica cinetica;
    FormulaEnergiaPotencialGravitatoria gravitatoria;
    FormulaEnergiaPotencialElastica elastica;

public:
    void ingresarDatos(){
        cout << "Cálculo de la energía total del sistema" << endl;
        
        cinetica.ingresarDato();
        gravitatoria.ingresarDato();
        elastica.ingresarDato();
    }

    double calcularEnergiaTotal() {
        double energiaCinetica = cinetica.calcularEnergiaCinetica();
        double energiaPotencialGravitatoria = gravitatoria.calcularEnergiaPotenicalGravitacional();
        double energiaPotencialElastica = elastica.calcularEnergiaPotenicalElastica();

        return energiaCinetica + energiaPotencialGravitatoria + energiaPotencialElastica;
    }

    void mostrarResultado() {
        double energiaTotal = calcularEnergiaTotal();
        cout << "La \033[1;35menergía total\033[0m del sistema es de " << energiaTotal << " julios" << endl;
    }
};
