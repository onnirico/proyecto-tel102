/*Submenú Físico*/

void menuFisico(){

    char respuesta;

    /* Option [2] Windows */

    cout << "\n\t\033[1;35m¡Bienvenido a la Calculadora de Energías!\033[0m\n" <<    endl;
    cout << "Seleccione el menú de energía a continuación:\n" << endl;
    
    cout << "\t[0] Volver al menú principal" << endl;
    cout << "\t[1] Energía Cinética" << endl;
    cout << "\t[2] Energía Potencial Gravitatoria" << endl;
    cout << "\t[3] Energía Potencial Elástica" << endl;
    cout << "\t[4] Energía Total de un Sistema" << endl;
    cout << "\t[5] Formularios de Energías" << endl;

    cout << "\nIngrese su selección: ";
    cin >> Conversion;
    mostrarRecuadro(30, 1); 

    /*Functions Used*/

    switch(Conversion){ /*Llamados de todas las funciones creadas antes*/

        case 0:
            cout << "...Volviendo al \033[1;35mmenú principal\033[0m ..." << endl;
            break;

        case 1:
            do{
                {
                    FormulaEnergiaCinetica calculadora;
                    cout << "\nCalculadora de Energía Cinética\n" << endl;
                    cout << "\nPor favor ingrese los datos solicitados a continuación en las \033[1;35munidades correspondientes.\033[0m\n" << endl;
                    calculadora.ingresarDato();
                    calculadora.mostrarResultado();
                    respuesta = askContinueFisico();

                };
            } while (respuesta == 's' || respuesta == 'S');
            break;

        case 2:
            break;

        case 3:
            break;

        case 4:
            break;
        
        case 5:
            do{
                {
                    cout << "\nFormulario de Energías\n" << endl;
                    cout << "\nA cotninuación se mostrarán las ecuaciones de cada uno de los cáluclos que se pueden hacer de energía en el programa:\n" << endl;
                    cout << "\t\033[1;35mEnergía Cinetica [Ec]\033[0m = (1/2) * masa * velocidad², unidad: \033[1;35m[J]\033[0m\n"<< endl;
                    cout << "\t\033[1;35mEnergía Potencial Gravitatoria [Ep] \033[0m= masa * gravedad * altura, unidad: \033[1;35m[J]\033[0m\n"<< endl;
                    cout << "\t\033[1;35mEnergía Potencial Elastica [Ee] \033[0m= (1/2) * constante * deformación², unidad: \033[1;35m[J]\033[0m\n"<< endl;
                    cout << "\t\033[1;35mEnergía Total de un Sístema [Em] \033[0m= [Ec] + [Ep] + [Ee], unidad: \033[1;35m[J]\033[0m\n"<< endl;
                    respuesta = askContinueForm();

                };
            } while (respuesta == 's' || respuesta == 'S');
            break;

    }
}