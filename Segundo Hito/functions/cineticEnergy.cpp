/*class FomrulaEnergiaCinetica*/
class FormulaEnergiaCinetica{
    private:
        double masa;
        double velocidad;
    
    public:
        FormulaEnergiaCinetica(){
            masa = 0.0;
            velocidad = 0.0;

        }

        void ingresarDato(){

            cout << "Ingrese la \033[1;35mmasa\033[0m del objeto en Kilogramos \033[1;35m[kg]\033[0m: " << endl;
            cin >> masa;

            cout << "Ingrese la \033[1;35mvelocidad\033[0m del objeto en metros por segundos \033[1;35m[m/s]\033[0m: " << endl; 
            cin >> velocidad;

        }

        double calcularEnergiaCinetica(){

            return 0.5 * masa * std::pow(velocidad, 2);

        }

        void mostrarResultado() {

            double energiaCinetica = calcularEnergiaCinetica();
            std::cout << "La \033[1;35menergía cinética\033[0m del objeto es de " << energiaCinetica << " julios\n" << std::endl;

        }
};