/*class FormulaEnergiaPotencialElastica*/

class FormulaEnergiaPotencialElastica{
    private:
        double cteElastica;
        double deformacion;
    
    public:
        FormulaEnergiaPotencialElastica(){
            cteElastica = 0.0;
            deformacion = 0.0;
        }

        void ingresarDato(){

            cout << "Ingrese la \033[1;35m constante elástica\033[0m del objeto\033[1;35m[kg]\033[0m: " << endl;
            cin >> cteElastica;

            cout << "Ingrese la \033[1;35mdeformación sufrida por el objeto\033[0m del objeto en metros\033[1;35m[m]\033[0m (revisar sistema de referencia): " << endl; 
            cin >> deformacion;

        }

        double calcularEnergiaPotenicalElastica(){

            return 0.5 * cteElastica * pow(deformacion, 2);

        }

        void mostrarResultado() {

            double energiapotencialelastica = calcularEnergiaPotenicalElastica();
            cout << "La \033[1;35menergía potencial elastica\033[0m del objeto es de " << energiapotencialelastica << " julios\n" << endl;

        }
};