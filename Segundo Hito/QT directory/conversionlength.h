#ifndef CONVERSIONLENGTH_H
#define CONVERSIONLENGTH_H

#include <QWidget>

namespace Ui {
class conversionLength;
}

class conversionLength : public QWidget
{
    Q_OBJECT

public:
    explicit conversionLength(QWidget *parent = nullptr);
    ~conversionLength();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void on_pushButton_3_clicked();
    void on_pushButton_4_clicked();
    void on_pushButton_5_clicked();
    void on_pushButton_6_clicked();


private:
    Ui::conversionLength *ui;
};

#endif // CONVERSIONLENGTH_H
