#ifndef PHYSICWINDOW_H
#define PHYSICWINDOW_H

#include <QWidget>

namespace Ui {
class physicWindow;
}

class physicWindow : public QWidget
{
    Q_OBJECT

public:
    explicit physicWindow(QWidget *parent = nullptr);
    ~physicWindow();

private slots:
    void on_pushButton_clicked();    // Función para cerrar la aplicación

private:
    Ui::physicWindow *ui;
};

#endif // PHYSICWINDOW_H
