#ifndef CONVERSIONENERGY_H
#define CONVERSIONENERGY_H

#include <QWidget>

namespace Ui {
class conversionEnergy;
}

class conversionEnergy : public QWidget
{
    Q_OBJECT

public:
    explicit conversionEnergy(QWidget *parent = nullptr);
    ~conversionEnergy();

private slots:
    void on_pushButton_clicked();    // Función para cerrar la aplicación

private:
    Ui::conversionEnergy *ui;
};

#endif // CONVERSIONENERGY_H
