#include "conversiontime.h"
#include "ui_conversiontime.h"

conversionTime::conversionTime(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::conversionTime)
{
    ui->setupUi(this);
}

void conversionTime::on_pushButton_clicked()
{
    this->close(); // Cierra solo la ventana actual
}


conversionTime::~conversionTime()
{
    delete ui;
}
