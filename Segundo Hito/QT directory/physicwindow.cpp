#include "physicwindow.h"
#include "ui_physicwindow.h"

physicWindow::physicWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::physicWindow)
{
    ui->setupUi(this);
}

void physicWindow::on_pushButton_clicked()
{
    this->close(); // Cierra solo la ventana actual
}

physicWindow::~physicWindow()
{
    delete ui;
}
