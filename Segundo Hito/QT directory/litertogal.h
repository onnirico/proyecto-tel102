#ifndef LITERTOGAL_H
#define LITERTOGAL_H

#include <QWidget>

namespace Ui {
class literToGal;
}

class literToGal : public QWidget
{
    Q_OBJECT

public:
    explicit literToGal(QWidget *parent = nullptr);
    ~literToGal();

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();

private:
    Ui::literToGal *ui;
};

#endif // LITERTOGAL_H
