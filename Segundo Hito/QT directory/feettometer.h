// feetToMeter.h

#ifndef FEETTOMETER_H
#define FEETTOMETER_H

#include <QWidget>

namespace Ui {
class feetToMeter;
}

class feetToMeter : public QWidget
{
    Q_OBJECT

public:
    explicit feetToMeter(QWidget *parent = nullptr);
    ~feetToMeter();

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();

private:
    Ui::feetToMeter *ui;
};

#endif // FEETTOMETER_H
