#include "mililitertoliter.h"
#include "ui_mililitertoliter.h"

mililiterToLiter::mililiterToLiter(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::mililiterToLiter)
{
    ui->setupUi(this);
    setWindowTitle("Conversión de Mililitros [mL] a Litros [L]");
        ui->label_3->setText("0.00");
}

mililiterToLiter::~mililiterToLiter()
{
    delete ui;
}

void mililiterToLiter::on_pushButton_clicked()
{
    this->close();
}

void mililiterToLiter::on_pushButton_2_clicked()
{
    double milliliters = ui->doubleSpinBox->value();
    double liters = milliliters / 1000;
    ui->label_3->setText(QString::number(liters, 'f', 2));
    ui->doubleSpinBox->setValue(0.0);
}
