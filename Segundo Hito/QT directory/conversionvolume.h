#ifndef CONVERSIONVOLUME_H
#define CONVERSIONVOLUME_H

#include <QWidget>

namespace Ui {
class conversionVolume;
}

class conversionVolume : public QWidget
{
    Q_OBJECT

public:
    explicit conversionVolume(QWidget *parent = nullptr);
    ~conversionVolume();

private slots:
    void on_pushButton_clicked();    // Función para cerrar la aplicación
    void on_pushButton_2_clicked();
    void on_pushButton_3_clicked();
    void on_pushButton_4_clicked();
    void on_pushButton_5_clicked();
    void on_pushButton_6_clicked();
    void on_pushButton_7_clicked();
    void on_pushButton_8_clicked();
    void on_pushButton_9_clicked();

private:
    Ui::conversionVolume *ui;
};

#endif // CONVERSIONVOLUME_H
