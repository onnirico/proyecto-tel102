#include "squarekilometertosquareacre.h"
#include "ui_squarekilometertosquareacre.h"

squareKilometerToSquareAcre::squareKilometerToSquareAcre(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::squareKilometerToSquareAcre)
{
    ui->setupUi(this);
    setWindowTitle("Conversión de Kilómetros Cuadrados a Acres"); // Cambia el título de la ventana

    // Configura la etiqueta de resultado a 0.00 al inicio
    ui->label_3->setText("0.00");
}

squareKilometerToSquareAcre::~squareKilometerToSquareAcre()
{
    delete ui;
}

void squareKilometerToSquareAcre::on_pushButton_clicked()
{
    this->close();
}

void squareKilometerToSquareAcre::on_pushButton_2_clicked()
{
    double squareKilometers = ui->doubleSpinBox->value(); // Obtén el valor en kilómetros cuadrados
    double acres = squareKilometers * 247.105; // Convierte kilómetros cuadrados a acres
    ui->label_3->setText(QString::number(acres, 'f', 2)); // Muestra el resultado en el QLabel
    ui->doubleSpinBox->setValue(0.0); // Establece el valor en la QDoubleSpinBox a cero
}
