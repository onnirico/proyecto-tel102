#include "kilometertomiles.h"
#include "ui_kilometertomiles.h"

kilometerToMiles::kilometerToMiles(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::kilometerToMiles)
{
    ui->setupUi(this);
    setWindowTitle("Kilometros a Millas"); // Cambia el título de la ventana

    // Configura la etiqueta de resultado a 0.00 al inicio
    ui->label_3->setText("0.00");
}

kilometerToMiles::~kilometerToMiles()
{
    delete ui;
}

void kilometerToMiles::on_pushButton_clicked()
{
    this->close(); // Cierra la ventana actual
}

void kilometerToMiles::on_pushButton_2_clicked()
{
    double kilometers = ui->doubleSpinBox->value(); // Obtiene el valor en kilómetros
    double miles = kilometers * 0.621371; // Convierte kilómetros a millas
    ui->label_3->setText(QString::number(miles, 'f', 2)); // Muestra el resultado en la etiqueta
    ui->doubleSpinBox->setValue(0.0); // Restablece el valor en la QDoubleSpinBox a cero
}
