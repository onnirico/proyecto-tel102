#include "acretohectare.h"
#include "ui_acretohectare.h"

acreToHectare::acreToHectare(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::acreToHectare)
{
    ui->setupUi(this);
    setWindowTitle("Conversión de Acres a Hectáreas"); // Cambia el título de la ventana

    // Configura la etiqueta de resultado a 0.00 al inicio
    ui->label_3->setText("0.00");
}

acreToHectare::~acreToHectare()
{
    delete ui;
}

void acreToHectare::on_pushButton_clicked()
{
    this->close();
}

void acreToHectare::on_pushButton_2_clicked()
{
    double acres = ui->doubleSpinBox->value(); // Obtén el valor en acres
    double hectares = acres * 0.404686; // Convierte acres a hectáreas
    ui->label_3->setText(QString::number(hectares, 'f', 2)); // Muestra el resultado en el QLabel
    ui->doubleSpinBox->setValue(0.0); // Establece el valor en la QDoubleSpinBox a cero
}

