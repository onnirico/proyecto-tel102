#include "cubicmetertocubicfoot.h"
#include "ui_cubicmetertocubicfoot.h"

cubicMeterToCubicFoot::cubicMeterToCubicFoot(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::cubicMeterToCubicFoot)
{
    ui->setupUi(this);
    setWindowTitle("Conversión de Metros Cúbicos [m³] a Pies Cúbicos [ft³]");
    ui->label_3->setText("0.00");
}

cubicMeterToCubicFoot::~cubicMeterToCubicFoot()
{
    delete ui;
}

void cubicMeterToCubicFoot::on_pushButton_clicked()
{
    this->close();
}

void cubicMeterToCubicFoot::on_pushButton_2_clicked()
{
    double cubicMeters = ui->doubleSpinBox->value();
    double cubicFeet = cubicMeters * 35.315;
    ui->label_3->setText(QString::number(cubicFeet, 'f', 2));
    ui->doubleSpinBox->setValue(0.0);
}
