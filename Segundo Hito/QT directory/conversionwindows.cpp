#include "conversionwindows.h"
#include "ui_conversionwindows.h"
#include "conversionlength.h"
#include "conversionarea.h"
#include "conversionvolume.h"
#include "conversiontemperature.h"
#include "conversionvelocity.h"
#include "conversiontime.h"
#include "conversionpressure.h"
#include "conversionenergy.h"

conversionWindows::conversionWindows(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::conversionWindows)
{
    ui->setupUi(this);
}


void conversionWindows::on_pushButton_clicked()
{
    this->close(); // Cierra solo la ventana actual
}

void conversionWindows::on_pushButton_2_clicked()
{
    conversionLength *lengthWindow = new conversionLength(); // Cambiamos el nombre de la variable
    lengthWindow->setAttribute(Qt::WA_DeleteOnClose);
    lengthWindow->show();
}

void conversionWindows::on_pushButton_3_clicked()
{
    conversionArea *areaWindow = new conversionArea(); // Cambiamos el nombre de la variable
    areaWindow->setAttribute(Qt::WA_DeleteOnClose);
    areaWindow->show();
}

void conversionWindows::on_pushButton_4_clicked()
{
    conversionVolume *volumeWindow = new conversionVolume(); // Cambiamos el nombre de la variable
    volumeWindow->setAttribute(Qt::WA_DeleteOnClose);
    volumeWindow  ->show();
}

void conversionWindows::on_pushButton_5_clicked()
{
    conversionTemperature *temperatureWindow = new conversionTemperature(); // Cambiamos el nombre de la variable
    temperatureWindow->setAttribute(Qt::WA_DeleteOnClose);
    temperatureWindow  ->show();
}

void conversionWindows::on_pushButton_6_clicked()
{
    conversionVelocity *velocityWindow = new conversionVelocity(); // Cambiamos el nombre de la variable
    velocityWindow->setAttribute(Qt::WA_DeleteOnClose);
    velocityWindow  ->show();
}

void conversionWindows::on_pushButton_7_clicked()
{
    conversionTime *timeWindow = new conversionTime(); // Cambiamos el nombre de la variable
    timeWindow->setAttribute(Qt::WA_DeleteOnClose);
    timeWindow  ->show();
}

void conversionWindows::on_pushButton_8_clicked()
{
    conversionPressure *pressureWindow = new conversionPressure(); // Cambiamos el nombre de la variable
    pressureWindow->setAttribute(Qt::WA_DeleteOnClose);
    pressureWindow  ->show();
}

void conversionWindows::on_pushButton_9_clicked()
{
    conversionEnergy *energyWindow = new conversionEnergy(); // Cambiamos el nombre de la variable
    energyWindow->setAttribute(Qt::WA_DeleteOnClose);
    energyWindow  ->show();
}

conversionWindows::~conversionWindows()
{
    delete ui;
}
