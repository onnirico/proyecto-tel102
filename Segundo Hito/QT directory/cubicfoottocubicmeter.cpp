#include "cubicfoottocubicmeter.h"
#include "ui_cubicfoottocubicmeter.h"

cubicFootToCubicMeter::cubicFootToCubicMeter(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::cubicFootToCubicMeter)
{
    ui->setupUi(this);
    setWindowTitle("Conversión de Pies Cúbicos [ft³] a Metros Cúbicos [m³]");
    ui->label_3->setText("0.00");
}

cubicFootToCubicMeter::~cubicFootToCubicMeter()
{
    delete ui;
}

void cubicFootToCubicMeter::on_pushButton_clicked()
{
    this->close();
}

void cubicFootToCubicMeter::on_pushButton_2_clicked()
{
    double cubicFeet = ui->doubleSpinBox->value();
    double cubicMeters = cubicFeet / 35.315;
    ui->label_3->setText(QString::number(cubicMeters, 'f', 2));
    ui->doubleSpinBox->setValue(0.0);
}
