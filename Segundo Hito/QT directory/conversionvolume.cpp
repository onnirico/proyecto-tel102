#include "conversionvolume.h"
#include "ui_conversionvolume.h"
#include "literToMililiter.h"
#include "mililiterToLiter.h"
#include "galToLiter.h"
#include "literToGal.h"
#include "cubicFootToCubicMeter.h"
#include "cubicMeterToCubicFoot.h"
#include "mililiterToOz.h"
#include "ozToMililiter.h"


conversionVolume::conversionVolume(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::conversionVolume)
{
    ui->setupUi(this);
}

void conversionVolume::on_pushButton_clicked()
{
    this->close(); // Cierra solo la ventana actual
}

void conversionVolume::on_pushButton_2_clicked()
{
    literToMililiter *literToMililiterWindow = new literToMililiter();
    literToMililiterWindow->show();
}

void conversionVolume::on_pushButton_3_clicked()
{
    mililiterToLiter *mililiterToLiterWindow = new mililiterToLiter();
    mililiterToLiterWindow->show();
}

void conversionVolume::on_pushButton_4_clicked()
{
    galToLiter *galToLiterWindow = new galToLiter();
    galToLiterWindow->show();
}


void conversionVolume::on_pushButton_5_clicked()
{
    literToGal *literToGalWindow = new literToGal();
    literToGalWindow->show();
}

void conversionVolume::on_pushButton_6_clicked()
{
    cubicFootToCubicMeter *cubicFootToCubicMeterWindow = new cubicFootToCubicMeter();
    cubicFootToCubicMeterWindow->show();
}

void conversionVolume::on_pushButton_7_clicked()
{
    cubicMeterToCubicFoot *cubicMeterToCubicFootWindow = new cubicMeterToCubicFoot();
    cubicMeterToCubicFootWindow->show();
}

void conversionVolume::on_pushButton_8_clicked()
{
    mililiterToOz *mililiterToOzWindow = new mililiterToOz();
    mililiterToOzWindow->show();
}

void conversionVolume::on_pushButton_9_clicked()
{
    ozToMililiter *ozToMililiterWindow = new ozToMililiter();
    ozToMililiterWindow->show();
}

conversionVolume::~conversionVolume()
{
    delete ui;
}
