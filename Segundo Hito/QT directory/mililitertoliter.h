#ifndef MILILITERTOLITER_H
#define MILILITERTOLITER_H

#include <QWidget>

namespace Ui {
class mililiterToLiter;
}

class mililiterToLiter : public QWidget
{
    Q_OBJECT

public:
    explicit mililiterToLiter(QWidget *parent = nullptr);
    ~mililiterToLiter();

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();

private:
    Ui::mililiterToLiter *ui;
};

#endif // MILILITERTOLITER_H
