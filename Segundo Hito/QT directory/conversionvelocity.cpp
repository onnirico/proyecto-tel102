#include "conversionvelocity.h"
#include "ui_conversionvelocity.h"

conversionVelocity::conversionVelocity(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::conversionVelocity)
{
    ui->setupUi(this);
}

void conversionVelocity::on_pushButton_clicked()
{
    this->close(); // Cierra solo la ventana actual
}


conversionVelocity::~conversionVelocity()
{
    delete ui;
}
