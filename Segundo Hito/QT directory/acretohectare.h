#ifndef ACRETOHECTARE_H
#define ACRETOHECTARE_H

#include <QWidget>

namespace Ui {
class acreToHectare;
}

class acreToHectare : public QWidget
{
    Q_OBJECT

public:
    explicit acreToHectare(QWidget *parent = nullptr);
    ~acreToHectare();

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();

private:
    Ui::acreToHectare *ui;
};

#endif // ACRETOHECTARE_H
