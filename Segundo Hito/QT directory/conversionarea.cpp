#include "conversionarea.h"
#include "ui_conversionarea.h"
#include "squaremetertosquarefoot.h"
#include "squarefoottosquaremeter.h"
#include "squarekilometertosquareacre.h"
#include "acretohectare.h"
#include "squaremiletosquarekilometer.h"
#include "squareinchtosquarecentimeter.h"
#include "squareyardtosquaremeter.h"

conversionArea::conversionArea(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::conversionArea)
{
    ui->setupUi(this);
}

void conversionArea::on_pushButton_clicked()
{
    this->close(); // Cierra solo la ventana actual
}

void conversionArea::on_pushButton_2_clicked()
{
    squareMeterToSquareFoot *squareMeterToSquareFootWindow = new squareMeterToSquareFoot();
    squareMeterToSquareFootWindow->show();
}

void conversionArea::on_pushButton_3_clicked()
{
    squareFootToSquareMeter *squareFootToSquareMeterWindow = new squareFootToSquareMeter();
    squareFootToSquareMeterWindow->show();
}

void conversionArea::on_pushButton_4_clicked()
{
    squareKilometerToSquareAcre *squareKilometerToSquareAcreWindow = new squareKilometerToSquareAcre();
    squareKilometerToSquareAcreWindow->show();
}


void conversionArea::on_pushButton_5_clicked()
{
    acreToHectare *acreToHectareWindow = new acreToHectare();
    acreToHectareWindow->show();
}

void conversionArea::on_pushButton_6_clicked()
{
    squareMileToSquareKilometer *SquareMileToSquareKilometerWindow = new squareMileToSquareKilometer();
    SquareMileToSquareKilometerWindow->show();
}

void conversionArea::on_pushButton_7_clicked()
{
    squareInchToSquareCentimeter *squareInchToSquareCentimeterWindow = new squareInchToSquareCentimeter();
    squareInchToSquareCentimeterWindow->show();
}

void conversionArea::on_pushButton_8_clicked()
{
    squareYardToSquareMeter *squareYardToSquareMeterWindow = new squareYardToSquareMeter();
    squareYardToSquareMeterWindow->show();
}


conversionArea::~conversionArea()
{
    delete ui;
}
