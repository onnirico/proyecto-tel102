#include "squaremiletosquarekilometer.h"
#include "ui_squaremiletosquarekilometer.h"

squareMileToSquareKilometer::squareMileToSquareKilometer(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::squareMileToSquareKilometer)
{
    ui->setupUi(this);
    setWindowTitle("Conversión de Millas Cuadradas a Kilómetros Cuadrados"); // Cambia el título de la ventana

    // Configura la etiqueta de resultado a 0.00 al inicio
    ui->label_3->setText("0.00");
}

squareMileToSquareKilometer::~squareMileToSquareKilometer()
{
    delete ui;
}

void squareMileToSquareKilometer::on_pushButton_clicked()
{
    this->close();
}

void squareMileToSquareKilometer::on_pushButton_2_clicked()
{
    double squareMiles = ui->doubleSpinBox->value(); // Obtén el valor en millas cuadradas
    double squareKilometers = squareMiles * 2.58999; // Convierte millas cuadradas a kilómetros cuadrados
    ui->label_3->setText(QString::number(squareKilometers, 'f', 2)); // Muestra el resultado en el QLabel
    ui->doubleSpinBox->setValue(0.0); // Establece el valor en la QDoubleSpinBox a cero
}
