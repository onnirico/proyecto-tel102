#ifndef SQUAREFOOTTOSQUAREMETER_H
#define SQUAREFOOTTOSQUAREMETER_H

#include <QWidget>

namespace Ui {
class squareFootToSquareMeter;
}

class squareFootToSquareMeter : public QWidget
{
    Q_OBJECT

public:
    explicit squareFootToSquareMeter(QWidget *parent = nullptr);
    ~squareFootToSquareMeter();

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();

private:
    Ui::squareFootToSquareMeter *ui;
};

#endif // SQUAREFOOTTOSQUAREMETER_H
