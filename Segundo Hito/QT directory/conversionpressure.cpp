#include "conversionpressure.h"
#include "ui_conversionpressure.h"

conversionPressure::conversionPressure(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::conversionPressure)
{
    ui->setupUi(this);
}

void conversionPressure::on_pushButton_clicked()
{
    this->close(); // Cierra solo la ventana actual
}


conversionPressure::~conversionPressure()
{
    delete ui;
}
