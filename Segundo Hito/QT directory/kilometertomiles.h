#ifndef KILOMETERTOMILES_H
#define KILOMETERTOMILES_H

#include <QWidget>

namespace Ui {
class kilometerToMiles;
}

class kilometerToMiles : public QWidget
{
    Q_OBJECT

public:
    explicit kilometerToMiles(QWidget *parent = nullptr);
    ~kilometerToMiles();

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();

private:
    Ui::kilometerToMiles *ui;
};


#endif // KILOMETERTOMILES_H
