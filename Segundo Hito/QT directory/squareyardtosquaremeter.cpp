#include "squareyardtosquaremeter.h"
#include "ui_squareyardtosquaremeter.h"

squareYardToSquareMeter::squareYardToSquareMeter(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::squareYardToSquareMeter)
{
    ui->setupUi(this);
    setWindowTitle("Conversión de Yardas Cuadradas a Metros Cuadrados"); // Cambia el título de la ventana

    // Configura la etiqueta de resultado a 0.00 al inicio
    ui->label_3->setText("0.00");
}

squareYardToSquareMeter::~squareYardToSquareMeter()
{
    delete ui;
}

void squareYardToSquareMeter::on_pushButton_clicked()
{
    this->close();
}

void squareYardToSquareMeter::on_pushButton_2_clicked()
{
    double squareYards = ui->doubleSpinBox->value(); // Obtén el valor en yardas cuadradas
    double squareMeters = squareYards * 0.836127; // Convierte yardas cuadradas a metros cuadrados
    ui->label_3->setText(QString::number(squareMeters, 'f', 2)); // Muestra el resultado en el QLabel
    ui->doubleSpinBox->setValue(0.0); // Establece el valor en la QDoubleSpinBox a cero
}
