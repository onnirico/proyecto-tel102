QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    acretohectare.cpp \
    centimetertoinches.cpp \
    conversionarea.cpp \
    conversionenergy.cpp \
    conversionlength.cpp \
    conversionpressure.cpp \
    conversiontemperature.cpp \
    conversiontime.cpp \
    conversionvelocity.cpp \
    conversionvolume.cpp \
    conversionwindows.cpp \
    cubicfoottocubicmeter.cpp \
    cubicmetertocubicfoot.cpp \
    feettometer.cpp \
    galtoliter.cpp \
    kilometertomiles.cpp \
    litertogal.cpp \
    litertomililiter.cpp \
    main.cpp \
    dialog.cpp \
    metertofeet.cpp \
    milestokilometer.cpp \
    mililitertoliter.cpp \
    mililitertooz.cpp \
    oztomililiter.cpp \
    physicwindow.cpp \
    squarefoottosquaremeter.cpp \
    squareinchtosquarecentimeter.cpp \
    squarekilometertosquareacre.cpp \
    squaremetertosquarefoot.cpp \
    squaremiletosquarekilometer.cpp \
    squareyardtosquaremeter.cpp

HEADERS += \
    acretohectare.h \
    centimetertoinches.h \
    conversionarea.h \
    conversionenergy.h \
    conversionlength.h \
    conversionpressure.h \
    conversiontemperature.h \
    conversiontime.h \
    conversionvelocity.h \
    conversionvolume.h \
    conversionwindows.h \
    cubicfoottocubicmeter.h \
    cubicmetertocubicfoot.h \
    dialog.h \
    feettometer.h \
    galtoliter.h \
    kilometertomiles.h \
    litertogal.h \
    litertomililiter.h \
    metertofeet.h \
    milestokilometer.h \
    mililitertoliter.h \
    mililitertooz.h \
    oztomililiter.h \
    physicwindow.h \
    squarefoottosquaremeter.h \
    squareinchtosquarecentimeter.h \
    squarekilometertosquareacre.h \
    squaremetertosquarefoot.h \
    squaremiletosquarekilometer.h \
    squareyardtosquaremeter.h

FORMS += \
    acretohectare.ui \
    centimetertoinches.ui \
    conversionarea.ui \
    conversionenergy.ui \
    conversionlength.ui \
    conversionpressure.ui \
    conversiontemperature.ui \
    conversiontime.ui \
    conversionvelocity.ui \
    conversionvolume.ui \
    conversionwindows.ui \
    cubicfoottocubicmeter.ui \
    cubicmetertocubicfoot.ui \
    dialog.ui \
    feettometer.ui \
    galtoliter.ui \
    kilometertomiles.ui \
    litertogal.ui \
    litertomililiter.ui \
    metertofeet.ui \
    milestokilometer.ui \
    mililitertoliter.ui \
    mililitertooz.ui \
    oztomililiter.ui \
    physicwindow.ui \
    squarefoottosquaremeter.ui \
    squareinchtosquarecentimeter.ui \
    squarekilometertosquareacre.ui \
    squaremetertosquarefoot.ui \
    squaremiletosquarekilometer.ui \
    squareyardtosquaremeter.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
