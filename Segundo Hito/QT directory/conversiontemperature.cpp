#include "conversiontemperature.h"
#include "ui_conversiontemperature.h"

conversionTemperature::conversionTemperature(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::conversionTemperature)
{
    ui->setupUi(this);
}

void conversionTemperature::on_pushButton_clicked()
{
    this->close(); // Cierra solo la ventana actual
}


conversionTemperature::~conversionTemperature()
{
    delete ui;
}
