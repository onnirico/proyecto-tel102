#include "centimetertoinches.h"
#include "ui_centimetertoinches.h"

centimeterToInches::centimeterToInches(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::centimeterToInches)
{
    ui->setupUi(this);
    setWindowTitle("Centímetros a Pulgadas"); // Cambia el título de la ventana

    // Configura la etiqueta de resultado a 0.00 al inicio
    ui->label_3->setText("0.00");
}

centimeterToInches::~centimeterToInches()
{
    delete ui;
}

void centimeterToInches::on_pushButton_clicked()
{
    this->close();
}

void centimeterToInches::on_pushButton_2_clicked()
{
    double centimeters = ui->doubleSpinBox->value(); // Obtén el valor en centímetros
    double inches = centimeters * 0.393701; // Convierte centímetros a pulgadas
    ui->label_3->setText(QString::number(inches, 'f', 2)); // Muestra el resultado en el QLabel
    ui->doubleSpinBox->setValue(0.0); // Establece el valor en la QDoubleSpinBox a cero
}
