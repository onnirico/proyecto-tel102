#include "squaremetertosquarefoot.h"
#include "ui_squaremetertosquarefoot.h"

squareMeterToSquareFoot::squareMeterToSquareFoot(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::squareMeterToSquareFoot)
{
    ui->setupUi(this);
    setWindowTitle("Conversión de Metros Cuadrados a Pies Cuadrados"); // Cambia el título de la ventana

    // Configura la etiqueta de resultado a 0.00 al inicio
    ui->label_3->setText("0.00");
}

squareMeterToSquareFoot::~squareMeterToSquareFoot()
{
    delete ui;
}

void squareMeterToSquareFoot::on_pushButton_clicked()
{
    this->close();
}

void squareMeterToSquareFoot::on_pushButton_2_clicked()
{
    double squareMeters = ui->doubleSpinBox->value(); // Obtén el valor en metros cuadrados
    double squareFeet = squareMeters * 10.7639; // Convierte metros cuadrados a pies cuadrados
    ui->label_3->setText(QString::number(squareFeet, 'f', 2)); // Muestra el resultado en el QLabel
    ui->doubleSpinBox->setValue(0.0); // Establece el valor en la QDoubleSpinBox a cero
}
