#ifndef CUBICMETERTOCUBICFOOT_H
#define CUBICMETERTOCUBICFOOT_H

#include <QWidget>

namespace Ui {
class cubicMeterToCubicFoot;
}

class cubicMeterToCubicFoot : public QWidget
{
    Q_OBJECT

public:
    explicit cubicMeterToCubicFoot(QWidget *parent = nullptr);
    ~cubicMeterToCubicFoot();

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();

private:
    Ui::cubicMeterToCubicFoot *ui;
};

#endif // CUBICMETERTOCUBICFOOT_H
