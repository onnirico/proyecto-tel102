#ifndef CUBICFOOTTOCUBICMETER_H
#define CUBICFOOTTOCUBICMETER_H

#include <QWidget>

namespace Ui {
class cubicFootToCubicMeter;
}

class cubicFootToCubicMeter : public QWidget
{
    Q_OBJECT

public:
    explicit cubicFootToCubicMeter(QWidget *parent = nullptr);
    ~cubicFootToCubicMeter();

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();

private:
    Ui::cubicFootToCubicMeter *ui;
};

#endif // CUBICFOOTTOCUBICMETER_H
