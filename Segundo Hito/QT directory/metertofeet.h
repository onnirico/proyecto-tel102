#ifndef METERTOFEET_H
#define METERTOFEET_H

#include <QWidget>

namespace Ui {
class length;
}

class length : public QWidget
{
    Q_OBJECT

public:
    explicit length(QWidget *parent = nullptr);
    ~length();

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();

private:
    Ui::length *ui;

};

#endif // METERTOFEET_H


