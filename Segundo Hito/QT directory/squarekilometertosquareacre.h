#ifndef SQUAREKILOMETERTOSQUAREACRE_H
#define SQUAREKILOMETERTOSQUAREACRE_H

#include <QWidget>

namespace Ui {
class squareKilometerToSquareAcre;
}

class squareKilometerToSquareAcre : public QWidget
{
    Q_OBJECT

public:
    explicit squareKilometerToSquareAcre(QWidget *parent = nullptr);
    ~squareKilometerToSquareAcre();

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();

private:
    Ui::squareKilometerToSquareAcre *ui;
};

#endif // SQUAREKILOMETERTOSQUAREACRE_H
