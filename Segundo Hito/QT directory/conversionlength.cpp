#include "conversionLength.h"
#include "ui_conversionLength.h"
#include "metertofeet.h"
#include "feettometer.h"
#include "kilometertomiles.h"
#include "milestokilometer.h"
#include "centimetertoinches.h"

conversionLength::conversionLength(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::conversionLength)
{
    ui->setupUi(this);
}

void conversionLength::on_pushButton_clicked()
{
    this->close();
}

void conversionLength::on_pushButton_2_clicked()
{
    length *lengthWindow = new length();
    lengthWindow->show();
}

void conversionLength::on_pushButton_3_clicked()
{
    feetToMeter *feetToMeterWindow = new feetToMeter();
    feetToMeterWindow->show();
}

void conversionLength::on_pushButton_4_clicked()
{
    kilometerToMiles *kilometerToMilesWindow = new kilometerToMiles();
    kilometerToMilesWindow->show();
}
void conversionLength::on_pushButton_5_clicked()
{
    milesToKilometer *milesToKilometerWindow = new milesToKilometer();
    milesToKilometerWindow->show();
}

void conversionLength::on_pushButton_6_clicked()
{
    centimeterToInches *centimeterToInchesWindow = new centimeterToInches();
    centimeterToInchesWindow->show();
}

conversionLength::~conversionLength()
{
    delete ui;
}
