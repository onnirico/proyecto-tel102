#ifndef CENTIMETERTOINCHES_H
#define CENTIMETERTOINCHES_H

#include <QWidget>

namespace Ui {
class centimeterToInches;
}

class centimeterToInches : public QWidget
{
    Q_OBJECT

public:
    explicit centimeterToInches(QWidget *parent = nullptr);
    ~centimeterToInches();

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();

private:
    Ui::centimeterToInches *ui;
};

#endif // CENTIMETERTOINCHES_H
