#ifndef GALTOLITER_H
#define GALTOLITER_H

#include <QWidget>

namespace Ui {
class galToLiter;
}

class galToLiter : public QWidget
{
    Q_OBJECT

public:
    explicit galToLiter(QWidget *parent = nullptr);
    ~galToLiter();

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();

private:
    Ui::galToLiter *ui;
};

#endif // GALTOLITER_H

