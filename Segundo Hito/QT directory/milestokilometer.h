#ifndef MILESTOKILOMETER_H
#define MILESTOKILOMETER_H

#include <QWidget>

namespace Ui {
class milesToKilometer;
}

class milesToKilometer : public QWidget
{
    Q_OBJECT

public:
    explicit milesToKilometer(QWidget *parent = nullptr);
    ~milesToKilometer();

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();

private:
    Ui::milesToKilometer *ui;
};

#endif // MILESTOKILOMETER_H
