#include "squareinchtosquarecentimeter.h"
#include "ui_squareinchtosquarecentimeter.h"

squareInchToSquareCentimeter::squareInchToSquareCentimeter(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::squareInchToSquareCentimeter)
{
    ui->setupUi(this);
    setWindowTitle("Conversión de Pulgadas Cuadradas a Centímetros Cuadrados"); // Cambia el título de la ventana

    // Configura la etiqueta de resultado a 0.00 al inicio
    ui->label_3->setText("0.00");
}

squareInchToSquareCentimeter::~squareInchToSquareCentimeter()
{
    delete ui;
}

void squareInchToSquareCentimeter::on_pushButton_clicked()
{
    this->close();
}

void squareInchToSquareCentimeter::on_pushButton_2_clicked()
{
    double squareInches = ui->doubleSpinBox->value(); // Obtén el valor en pulgadas cuadradas
    double squareCentimeters = squareInches * 6.4516; // Convierte pulgadas cuadradas a centímetros cuadrados
    ui->label_3->setText(QString::number(squareCentimeters, 'f', 2)); // Muestra el resultado en el QLabel
    ui->doubleSpinBox->setValue(0.0); // Establece el valor en la QDoubleSpinBox a cero
}
