#include "milestokilometer.h"
#include "ui_milestokilometer.h"

milesToKilometer::milesToKilometer(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::milesToKilometer)
{
    ui->setupUi(this);
    setWindowTitle("Millas a kilometros"); // Cambia el título de la ventana

    // Configura la etiqueta de resultado a 0.00 al inicio
    ui->label_3->setText("0.00");
}

milesToKilometer::~milesToKilometer()
{
    delete ui;
}

void milesToKilometer::on_pushButton_clicked()
{
    this->close();
}

void milesToKilometer::on_pushButton_2_clicked()
{
    double miles = ui->doubleSpinBox->value(); // Obtén el valor en millas
    double kilometers = miles * 1.60934; // Convierte millas a kilómetros
    ui->label_3->setText(QString::number(kilometers)); // Muestra el resultado en el QLabel
    ui->doubleSpinBox->setValue(0.0); // Establece el valor en la QDoubleSpinBox a cero
}
