#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_pushButton_2_clicked()
{
    physicWindow *physicsWindow = new physicWindow(); // Crea una nueva instancia de PhysicsWindow
    physicsWindow->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    physicsWindow->show(); // Muestra la ventana de física
}

void Dialog::on_pushButton_3_clicked()
{
    conversionWindows *conversionWindow = new conversionWindows(); // Crea una nueva instancia de ConversionWindow
    conversionWindow->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    conversionWindow->show(); // Muestra la ventana de conversión
}

void Dialog::on_pushButton_clicked()
{
    QApplication::quit(); // Cierra la aplicación
}
