#include "squarefoottosquaremeter.h"
#include "ui_squarefoottosquaremeter.h"

squareFootToSquareMeter::squareFootToSquareMeter(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::squareFootToSquareMeter)
{
    ui->setupUi(this);
    setWindowTitle("Conversión de Pies Cuadrados a Metros Cuadrados"); // Cambia el título de la ventana

    // Configura la etiqueta de resultado a 0.00 al inicio
    ui->label_3->setText("0.00");
}

squareFootToSquareMeter::~squareFootToSquareMeter()
{
    delete ui;
}

void squareFootToSquareMeter::on_pushButton_clicked()
{
    this->close();
}

void squareFootToSquareMeter::on_pushButton_2_clicked()
{
    double squareFeet = ui->doubleSpinBox->value(); // Obtén el valor en pies cuadrados
    double squareMeters = squareFeet * 0.092903; // Convierte pies cuadrados a metros cuadrados
    ui->label_3->setText(QString::number(squareMeters, 'f', 2)); // Muestra el resultado en el QLabel
    ui->doubleSpinBox->setValue(0.0); // Establece el valor en la QDoubleSpinBox a cero
}
