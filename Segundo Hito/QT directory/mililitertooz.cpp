#include "mililitertooz.h"
#include "ui_mililitertooz.h"

mililiterToOz::mililiterToOz(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::mililiterToOz)
{
    ui->setupUi(this);
    setWindowTitle("Conversión de Mililitros [mL] a Onzas Fluidas [fl oz]");
        ui->label_3->setText("0.00");
}

mililiterToOz::~mililiterToOz()
{
    delete ui;
}

void mililiterToOz::on_pushButton_clicked()
{
    this->close();
}

void mililiterToOz::on_pushButton_2_clicked()
{
    double milliliters = ui->doubleSpinBox->value();
    double fluidOunces = milliliters * 0.033814;
    ui->label_3->setText(QString::number(fluidOunces, 'f', 2));
    ui->doubleSpinBox->setValue(0.0);
}
