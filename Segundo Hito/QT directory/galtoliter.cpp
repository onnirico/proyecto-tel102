#include "galtoliter.h"
#include "ui_galtoliter.h"

galToLiter::galToLiter(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::galToLiter)
{
    ui->setupUi(this);
    setWindowTitle("Conversión de Galones [gal] a Litros [L]");
        ui->label_3->setText("0.00");
}

galToLiter::~galToLiter()
{
    delete ui;
}

void galToLiter::on_pushButton_clicked()
{
    this->close();
}

void galToLiter::on_pushButton_2_clicked()
{
    double gallons = ui->doubleSpinBox->value();
    double liters = gallons * 3.78541;
    ui->label_3->setText(QString::number(liters, 'f', 2));
    ui->doubleSpinBox->setValue(0.0);
}
