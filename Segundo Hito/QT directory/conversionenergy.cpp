#include "conversionenergy.h"
#include "ui_conversionenergy.h"

conversionEnergy::conversionEnergy(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::conversionEnergy)
{
    ui->setupUi(this);
}

void conversionEnergy::on_pushButton_clicked()
{
    this->close(); // Cierra solo la ventana actual
}


conversionEnergy::~conversionEnergy()
{
    delete ui;
}
