#ifndef CONVERSIONPRESSURE_H
#define CONVERSIONPRESSURE_H

#include <QWidget>

namespace Ui {
class conversionPressure;
}

class conversionPressure : public QWidget
{
    Q_OBJECT

public:
    explicit conversionPressure(QWidget *parent = nullptr);
    ~conversionPressure();

private slots:
    void on_pushButton_clicked();    // Función para cerrar la aplicación

private:
    Ui::conversionPressure *ui;
};

#endif // CONVERSIONPRESSURE_H
