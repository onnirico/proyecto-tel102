#ifndef CONVERSIONAREA_H
#define CONVERSIONAREA_H

#include <QWidget>

namespace Ui {
class conversionArea;
}

class conversionArea : public QWidget
{
    Q_OBJECT

public:
    explicit conversionArea(QWidget *parent = nullptr);
    ~conversionArea();

private slots:
    void on_pushButton_clicked();    // Función para cerrar la aplicación
    void on_pushButton_2_clicked();
    void on_pushButton_3_clicked();
    void on_pushButton_4_clicked();
    void on_pushButton_5_clicked();
    void on_pushButton_6_clicked();
    void on_pushButton_7_clicked();
    void on_pushButton_8_clicked();

private:
    Ui::conversionArea *ui;
};

#endif // CONVERSIONAREA_H
