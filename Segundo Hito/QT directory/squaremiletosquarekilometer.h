#ifndef SQUAREMILETOSQUAREKILOMETER_H
#define SQUAREMILETOSQUAREKILOMETER_H

#include <QWidget>

namespace Ui {
class squareMileToSquareKilometer;
}

class squareMileToSquareKilometer : public QWidget
{
    Q_OBJECT

public:
    explicit squareMileToSquareKilometer(QWidget *parent = nullptr);
    ~squareMileToSquareKilometer();

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();

private:
    Ui::squareMileToSquareKilometer *ui;
};

#endif // SQUAREMILETOSQUAREKILOMETER_H
