#ifndef SQUAREMETERTOSQUAREFOOT_H
#define SQUAREMETERTOSQUAREFOOT_H

#include <QWidget>

namespace Ui {
class squareMeterToSquareFoot;
}

class squareMeterToSquareFoot : public QWidget
{
    Q_OBJECT

public:
    explicit squareMeterToSquareFoot(QWidget *parent = nullptr);
    ~squareMeterToSquareFoot();

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();

private:
    Ui::squareMeterToSquareFoot *ui;
};

#endif // SQUAREMETERTOSQUAREFOOT_H
