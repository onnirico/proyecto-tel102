#include "litertogal.h"
#include "ui_litertogal.h"

literToGal::literToGal(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::literToGal)
{
    ui->setupUi(this);
    setWindowTitle("Conversión de Litros [L] a Galones [gal]");
        ui->label_3->setText("0.00");
}

literToGal::~literToGal()
{
    delete ui;
}

void literToGal::on_pushButton_clicked()
{
    this->close();
}

void literToGal::on_pushButton_2_clicked()
{
    double liters = ui->doubleSpinBox->value();
    double gallons = liters / 3.78541;
    ui->label_3->setText(QString::number(gallons, 'f', 2));
    ui->doubleSpinBox->setValue(0.0);
}
