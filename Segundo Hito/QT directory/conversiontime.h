#ifndef CONVERSIONTIME_H
#define CONVERSIONTIME_H

#include <QWidget>

namespace Ui {
class conversionTime;
}

class conversionTime : public QWidget
{
    Q_OBJECT

public:
    explicit conversionTime(QWidget *parent = nullptr);
    ~conversionTime();

private slots:
    void on_pushButton_clicked();    // Función para cerrar la aplicación

private:
    Ui::conversionTime *ui;
};

#endif // CONVERSIONTIME_H
