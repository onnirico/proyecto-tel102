#ifndef SQUAREYARDTOSQUAREMETER_H
#define SQUAREYARDTOSQUAREMETER_H

#include <QWidget>

namespace Ui {
class squareYardToSquareMeter;
}

class squareYardToSquareMeter : public QWidget
{
    Q_OBJECT

public:
    explicit squareYardToSquareMeter(QWidget *parent = nullptr);
    ~squareYardToSquareMeter();

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();

private:
    Ui::squareYardToSquareMeter *ui;
};

#endif // SQUAREYARDTOSQUAREMETER_H
