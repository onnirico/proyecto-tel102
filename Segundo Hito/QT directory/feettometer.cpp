#include "feettometer.h"
#include "ui_feettometer.h"

feetToMeter::feetToMeter(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::feetToMeter)
{
    ui->setupUi(this);
    setWindowTitle("Pies a Metros"); // Cambia el título de la ventana

    // Configura la etiqueta de resultado a 0.00 al inicio
    ui->label_3->setText("0.00");
}

feetToMeter::~feetToMeter()
{
    delete ui;
}

void feetToMeter::on_pushButton_clicked()
{
    this->close(); // Cierra la ventana actual
}

void feetToMeter::on_pushButton_2_clicked()
{
    double feet = ui->doubleSpinBox->value(); // Obtiene el valor en pies
    double meters = feet / 3.28084; // Convierte pies a metros
    ui->label_3->setText(QString::number(meters)); // Muestra el resultado en la etiqueta
    ui->doubleSpinBox->setValue(0.0); // Restablece el valor en la QDoubleSpinBox a cero
}
