#include "metertofeet.h"
#include "ui_metertofeet.h"

length::length(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::length)
{
    ui->setupUi(this);
    setWindowTitle("Metros a pies");
    ui->label_3->setText("0.00");
}

length::~length()
{
    delete ui;
}

void length::on_pushButton_clicked()
{
    this->close();
}

void length::on_pushButton_2_clicked()
{
    double meters = ui->doubleSpinBox->value(); // Obtén el valor en metros
    ui->label_3->setText(QString::number(meters * 3.28084)); // Aplica el factor de conversión y muestra el valor en el QLabel
    ui->doubleSpinBox->setValue(0.0); // Establece el valor en la QDoubleSpinBox a cero
}
