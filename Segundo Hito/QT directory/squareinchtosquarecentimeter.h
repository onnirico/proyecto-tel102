#ifndef SQUAREINCHTOSQUARECENTIMETER_H
#define SQUAREINCHTOSQUARECENTIMETER_H

#include <QWidget>

namespace Ui {
class squareInchToSquareCentimeter;
}

class squareInchToSquareCentimeter : public QWidget
{
    Q_OBJECT

public:
    explicit squareInchToSquareCentimeter(QWidget *parent = nullptr);
    ~squareInchToSquareCentimeter();

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();

private:
    Ui::squareInchToSquareCentimeter *ui;
};

#endif // SQUAREINCHTOSQUARECENTIMETER_H

