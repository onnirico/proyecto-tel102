#ifndef OZTOMILILITER_H
#define OZTOMILILITER_H

#include <QWidget>

namespace Ui {
class ozToMililiter;
}

class ozToMililiter : public QWidget
{
    Q_OBJECT

public:
    explicit ozToMililiter(QWidget *parent = nullptr);
    ~ozToMililiter();

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();

private:
    Ui::ozToMililiter *ui;
};

#endif // OZTOMILILITER_H
