#ifndef CONVERSIONVELOCITY_H
#define CONVERSIONVELOCITY_H

#include <QWidget>

namespace Ui {
class conversionVelocity;
}

class conversionVelocity : public QWidget
{
    Q_OBJECT

public:
    explicit conversionVelocity(QWidget *parent = nullptr);
    ~conversionVelocity();

private slots:
    void on_pushButton_clicked();    // Función para cerrar la aplicación

private:
    Ui::conversionVelocity *ui;
};

#endif // CONVERSIONVELOCITY_H
