#ifndef MILILITERTOOZ_H
#define MILILITERTOOZ_H

#include <QWidget>

namespace Ui {
class mililiterToOz;
}

class mililiterToOz : public QWidget
{
    Q_OBJECT

public:
    explicit mililiterToOz(QWidget *parent = nullptr);
    ~mililiterToOz();

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();

private:
    Ui::mililiterToOz *ui;
};

#endif // MILILITERTOOZ_H

