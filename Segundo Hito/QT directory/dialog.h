#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include "conversionwindows.h"
#include "physicwindow.h"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = nullptr);
    ~Dialog();

private slots:
    void on_pushButton_2_clicked(); // Función para abrir ConversionWindow
    void on_pushButton_3_clicked(); // Función para abrir PhysicsWindow
    void on_pushButton_clicked();    // Función para cerrar la aplicación

private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H

