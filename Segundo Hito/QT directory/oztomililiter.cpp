#include "oztomililiter.h"
#include "ui_oztomililiter.h"

ozToMililiter::ozToMililiter(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ozToMililiter)
{
    ui->setupUi(this);
    setWindowTitle("Conversión de Onzas Fluidas [fl oz] a Mililitros [mL]");
        ui->label_3->setText("0.00");
}

ozToMililiter::~ozToMililiter()
{
    delete ui;
}

void ozToMililiter::on_pushButton_clicked()
{
    this->close();
}

void ozToMililiter::on_pushButton_2_clicked()
{
    double fluidOunces = ui->doubleSpinBox->value();
    double milliliters = fluidOunces / 0.033814;
    ui->label_3->setText(QString::number(milliliters, 'f', 2));
    ui->doubleSpinBox->setValue(0.0);
}
