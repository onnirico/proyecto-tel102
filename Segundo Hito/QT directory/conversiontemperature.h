#ifndef CONVERSIONTEMPERATURE_H
#define CONVERSIONTEMPERATURE_H

#include <QWidget>

namespace Ui {
class conversionTemperature;
}

class conversionTemperature : public QWidget
{
    Q_OBJECT

public:
    explicit conversionTemperature(QWidget *parent = nullptr);
    ~conversionTemperature();

private slots:
    void on_pushButton_clicked();    // Función para cerrar la aplicación

private:
    Ui::conversionTemperature *ui;
};

#endif // CONVERSIONTEMPERATURE_H
