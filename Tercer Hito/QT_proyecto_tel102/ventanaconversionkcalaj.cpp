#include "ventanaconversionkcalaj.h"
#include "ui_ventanaconversionkcalaj.h"

ventanaConversionkCalAJ::ventanaConversionkCalAJ(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionkCalAJ)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionkCalAJ::on_pushButton_clicked()
{
    close();
}

void ventanaConversionkCalAJ::on_pushButton_clicked_2()
{
    // Obtiene el valor en kilocalorías desde la QDoubleSpinBox
    double kcal = ui->doubleSpinBox->value();

    // Convierte kilocalorías a julios
    double julios = kcal * 4184.0;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(julios));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionkCalAJ::~ventanaConversionkCalAJ()
{
    delete ui;
}
