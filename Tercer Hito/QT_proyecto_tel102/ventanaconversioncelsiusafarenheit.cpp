#include "ventanaconversioncelsiusafarenheit.h"
#include "ui_ventanaconversioncelsiusafarenheit.h"

ventanaConversionCelsiusAFarenheit::ventanaConversionCelsiusAFarenheit(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionCelsiusAFarenheit)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionCelsiusAFarenheit::on_pushButton_clicked()
{
    close();
}

void ventanaConversionCelsiusAFarenheit::on_pushButton_clicked_2()
{
    // Obtiene el valor en grados Celsius desde la QDoubleSpinBox
    double celsius = ui->doubleSpinBox->value();

    // Convierte grados Celsius a Fahrenheit utilizando la fórmula (F = C * 9/5 + 32)
    double fahrenheit = celsius * 9.0 / 5.0 + 32.0;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(fahrenheit));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionCelsiusAFarenheit::~ventanaConversionCelsiusAFarenheit()
{
    delete ui;
}
