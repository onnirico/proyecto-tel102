#ifndef VENTANACONVERSIONKMHAKNOTS_H
#define VENTANACONVERSIONKMHAKNOTS_H

#include <QWidget>

namespace Ui {
class ventanaConversionKmHAKnots;
}

class ventanaConversionKmHAKnots : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionKmHAKnots(QWidget *parent = nullptr);
    ~ventanaConversionKmHAKnots();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionKmHAKnots *ui;
};

#endif // VENTANACONVERSIONKMHAKNOTS_H
