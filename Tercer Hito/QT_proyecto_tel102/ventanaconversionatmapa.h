#ifndef VENTANACONVERSIONATMAPA_H
#define VENTANACONVERSIONATMAPA_H

#include <QWidget>

namespace Ui {
class ventanaConversionAtmAPa;
}

class ventanaConversionAtmAPa : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionAtmAPa(QWidget *parent = nullptr);
    ~ventanaConversionAtmAPa();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionAtmAPa *ui;
};

#endif // VENTANACONVERSIONATMAPA_H
