#include "ventanaconversioneskilometrosmillas.h"
#include "ui_ventanaconversioneskilometrosmillas.h"

ventanaConversionesKIlometrosMillas::ventanaConversionesKIlometrosMillas(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionesKIlometrosMillas)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionesKIlometrosMillas::on_pushButton_clicked()
{
    this->close(); // Cierra la ventana actual
}

void ventanaConversionesKIlometrosMillas::on_pushButton_clicked_2()
{
    // Obtiene el valor en kilómetros desde la QDoubleSpinBox
    double kilometers = ui->doubleSpinBox->value();

    // Convierte kilómetros a millas utilizando el factor de conversión (1 km = 0.621371 millas)
    double miles = kilometers * 0.621371;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(miles));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionesKIlometrosMillas::~ventanaConversionesKIlometrosMillas()
{
    delete ui;
}
