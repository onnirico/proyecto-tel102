#include "ventanaconversionesmetroscuadradosapiescuadrados.h"
#include "ui_ventanaconversionesmetroscuadradosapiescuadrados.h"

ventanaConversionesMetrosCuadradosAPiesCuadrados::ventanaConversionesMetrosCuadradosAPiesCuadrados(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionesMetrosCuadradosAPiesCuadrados)
{
    ui->setupUi(this);

    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionesMetrosCuadradosAPiesCuadrados::on_pushButton_clicked()
{
    this->close(); // Cierra la ventana actual
}

void ventanaConversionesMetrosCuadradosAPiesCuadrados::on_pushButton_clicked_2()
{
    // Obtiene el valor en metros cuadrados desde la QDoubleSpinBox
    double squareMeters = ui->doubleSpinBox->value();

    // Convierte metros cuadrados a pies cuadrados utilizando el factor de conversión (1 m^2 = 10.7639 ft^2)
    double squareFeet = squareMeters * 10.7639;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(squareFeet));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}


ventanaConversionesMetrosCuadradosAPiesCuadrados::~ventanaConversionesMetrosCuadradosAPiesCuadrados()
{
    delete ui;
}
