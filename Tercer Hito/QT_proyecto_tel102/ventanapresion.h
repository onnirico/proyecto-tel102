#ifndef VENTANAPRESION_H
#define VENTANAPRESION_H

#include <QWidget>

namespace Ui {
class ventanaPresion;
}

class ventanaPresion : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaPresion(QWidget *parent = nullptr);
    ~ventanaPresion();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();
    void on_pushButton_clicked_3();
    void on_pushButton_clicked_4();
    void on_pushButton_clicked_5();
    void on_pushButton_clicked_6();
    void on_pushButton_clicked_7();
    void on_pushButton_clicked_8();

private:
    Ui::ventanaPresion *ui;
};

#endif // VENTANAPRESION_H
