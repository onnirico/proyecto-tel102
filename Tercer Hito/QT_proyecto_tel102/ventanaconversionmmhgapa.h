#ifndef VENTANACONVERSIONMMHGAPA_H
#define VENTANACONVERSIONMMHGAPA_H

#include <QWidget>

namespace Ui {
class ventanaConversionmmHgAPa;
}

class ventanaConversionmmHgAPa : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionmmHgAPa(QWidget *parent = nullptr);
    ~ventanaConversionmmHgAPa();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionmmHgAPa *ui;
};

#endif // VENTANACONVERSIONMMHGAPA_H
