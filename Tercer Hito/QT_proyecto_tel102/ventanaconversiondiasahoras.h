#ifndef VENTANACONVERSIONDIASAHORAS_H
#define VENTANACONVERSIONDIASAHORAS_H

#include <QWidget>

namespace Ui {
class ventanaConversionDiasAHoras;
}

class ventanaConversionDiasAHoras : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionDiasAHoras(QWidget *parent = nullptr);
    ~ventanaConversionDiasAHoras();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionDiasAHoras *ui;
};

#endif // VENTANACONVERSIONDIASAHORAS_H
