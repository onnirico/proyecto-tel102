#ifndef VENTANACONVERSIONES_H
#define VENTANACONVERSIONES_H

#include <QWidget>

namespace Ui {
class ventanaConversiones;
}

class ventanaConversiones : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversiones(QWidget *parent = nullptr);
    ~ventanaConversiones();

 /*En public slots en el header van colocadas las definiciones de las funciones incluidas*/
public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();
    void on_pushButton_clicked_3();
    void on_pushButton_clicked_4();
    void on_pushButton_clicked_5();
    void on_pushButton_clicked_6();
    void on_pushButton_clicked_7();
    void on_pushButton_clicked_8();
    void on_pushButton_clicked_9();

private:
    Ui::ventanaConversiones *ui;
};

#endif // VENTANACONVERSIONES_Hz
