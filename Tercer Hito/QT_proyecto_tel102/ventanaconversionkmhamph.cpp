#include "ventanaconversionkmhamph.h"
#include "ui_ventanaconversionkmhamph.h"

ventanaConversionKmHAMph::ventanaConversionKmHAMph(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionKmHAMph)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionKmHAMph::on_pushButton_clicked()
{
    close();
}

void ventanaConversionKmHAMph::on_pushButton_clicked_2()
{
    // Obtiene el valor en kilómetros por hora desde la QDoubleSpinBox
    double kmPerHour = ui->doubleSpinBox->value();

    // Convierte kilómetros por hora a millas por hora
    double mph = kmPerHour * 0.621371;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(mph));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionKmHAMph::~ventanaConversionKmHAMph()
{
    delete ui;
}
