#include "ventanaconversioncelsiusakelvin.h"
#include "ui_ventanaconversioncelsiusakelvin.h"

ventanaConversionCelsiusAKelvin::ventanaConversionCelsiusAKelvin(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionCelsiusAKelvin)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionCelsiusAKelvin::on_pushButton_clicked()
{
    close();
}

void ventanaConversionCelsiusAKelvin::on_pushButton_clicked_2()
{
    // Obtiene el valor en grados Celsius desde la QDoubleSpinBox
    double celsius = ui->doubleSpinBox->value();

    // Convierte grados Celsius a Kelvin utilizando la fórmula (K = C + 273.15)
    double kelvin = celsius + 273.15;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(kelvin));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionCelsiusAKelvin::~ventanaConversionCelsiusAKelvin()
{
    delete ui;
}
