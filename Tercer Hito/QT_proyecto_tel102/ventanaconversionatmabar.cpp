#include "ventanaconversionatmabar.h"
#include "ui_ventanaconversionatmabar.h"

ventanaConversionAtmABar::ventanaConversionAtmABar(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionAtmABar)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionAtmABar::on_pushButton_clicked()
{
    close();
}

void ventanaConversionAtmABar::on_pushButton_clicked_2()
{
    // Obtiene el valor en atmósferas desde la QDoubleSpinBox
    double atmospheres = ui->doubleSpinBox->value();

    // Convierte atmósferas a bares (1 atm = 1.01325 bar)
    double bars = atmospheres * 1.01325;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(bars));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}


ventanaConversionAtmABar::~ventanaConversionAtmABar()
{
    delete ui;
}
