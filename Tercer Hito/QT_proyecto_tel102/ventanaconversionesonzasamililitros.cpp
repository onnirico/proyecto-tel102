#include "ventanaconversionesonzasamililitros.h"
#include "ui_ventanaconversionesonzasamililitros.h"

ventanaConversionesOnzasAMililitros::ventanaConversionesOnzasAMililitros(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionesOnzasAMililitros)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionesOnzasAMililitros::on_pushButton_clicked()
{
    this->close(); // Cierra la ventana actual
}

void ventanaConversionesOnzasAMililitros::on_pushButton_clicked_2()
{
    // Obtiene el valor en onzas desde la QDoubleSpinBox
    double onzas = ui->doubleSpinBox->value();

    // Convierte onzas a mililitros utilizando el factor de conversión (1 onza ≈ 29.5735 mililitros)
    double mililitros = onzas * 29.5735;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(mililitros));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionesOnzasAMililitros::~ventanaConversionesOnzasAMililitros()
{
    delete ui;
}
