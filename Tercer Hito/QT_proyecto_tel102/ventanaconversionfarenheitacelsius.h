#ifndef VENTANACONVERSIONFARENHEITACELSIUS_H
#define VENTANACONVERSIONFARENHEITACELSIUS_H

#include <QWidget>

namespace Ui {
class ventanaConversionFarenheitACelsius;
}

class ventanaConversionFarenheitACelsius : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionFarenheitACelsius(QWidget *parent = nullptr);
    ~ventanaConversionFarenheitACelsius();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionFarenheitACelsius *ui;
};

#endif // VENTANACONVERSIONFARENHEITACELSIUS_H
