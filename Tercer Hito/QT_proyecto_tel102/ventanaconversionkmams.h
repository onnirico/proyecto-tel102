#ifndef VENTANACONVERSIONKMAMS_H
#define VENTANACONVERSIONKMAMS_H

#include <QWidget>

namespace Ui {
class ventanaConversionKmAMS;
}

class ventanaConversionKmAMS : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionKmAMS(QWidget *parent = nullptr);
    ~ventanaConversionKmAMS();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionKmAMS *ui;
};

#endif // VENTANACONVERSIONKMAMS_H
