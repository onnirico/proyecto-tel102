#ifndef VENTANACONVERSIONESMILLASKILOMETROS_H
#define VENTANACONVERSIONESMILLASKILOMETROS_H

#include <QWidget>

namespace Ui {
class ventanaConversionesMillasKilometros;
}

class ventanaConversionesMillasKilometros : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionesMillasKilometros(QWidget *parent = nullptr);
    ~ventanaConversionesMillasKilometros();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionesMillasKilometros *ui;
};

#endif // VENTANACONVERSIONESMILLASKILOMETROS_H
