#ifndef VENTANACONVERSIONHORASASEGUNDOS_H
#define VENTANACONVERSIONHORASASEGUNDOS_H

#include <QWidget>

namespace Ui {
class ventanaConversionHorasASegundos;
}

class ventanaConversionHorasASegundos : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionHorasASegundos(QWidget *parent = nullptr);
    ~ventanaConversionHorasASegundos();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionHorasASegundos *ui;
};

#endif // VENTANACONVERSIONHORASASEGUNDOS_H
