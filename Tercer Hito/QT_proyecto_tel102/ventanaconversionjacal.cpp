#include "ventanaconversionjacal.h"
#include "ui_ventanaconversionjacal.h"

ventanaConversionJACal::ventanaConversionJACal(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionJACal)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionJACal::on_pushButton_clicked()
{
    close();
}

void ventanaConversionJACal::on_pushButton_clicked_2()
{
    // Obtiene el valor en julios desde la QDoubleSpinBox
    double julios = ui->doubleSpinBox->value();

    // Convierte julios a calorías utilizando la relación estándar
    double calorias = julios * 0.23885;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(calorias));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionJACal::~ventanaConversionJACal()
{
    delete ui;
}
