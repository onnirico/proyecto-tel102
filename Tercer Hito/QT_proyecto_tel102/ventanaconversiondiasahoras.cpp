#include "ventanaconversiondiasahoras.h"
#include "ui_ventanaconversiondiasahoras.h"

ventanaConversionDiasAHoras::ventanaConversionDiasAHoras(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionDiasAHoras)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionDiasAHoras::on_pushButton_clicked()
{
    close();
}

void ventanaConversionDiasAHoras::on_pushButton_clicked_2()
{
    // Obtiene el valor en días desde la QDoubleSpinBox
    double dias = ui->doubleSpinBox->value();

    // Convierte días a horas (1 día = 24 horas)
    double horas = dias * 24.0;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(horas));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionDiasAHoras::~ventanaConversionDiasAHoras()
{
    delete ui;
}
