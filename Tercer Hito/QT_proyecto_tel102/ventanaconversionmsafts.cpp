#include "ventanaconversionmsafts.h"
#include "ui_ventanaconversionmsafts.h"

ventanaConversionMSAFtS::ventanaConversionMSAFtS(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionMSAFtS)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionMSAFtS::on_pushButton_clicked()
{
    this->close(); // Cierra la ventana actual
}

void ventanaConversionMSAFtS::on_pushButton_clicked_2()
{
    // Obtiene el valor en metros por segundo desde la QDoubleSpinBox
    double mPerSecond = ui->doubleSpinBox->value();

    // Convierte metros por segundo a pies por segundo
    double ftPerSecond = mPerSecond * 3.28084;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(ftPerSecond));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionMSAFtS::~ventanaConversionMSAFtS()
{
    delete ui;
}
