#include "ventanaconversionesmililitrosalitros.h"
#include "ui_ventanaconversionesmililitrosalitros.h"

ventanaConversionesMililitrosALitros::ventanaConversionesMililitrosALitros(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionesMililitrosALitros)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionesMililitrosALitros::on_pushButton_clicked()
{
    close();
}

void ventanaConversionesMililitrosALitros::on_pushButton_clicked_2()
{
    // Obtiene el valor en mililitros desde la QDoubleSpinBox
    double mililitros = ui->doubleSpinBox->value();

    // Convierte mililitros a litros utilizando el factor de conversión (1 litro = 1000 mililitros)
    double litros = mililitros / 1000;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(litros));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionesMililitrosALitros::~ventanaConversionesMililitrosALitros()
{
    delete ui;
}
