#ifndef VENTANACONVERSIONMSAFTS_H
#define VENTANACONVERSIONMSAFTS_H

#include <QWidget>

namespace Ui {
class ventanaConversionMSAFtS;
}

class ventanaConversionMSAFtS : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionMSAFtS(QWidget *parent = nullptr);
    ~ventanaConversionMSAFtS();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionMSAFtS *ui;
};

#endif // VENTANACONVERSIONMSAFTS_H
