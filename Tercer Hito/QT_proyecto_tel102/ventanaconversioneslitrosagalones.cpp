#include "ventanaconversioneslitrosagalones.h"
#include "ui_ventanaconversioneslitrosagalones.h"

ventanaConversionesLitrosAGalones::ventanaConversionesLitrosAGalones(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionesLitrosAGalones)
{
    ui->setupUi(this);

    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionesLitrosAGalones::on_pushButton_clicked()
{
    this->close(); // Cierra la ventana actual
}

void ventanaConversionesLitrosAGalones::on_pushButton_clicked_2()
{
    // Obtiene el valor en litros desde la QDoubleSpinBox
    double litros = ui->doubleSpinBox->value();

    // Convierte litros a galones utilizando el factor de conversión (1 litro = 0.264172 galones)
    double galones = litros * 0.264172;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(galones));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}


ventanaConversionesLitrosAGalones::~ventanaConversionesLitrosAGalones()
{
    delete ui;
}
