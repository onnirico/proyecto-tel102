#ifndef VENTANACONVERSIONSEGUNDOSAMILISEGUNDOS_H
#define VENTANACONVERSIONSEGUNDOSAMILISEGUNDOS_H

#include <QWidget>

namespace Ui {
class ventanaConversionSegundosAMilisegundos;
}

class ventanaConversionSegundosAMilisegundos : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionSegundosAMilisegundos(QWidget *parent = nullptr);
    ~ventanaConversionSegundosAMilisegundos();

public slots:
    void on_pushButton_clicked_3();
    void on_pushButton_clicked_4();

private:
    Ui::ventanaConversionSegundosAMilisegundos *ui;
    long long secondsToMilliseconds(int Valor);
};

#endif // VENTANACONVERSIONSEGUNDOSAMILISEGUNDOS_H
