#include "ventanaconversionesmetrospies.h"
#include "ui_ventanaconversionesmetrospies.h"

ventanaConversionesMetrosPies::ventanaConversionesMetrosPies(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionesMetrosPies)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionesMetrosPies::on_pushButton_clicked()
{
    this->close(); // Cierra la ventana actual
}

void ventanaConversionesMetrosPies::on_pushButton_clicked_2()
{
    // Obtiene el valor en metros desde la QDoubleSpinBox
    double meters = ui->doubleSpinBox->value();

    // Convierte metros a pies utilizando el factor de conversión (1 metro = 3.28084 pies)
    double feet = meters * 3.28084;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(feet));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}



ventanaConversionesMetrosPies::~ventanaConversionesMetrosPies()
{
    delete ui;
}
