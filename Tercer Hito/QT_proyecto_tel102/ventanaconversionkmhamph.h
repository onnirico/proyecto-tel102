#ifndef VENTANACONVERSIONKMHAMPH_H
#define VENTANACONVERSIONKMHAMPH_H

#include <QWidget>

namespace Ui {
class ventanaConversionKmHAMph;
}

class ventanaConversionKmHAMph : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionKmHAMph(QWidget *parent = nullptr);
    ~ventanaConversionKmHAMph();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionKmHAMph *ui;
};

#endif // VENTANACONVERSIONKMHAMPH_H
