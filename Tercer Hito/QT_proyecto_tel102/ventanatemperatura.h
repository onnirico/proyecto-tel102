#ifndef VENTANATEMPERATURA_H
#define VENTANATEMPERATURA_H

#include <QWidget>

namespace Ui {
class ventanaTemperatura;
}

class ventanaTemperatura : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaTemperatura(QWidget *parent = nullptr);
    ~ventanaTemperatura();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();
    void on_pushButton_clicked_3();
    void on_pushButton_clicked_4();
    void on_pushButton_clicked_5();
    void on_pushButton_clicked_6();
    void on_pushButton_clicked_7();



private:
    Ui::ventanaTemperatura *ui;
};

#endif // VENTANATEMPERATURA_H
