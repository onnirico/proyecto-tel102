#ifndef VENTANALONGITUD_H
#define VENTANALONGITUD_H

#include <QWidget>

namespace Ui {
class ventanaLongitud;
}

class ventanaLongitud : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaLongitud(QWidget *parent = nullptr);
    ~ventanaLongitud();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();
    void on_pushButton_clicked_3();
    void on_pushButton_clicked_4();
    void on_pushButton_clicked_5();
    void on_pushButton_clicked_6();


private:
    Ui::ventanaLongitud *ui;
};

#endif // VENTANALONGITUD_H
