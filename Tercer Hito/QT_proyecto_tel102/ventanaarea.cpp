#include "ventanaarea.h"
#include "ui_ventanaarea.h"
#include "ventanaconversionesmetroscuadradosapiescuadrados.h"
#include "ventanaconversionespiescuadradosametroscuadrados.h"
#include "ventanaconversioneskilometroscuadradosaacre.h"
#include "ventanaconversionesacresahectareas.h"
#include "ventanaconversionesmillascuadradasakilometroscuadrados.h"
#include "ventanaconversionespulgadascuadradasacentimetroscuadrados.h"
#include "ventanaconversionesyardascuadradasametroscuadrados.h"


ventanaArea::ventanaArea(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaArea)
{
    ui->setupUi(this);

    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
    connect(ui->pushButton_3, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_3()));
    connect(ui->pushButton_4, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_4()));
    connect(ui->pushButton_5, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_5()));
    connect(ui->pushButton_6, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_6()));
    connect(ui->pushButton_7, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_7()));
    connect(ui->pushButton_8, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_8()));

}

void ventanaArea::on_pushButton_clicked()
{
    close();
}

void ventanaArea::on_pushButton_clicked_2()
{
    ventanaConversionesMetrosCuadradosAPiesCuadrados *ventanaConversionesMetrosCuadradosAPiesCuadrados = new class ventanaConversionesMetrosCuadradosAPiesCuadrados(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionesMetrosCuadradosAPiesCuadrados->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionesMetrosCuadradosAPiesCuadrados->show(); // Muestra la ventana de física
}

void ventanaArea::on_pushButton_clicked_3()
{
    ventanaConversionesPiesCuadradosAMetrosCuadrados *ventanaConversionesPiesCuadradosAMetrosCuadrados = new class ventanaConversionesPiesCuadradosAMetrosCuadrados(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionesPiesCuadradosAMetrosCuadrados->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionesPiesCuadradosAMetrosCuadrados->show(); // Muestra la ventana de física
}

void ventanaArea::on_pushButton_clicked_4()
{
    ventanaConversionesKilometrosCuadradosAAcre *ventanaConversionesKilometrosCuadradosAAcre = new class ventanaConversionesKilometrosCuadradosAAcre(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionesKilometrosCuadradosAAcre->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionesKilometrosCuadradosAAcre->show(); // Muestra la ventana de física
}

void ventanaArea::on_pushButton_clicked_5()
{
    ventanaConversionesAcresAHectareas *ventanaConversionesAcresAHectareas = new class ventanaConversionesAcresAHectareas(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionesAcresAHectareas->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionesAcresAHectareas->show(); // Muestra la ventana de física
}

void ventanaArea::on_pushButton_clicked_6()
{
    ventanaConversionesMillasCuadradasAKilometrosCuadrados *ventanaConversionesMillasCuadradasAKilometrosCuadrados = new class ventanaConversionesMillasCuadradasAKilometrosCuadrados(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionesMillasCuadradasAKilometrosCuadrados->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionesMillasCuadradasAKilometrosCuadrados->show(); // Muestra la ventana de física
}

void ventanaArea::on_pushButton_clicked_7()
{
    ventanaConversionesPulgadasCuadradasACentimetrosCuadrados *ventanaConversionesPulgadasCuadradasACentimetrosCuadrados = new class ventanaConversionesPulgadasCuadradasACentimetrosCuadrados(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionesPulgadasCuadradasACentimetrosCuadrados->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionesPulgadasCuadradasACentimetrosCuadrados->show(); // Muestra la ventana de física
}

void ventanaArea::on_pushButton_clicked_8()
{
    ventanaConversionesYardasCuadradasAMetrosCuadrados *ventanaConversionesYardasCuadradasAMetrosCuadrados = new class ventanaConversionesYardasCuadradasAMetrosCuadrados(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionesYardasCuadradasAMetrosCuadrados->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionesYardasCuadradasAMetrosCuadrados->show(); // Muestra la ventana de física
}

ventanaArea::~ventanaArea()
{
    delete ui;
}
