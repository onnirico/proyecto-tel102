#include "ventanaconversionesmetrocubicoapiecubico.h"
#include "ui_ventanaconversionesmetrocubicoapiecubico.h"

ventanaConversionesMetroCubicoAPieCubico::ventanaConversionesMetroCubicoAPieCubico(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionesMetroCubicoAPieCubico)
{
    ui->setupUi(this);

    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionesMetroCubicoAPieCubico::on_pushButton_clicked()
{
    this->close(); // Cierra la ventana actual
}

void ventanaConversionesMetroCubicoAPieCubico::on_pushButton_clicked_2()
{
    // Obtiene el valor en metros cúbicos desde la QDoubleSpinBox
    double metrosCubicos = ui->doubleSpinBox->value();

    // Convierte metros cúbicos a pies cúbicos utilizando el factor de conversión (1 metro cúbico = 35.3147 pies cúbicos)
    double piesCubicos = metrosCubicos * 35.3147;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(piesCubicos));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}


ventanaConversionesMetroCubicoAPieCubico::~ventanaConversionesMetroCubicoAPieCubico()
{
    delete ui;
}
