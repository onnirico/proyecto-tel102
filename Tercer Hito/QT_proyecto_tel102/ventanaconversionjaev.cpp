#include "ventanaconversionjaev.h"
#include "ui_ventanaconversionjaev.h"

ventanaConversionJAeV::ventanaConversionJAeV(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionJAeV)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionJAeV::on_pushButton_clicked()
{
    close();
}

void ventanaConversionJAeV::on_pushButton_clicked_2()
{
    // Obtiene el valor en julios desde la QDoubleSpinBox
    double julios = ui->doubleSpinBox->value();

    // Convierte julios a electronvoltios
    double electronvoltios = julios * 6.242e18;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(electronvoltios));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}


ventanaConversionJAeV::~ventanaConversionJAeV()
{
    delete ui;
}
