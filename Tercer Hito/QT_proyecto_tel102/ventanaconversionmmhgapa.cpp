#include "ventanaconversionmmhgapa.h"
#include "ui_ventanaconversionmmhgapa.h"

ventanaConversionmmHgAPa::ventanaConversionmmHgAPa(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionmmHgAPa)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionmmHgAPa::on_pushButton_clicked()
{
    this->close(); // Cierra la ventana actual
}
void ventanaConversionmmHgAPa::on_pushButton_clicked_2()
{
    // Obtiene el valor en milímetros de mercurio desde la QDoubleSpinBox
    double mmHg = ui->doubleSpinBox->value();

    // Convierte milímetros de mercurio a pascales utilizando la relación estándar
    double pascals = mmHg * 133.322; // 1 mmHg = 133.322 Pa

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(pascals));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionmmHgAPa::~ventanaConversionmmHgAPa()
{
    delete ui;
}
