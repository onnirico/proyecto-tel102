#include "formulas.h"
#include "ui_formulas.h"

formulas::formulas(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::formulas)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
}

void formulas::on_pushButton_clicked()
{
    close();
}

formulas::~formulas()
{
    delete ui;
}
