#ifndef VENTANACONVERSIONSEMANASAMESES_H
#define VENTANACONVERSIONSEMANASAMESES_H

#include <QWidget>

namespace Ui {
class ventanaConversionSemanasAMeses;
}

class ventanaConversionSemanasAMeses : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionSemanasAMeses(QWidget *parent = nullptr);
    ~ventanaConversionSemanasAMeses();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionSemanasAMeses *ui;
};

#endif // VENTANACONVERSIONSEMANASAMESES_H
