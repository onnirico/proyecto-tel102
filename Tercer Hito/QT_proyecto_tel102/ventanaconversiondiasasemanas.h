#ifndef VENTANACONVERSIONDIASASEMANAS_H
#define VENTANACONVERSIONDIASASEMANAS_H

#include <QWidget>

namespace Ui {
class ventanaConversionDiasASemanas;
}

class ventanaConversionDiasASemanas : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionDiasASemanas(QWidget *parent = nullptr);
    ~ventanaConversionDiasASemanas();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionDiasASemanas *ui;
    double daysToWeeks(int Valor);
};

#endif // VENTANACONVERSIONDIASASEMANAS_H
