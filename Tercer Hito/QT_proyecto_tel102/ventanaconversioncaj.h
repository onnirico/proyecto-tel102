#ifndef VENTANACONVERSIONCAJ_H
#define VENTANACONVERSIONCAJ_H

#include <QWidget>

namespace Ui {
class ventanaConversionCAJ;
}

class ventanaConversionCAJ : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionCAJ(QWidget *parent = nullptr);
    ~ventanaConversionCAJ();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionCAJ *ui;
};

#endif // VENTANACONVERSIONCAJ_H
