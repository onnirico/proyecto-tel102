#ifndef VENTANACONVERSIONFARENHEITAKLEVIN_H
#define VENTANACONVERSIONFARENHEITAKLEVIN_H

#include <QWidget>

namespace Ui {
class ventanaConversionFarenheitAKlevin;
}

class ventanaConversionFarenheitAKlevin : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionFarenheitAKlevin(QWidget *parent = nullptr);
    ~ventanaConversionFarenheitAKlevin();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionFarenheitAKlevin *ui;
};

#endif // VENTANACONVERSIONFARENHEITAKLEVIN_H
