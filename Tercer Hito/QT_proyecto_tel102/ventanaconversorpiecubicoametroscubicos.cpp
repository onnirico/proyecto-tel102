#include "ventanaconversorpiecubicoametroscubicos.h"
#include "ui_ventanaconversorpiecubicoametroscubicos.h"

ventanaConversorPieCubicoAMetrosCubicos::ventanaConversorPieCubicoAMetrosCubicos(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversorPieCubicoAMetrosCubicos)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversorPieCubicoAMetrosCubicos::on_pushButton_clicked()
{
    this->close(); // Cierra la ventana actual
}

void ventanaConversorPieCubicoAMetrosCubicos::on_pushButton_clicked_2()
{
    // Obtiene el valor en pies cúbicos desde la QDoubleSpinBox
    double piesCubicos = ui->doubleSpinBox->value();

    // Convierte pies cúbicos a metros cúbicos utilizando el factor de conversión (1 pie cúbico = 0.0283168 metros cúbicos)
    double metrosCubicos = piesCubicos * 0.0283168;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(metrosCubicos));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}


ventanaConversorPieCubicoAMetrosCubicos::~ventanaConversorPieCubicoAMetrosCubicos()
{
    delete ui;
}
