#include "ventanaconversionpaabar.h"
#include "ui_ventanaconversionpaabar.h"

ventanaConversionPaABar::ventanaConversionPaABar(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionPaABar)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionPaABar::on_pushButton_clicked()
{
    this->close(); // Cierra la ventana actual
}

void ventanaConversionPaABar::on_pushButton_clicked_2()
{
    // Obtiene el valor en pascales desde la QDoubleSpinBox
    double pascals = ui->doubleSpinBox->value();

    // Convierte pascales a bares (1 bar = 100,000 pascales)
    double bars = pascals / 100000.0;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(bars));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionPaABar::~ventanaConversionPaABar()
{
    delete ui;
}
