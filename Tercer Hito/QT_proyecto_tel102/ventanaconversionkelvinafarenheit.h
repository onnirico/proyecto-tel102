#ifndef VENTANACONVERSIONKELVINAFARENHEIT_H
#define VENTANACONVERSIONKELVINAFARENHEIT_H

#include <QWidget>

namespace Ui {
class ventanaConversionKelvinAFarenheit;
}

class ventanaConversionKelvinAFarenheit : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionKelvinAFarenheit(QWidget *parent = nullptr);
    ~ventanaConversionKelvinAFarenheit();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionKelvinAFarenheit *ui;
};

#endif // VENTANACONVERSIONKELVINAFARENHEIT_H
