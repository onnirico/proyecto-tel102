#include "ventanaconversionesacresahectareas.h"
#include "ui_ventanaconversionesacresahectareas.h"

ventanaConversionesAcresAHectareas::ventanaConversionesAcresAHectareas(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionesAcresAHectareas)
{
    ui->setupUi(this);
    connect(ui->pushButton_5, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_6, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionesAcresAHectareas::on_pushButton_clicked()
{
    close();
}

void ventanaConversionesAcresAHectareas::on_pushButton_clicked_2()
{
    // Obtiene el valor en acres desde la QDoubleSpinBox
    double acres = ui->doubleSpinBox->value();

    // Convierte acres a hectáreas utilizando el factor de conversión (1 acre = 0.404686 hectáreas)
    double hectareas = acres * 0.404686;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(hectareas));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}
ventanaConversionesAcresAHectareas::~ventanaConversionesAcresAHectareas()
{
    delete ui;
}
