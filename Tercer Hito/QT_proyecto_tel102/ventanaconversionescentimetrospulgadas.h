#ifndef VENTANACONVERSIONESCENTIMETROSPULGADAS_H
#define VENTANACONVERSIONESCENTIMETROSPULGADAS_H

#include <QWidget>

namespace Ui {
class ventanaConversionesCentimetrosPulgadas;
}

class ventanaConversionesCentimetrosPulgadas : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionesCentimetrosPulgadas(QWidget *parent = nullptr);
    ~ventanaConversionesCentimetrosPulgadas();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionesCentimetrosPulgadas *ui;
};

#endif // VENTANACONVERSIONESCENTIMETROSPULGADAS_H
