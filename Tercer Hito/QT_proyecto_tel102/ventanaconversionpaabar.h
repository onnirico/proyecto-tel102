#ifndef VENTANACONVERSIONPAABAR_H
#define VENTANACONVERSIONPAABAR_H

#include <QWidget>

namespace Ui {
class ventanaConversionPaABar;
}

class ventanaConversionPaABar : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionPaABar(QWidget *parent = nullptr);
    ~ventanaConversionPaABar();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionPaABar *ui;
};

#endif // VENTANACONVERSIONPAABAR_H
