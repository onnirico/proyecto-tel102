#include "ventanaconversionlitrosamililitros.h"
#include "ui_ventanaconversionlitrosamililitros.h"

ventanaConversionLitrosAMililitros::ventanaConversionLitrosAMililitros(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionLitrosAMililitros)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionLitrosAMililitros::on_pushButton_clicked()
{
    this->close(); // Cierra la ventana actual
}

void ventanaConversionLitrosAMililitros::on_pushButton_clicked_2()
{
    // Obtiene el valor en litros desde la QDoubleSpinBox
    double litros = ui->doubleSpinBox->value();

    // Convierte litros a mililitros utilizando el factor de conversión (1 litro = 1000 mililitros)
    double mililitros = litros * 1000;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(mililitros));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionLitrosAMililitros::~ventanaConversionLitrosAMililitros()
{
    delete ui;
}
