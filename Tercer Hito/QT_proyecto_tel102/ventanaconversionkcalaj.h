#ifndef VENTANACONVERSIONKCALAJ_H
#define VENTANACONVERSIONKCALAJ_H

#include <QWidget>

namespace Ui {
class ventanaConversionkCalAJ;
}

class ventanaConversionkCalAJ : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionkCalAJ(QWidget *parent = nullptr);
    ~ventanaConversionkCalAJ();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionkCalAJ *ui;
};

#endif // VENTANACONVERSIONKCALAJ_H
