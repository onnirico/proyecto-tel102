#ifndef VENTANACONVERSIONESLITROSAGALONES_H
#define VENTANACONVERSIONESLITROSAGALONES_H

#include <QWidget>

namespace Ui {
class ventanaConversionesLitrosAGalones;
}

class ventanaConversionesLitrosAGalones : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionesLitrosAGalones(QWidget *parent = nullptr);
    ~ventanaConversionesLitrosAGalones();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionesLitrosAGalones *ui;
};

#endif // VENTANACONVERSIONESLITROSAGALONES_H
