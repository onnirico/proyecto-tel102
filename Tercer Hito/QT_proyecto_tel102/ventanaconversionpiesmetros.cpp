#include "ventanaconversionpiesmetros.h"
#include "ui_ventanaconversionpiesmetros.h"

ventanaConversionPiesMetros::ventanaConversionPiesMetros(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionPiesMetros)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionPiesMetros::on_pushButton_clicked()
{
    this->close(); // Cierra la ventana actual
}

void ventanaConversionPiesMetros::on_pushButton_clicked_2()
{
    // Obtiene el valor en pies desde la QDoubleSpinBox
    double feet = ui->doubleSpinBox->value();

    // Convierte pies a metros utilizando el factor de conversión (1 pie = 0.3048 metros)
    double meters = feet * 0.3048;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(meters));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionPiesMetros::~ventanaConversionPiesMetros()
{
    delete ui;
}
