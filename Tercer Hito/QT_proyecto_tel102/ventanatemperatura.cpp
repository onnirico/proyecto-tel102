#include "ventanatemperatura.h"
#include "ui_ventanatemperatura.h"
#include "ventanaconversioncelsiusafarenheit.h"
#include "ventanaconversionfarenheitacelsius.h"
#include "ventanaconversioncelsiusakelvin.h"
#include "ventanaconversionkelvinacelsius.h"
#include "ventanaconversionfarenheitaklevin.h"
#include "ventanaconversionkelvinafarenheit.h"


ventanaTemperatura::ventanaTemperatura(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaTemperatura)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
    connect(ui->pushButton_3, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_3()));
    connect(ui->pushButton_4, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_4()));
    connect(ui->pushButton_5, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_5()));
    connect(ui->pushButton_6, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_6()));
    connect(ui->pushButton_7, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_7()));
}

void ventanaTemperatura::on_pushButton_clicked()
{
    close();
}

void ventanaTemperatura::on_pushButton_clicked_2()
{
    ventanaConversionCelsiusAFarenheit *ventanaConversionCelsiusAFarenheit = new class ventanaConversionCelsiusAFarenheit(); // Crea una nueva instancia
    ventanaConversionCelsiusAFarenheit->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionCelsiusAFarenheit->show();
}

void ventanaTemperatura::on_pushButton_clicked_3()
{
    ventanaConversionFarenheitACelsius *ventanaConversionFarenheitACelsius = new class ventanaConversionFarenheitACelsius(); // Crea una nueva instancia
    ventanaConversionFarenheitACelsius->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionFarenheitACelsius->show();
}

void ventanaTemperatura::on_pushButton_clicked_4()
{
    ventanaConversionCelsiusAKelvin *ventanaConversionCelsiusAKelvin = new class ventanaConversionCelsiusAKelvin(); // Crea una nueva instancia
    ventanaConversionCelsiusAKelvin->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionCelsiusAKelvin->show();
}

void ventanaTemperatura::on_pushButton_clicked_5()
{
    ventanaConversionKelvinACelsius *ventanaConversionKelvinACelsius = new class ventanaConversionKelvinACelsius(); // Crea una nueva instancia
    ventanaConversionKelvinACelsius->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionKelvinACelsius->show();
}

void ventanaTemperatura::on_pushButton_clicked_6()
{
    ventanaConversionFarenheitAKlevin *ventanaConversionFarenheitAKlevin = new class ventanaConversionFarenheitAKlevin(); // Crea una nueva instancia
    ventanaConversionFarenheitAKlevin->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionFarenheitAKlevin->show();
}

void ventanaTemperatura::on_pushButton_clicked_7()
{
    ventanaConversionKelvinAFarenheit *ventanaConversionKelvinAFarenheit = new class ventanaConversionKelvinAFarenheit(); // Crea una nueva instancia
    ventanaConversionKelvinAFarenheit->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionKelvinAFarenheit->show();
}

ventanaTemperatura::~ventanaTemperatura()
{
    delete ui;
}
