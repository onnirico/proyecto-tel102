#include "ventanaconversionfarenheitacelsius.h"
#include "ui_ventanaconversionfarenheitacelsius.h"

ventanaConversionFarenheitACelsius::ventanaConversionFarenheitACelsius(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionFarenheitACelsius)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionFarenheitACelsius::on_pushButton_clicked()
{
    close();
}

void ventanaConversionFarenheitACelsius::on_pushButton_clicked_2()
{
    // Obtiene el valor en grados Fahrenheit desde la QDoubleSpinBox
    double fahrenheit = ui->doubleSpinBox->value();

    // Convierte grados Fahrenheit a Celsius utilizando la fórmula (C = (F - 32) * 5/9)
    double celsius = (fahrenheit - 32.0) * 5.0 / 9.0;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(celsius));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionFarenheitACelsius::~ventanaConversionFarenheitACelsius()
{
    delete ui;
}
