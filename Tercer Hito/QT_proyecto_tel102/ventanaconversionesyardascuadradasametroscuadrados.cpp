#include "ventanaconversionesyardascuadradasametroscuadrados.h"
#include "ui_ventanaconversionesyardascuadradasametroscuadrados.h"

ventanaConversionesYardasCuadradasAMetrosCuadrados::ventanaConversionesYardasCuadradasAMetrosCuadrados(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionesYardasCuadradasAMetrosCuadrados)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionesYardasCuadradasAMetrosCuadrados::on_pushButton_clicked()
{
    this->close(); // Cierra la ventana actual
}

void ventanaConversionesYardasCuadradasAMetrosCuadrados::on_pushButton_clicked_2()
{
    // Obtiene el valor en yardas cuadradas desde la QDoubleSpinBox
    double squareYards = ui->doubleSpinBox->value();

    // Convierte yardas cuadradas a metros cuadrados utilizando el factor de conversión (1 yarda cuadrada = 0.836127 metros cuadrados)
    double squareMeters = squareYards * 0.836127;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(squareMeters));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}


ventanaConversionesYardasCuadradasAMetrosCuadrados::~ventanaConversionesYardasCuadradasAMetrosCuadrados()
{
    delete ui;
}
