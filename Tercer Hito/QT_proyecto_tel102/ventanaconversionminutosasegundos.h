#ifndef VENTANACONVERSIONMINUTOSASEGUNDOS_H
#define VENTANACONVERSIONMINUTOSASEGUNDOS_H

#include <QWidget>

namespace Ui {
class ventanaConversionMinutosASegundos;
}

class ventanaConversionMinutosASegundos : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionMinutosASegundos(QWidget *parent = nullptr);
    ~ventanaConversionMinutosASegundos();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionMinutosASegundos *ui;
};

#endif // VENTANACONVERSIONMINUTOSASEGUNDOS_H
