#ifndef VENTANACONVERSIONPIESMETROS_H
#define VENTANACONVERSIONPIESMETROS_H

#include <QWidget>

namespace Ui {
class ventanaConversionPiesMetros;
}

class ventanaConversionPiesMetros : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionPiesMetros(QWidget *parent = nullptr);
    ~ventanaConversionPiesMetros();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionPiesMetros *ui;
};

#endif // VENTANACONVERSIONPIESMETROS_H
