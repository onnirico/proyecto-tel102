#ifndef VENTANACONVERSIONSEMANASADIAS_H
#define VENTANACONVERSIONSEMANASADIAS_H

#include <QWidget>

namespace Ui {
class ventanaConversionSemanasADias;
}

class ventanaConversionSemanasADias : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionSemanasADias(QWidget *parent = nullptr);
    ~ventanaConversionSemanasADias();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionSemanasADias *ui;
};

#endif // VENTANACONVERSIONSEMANASADIAS_H
