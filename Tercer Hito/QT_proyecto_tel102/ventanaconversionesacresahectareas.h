#ifndef VENTANACONVERSIONESACRESAHECTAREAS_H
#define VENTANACONVERSIONESACRESAHECTAREAS_H

#include <QWidget>

namespace Ui {
class ventanaConversionesAcresAHectareas;
}

class ventanaConversionesAcresAHectareas : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionesAcresAHectareas(QWidget *parent = nullptr);
    ~ventanaConversionesAcresAHectareas();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionesAcresAHectareas *ui;
};

#endif // VENTANACONVERSIONESACRESAHECTAREAS_H
