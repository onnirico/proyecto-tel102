#ifndef VENTANACONVERSIONESMETROSPIES_H
#define VENTANACONVERSIONESMETROSPIES_H

#include <QWidget>

namespace Ui {
class ventanaConversionesMetrosPies;
}

class ventanaConversionesMetrosPies : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionesMetrosPies(QWidget *parent = nullptr);
    ~ventanaConversionesMetrosPies();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionesMetrosPies *ui;
};

#endif // VENTANACONVERSIONESMETROSPIES_H
