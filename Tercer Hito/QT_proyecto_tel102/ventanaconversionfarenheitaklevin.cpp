#include "ventanaconversionfarenheitaklevin.h"
#include "ui_ventanaconversionfarenheitaklevin.h"

ventanaConversionFarenheitAKlevin::ventanaConversionFarenheitAKlevin(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionFarenheitAKlevin)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionFarenheitAKlevin::on_pushButton_clicked()
{
    close();
}

void ventanaConversionFarenheitAKlevin::on_pushButton_clicked_2()
{
    // Obtiene el valor en grados Fahrenheit desde la QDoubleSpinBox
    double fahrenheit = ui->doubleSpinBox->value();

    // Convierte grados Fahrenheit a Kelvin utilizando la fórmula (K = (F - 32) * 5/9 + 273.15)
    double kelvin = (fahrenheit - 32.0) * 5.0 / 9.0 + 273.15;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(kelvin));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}


ventanaConversionFarenheitAKlevin::~ventanaConversionFarenheitAKlevin()
{
    delete ui;
}
