#include "ventanaconversionkelvinacelsius.h"
#include "ui_ventanaconversionkelvinacelsius.h"

ventanaConversionKelvinACelsius::ventanaConversionKelvinACelsius(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionKelvinACelsius)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionKelvinACelsius::on_pushButton_clicked()
{
    close();
}

void ventanaConversionKelvinACelsius::on_pushButton_clicked_2()
{
    // Obtiene el valor en grados Kelvin desde la QDoubleSpinBox
    double kelvin = ui->doubleSpinBox->value();

    // Convierte grados Kelvin a Celsius utilizando la fórmula (C = K - 273.15)
    double celsius = kelvin - 273.15;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(celsius));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionKelvinACelsius::~ventanaConversionKelvinACelsius()
{
    delete ui;
}
