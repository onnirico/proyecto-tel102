#ifndef ENERGIAMECANICA_H
#define ENERGIAMECANICA_H

#include <QWidget>

namespace Ui {
class energiaMecanica;
}

class energiaMecanica : public QWidget
{
    Q_OBJECT

public:
    explicit energiaMecanica(QWidget *parent = nullptr);
    ~energiaMecanica();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::energiaMecanica *ui;
};

#endif // ENERGIAMECANICA_H
