#ifndef VENTANAAREA_H
#define VENTANAAREA_H

#include <QWidget>

namespace Ui {
class ventanaArea;
}

class ventanaArea : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaArea(QWidget *parent = nullptr);
    ~ventanaArea();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();
    void on_pushButton_clicked_3();
    void on_pushButton_clicked_4();
    void on_pushButton_clicked_5();
    void on_pushButton_clicked_6();
    void on_pushButton_clicked_7();
    void on_pushButton_clicked_8();


private:
    Ui::ventanaArea *ui;
};

#endif // VENTANAAREA_H
