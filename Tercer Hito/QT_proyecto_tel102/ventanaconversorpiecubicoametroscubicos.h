#ifndef VENTANACONVERSORPIECUBICOAMETROSCUBICOS_H
#define VENTANACONVERSORPIECUBICOAMETROSCUBICOS_H

#include <QWidget>

namespace Ui {
class ventanaConversorPieCubicoAMetrosCubicos;
}

class ventanaConversorPieCubicoAMetrosCubicos : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversorPieCubicoAMetrosCubicos(QWidget *parent = nullptr);
    ~ventanaConversorPieCubicoAMetrosCubicos();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversorPieCubicoAMetrosCubicos *ui;
};

#endif // VENTANACONVERSORPIECUBICOAMETROSCUBICOS_H
