#ifndef VENTANACONVERSIONMPHAKMH_H
#define VENTANACONVERSIONMPHAKMH_H

#include <QWidget>

namespace Ui {
class ventanaConversionMphAKmH;
}

class ventanaConversionMphAKmH : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionMphAKmH(QWidget *parent = nullptr);
    ~ventanaConversionMphAKmH();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionMphAKmH *ui;
};

#endif // VENTANACONVERSIONMPHAKMH_H
