#ifndef VENTANACONVERSIONCELSIUSAKELVIN_H
#define VENTANACONVERSIONCELSIUSAKELVIN_H

#include <QWidget>

namespace Ui {
class ventanaConversionCelsiusAKelvin;
}

class ventanaConversionCelsiusAKelvin : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionCelsiusAKelvin(QWidget *parent = nullptr);
    ~ventanaConversionCelsiusAKelvin();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();
private:
    Ui::ventanaConversionCelsiusAKelvin *ui;
};

#endif // VENTANACONVERSIONCELSIUSAKELVIN_H
