#ifndef VENTANACONVERSIONMILILITROSAONZAS_H
#define VENTANACONVERSIONMILILITROSAONZAS_H

#include <QWidget>

namespace Ui {
class ventanaConversionMililitrosAOnzas;
}

class ventanaConversionMililitrosAOnzas : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionMililitrosAOnzas(QWidget *parent = nullptr);
    ~ventanaConversionMililitrosAOnzas();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionMililitrosAOnzas *ui;
};

#endif // VENTANACONVERSIONMILILITROSAONZAS_H
