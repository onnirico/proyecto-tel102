#ifndef VENTANACONVERSIONESYARDASCUADRADASAMETROSCUADRADOS_H
#define VENTANACONVERSIONESYARDASCUADRADASAMETROSCUADRADOS_H

#include <QWidget>

namespace Ui {
class ventanaConversionesYardasCuadradasAMetrosCuadrados;
}

class ventanaConversionesYardasCuadradasAMetrosCuadrados : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionesYardasCuadradasAMetrosCuadrados(QWidget *parent = nullptr);
    ~ventanaConversionesYardasCuadradasAMetrosCuadrados();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionesYardasCuadradasAMetrosCuadrados *ui;
};

#endif // VENTANACONVERSIONESYARDASCUADRADASAMETROSCUADRADOS_H
