#include "ventanaconversiondiasaanios.h"
#include "ui_ventanaconversiondiasaanios.h"

ventanaConversionDiasAAnios::ventanaConversionDiasAAnios(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionDiasAAnios)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionDiasAAnios::on_pushButton_clicked()
{
    close();
}

void ventanaConversionDiasAAnios::on_pushButton_clicked_2()
{
    // Obtiene el valor en días desde la QDoubleSpinBox
    int dias = static_cast<int>(ui->doubleSpinBox->value());

    // Convierte días a años utilizando la función proporcionada
    double anos = daysToYears(dias);

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(anos));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

double ventanaConversionDiasAAnios::daysToYears(int Valor)
{
    // Asumiendo un año promedio de 365.25 días
    return static_cast<double>(Valor) / 365.25;
}

ventanaConversionDiasAAnios::~ventanaConversionDiasAAnios()
{
    delete ui;
}
