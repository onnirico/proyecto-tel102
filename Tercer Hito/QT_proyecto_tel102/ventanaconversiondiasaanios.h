#ifndef VENTANACONVERSIONDIASAANIOS_H
#define VENTANACONVERSIONDIASAANIOS_H

#include <QWidget>

namespace Ui {
class ventanaConversionDiasAAnios;
}

class ventanaConversionDiasAAnios : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionDiasAAnios(QWidget *parent = nullptr);
    ~ventanaConversionDiasAAnios();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionDiasAAnios *ui;
    double daysToYears(int Valor);
};

#endif // VENTANACONVERSIONDIASAANIOS_H
