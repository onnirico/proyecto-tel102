#include "ventanaconversionjakj.h"
#include "ui_ventanaconversionjakj.h"

ventanaConversionJAKJ::ventanaConversionJAKJ(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionJAKJ)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionJAKJ::on_pushButton_clicked()
{
    close();
}

void ventanaConversionJAKJ::on_pushButton_clicked_2()
{
    // Obtiene el valor en julios desde la QDoubleSpinBox
    double julios = ui->doubleSpinBox->value();

    // Convierte julios a kilojulios
    double kilojulios = julios * 0.001;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(kilojulios));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionJAKJ::~ventanaConversionJAKJ()
{
    delete ui;
}
