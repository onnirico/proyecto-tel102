#include "ventanaconversionespulgadascuadradasacentimetroscuadrados.h"
#include "ui_ventanaconversionespulgadascuadradasacentimetroscuadrados.h"

ventanaConversionesPulgadasCuadradasACentimetrosCuadrados::ventanaConversionesPulgadasCuadradasACentimetrosCuadrados(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionesPulgadasCuadradasACentimetrosCuadrados)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionesPulgadasCuadradasACentimetrosCuadrados::on_pushButton_clicked()
{
    this->close(); // Cierra la ventana actual
}

void ventanaConversionesPulgadasCuadradasACentimetrosCuadrados::on_pushButton_clicked_2()
{
    // Obtiene el valor en pulgadas cuadradas desde la QDoubleSpinBox
    double squareInches = ui->doubleSpinBox->value();

    // Convierte pulgadas cuadradas a centímetros cuadrados utilizando el factor de conversión (1 pulgada cuadrada = 6.4516 centímetros cuadrados)
    double squareCentimeters = squareInches * 6.4516;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(squareCentimeters));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}


ventanaConversionesPulgadasCuadradasACentimetrosCuadrados::~ventanaConversionesPulgadasCuadradasACentimetrosCuadrados()
{
    delete ui;
}
