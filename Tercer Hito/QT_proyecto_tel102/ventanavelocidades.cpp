#include "ventanavelocidades.h"
#include "ui_ventanavelocidades.h"
#include "ventanaconversionkmams.h"
#include "ventanaconversionesmsakmh.h"
#include "ventanaconversionkmhamph.h"
#include "ventanaconversionmphakmh.h"
#include "ventanaconversionkmhaknots.h"
#include "ventanaconversionknotsakmh.h"
#include "ventanaconversionmsafts.h"
#include "ventanaconversionftsams.h"

ventanaVelocidades::ventanaVelocidades(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaVelocidades)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
    connect(ui->pushButton_3, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_3()));
    connect(ui->pushButton_4, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_4()));
    connect(ui->pushButton_5, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_5()));
    connect(ui->pushButton_6, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_6()));
    connect(ui->pushButton_7, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_7()));
    connect(ui->pushButton_8, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_8()));
    connect(ui->pushButton_9, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_9()));


}

void ventanaVelocidades::on_pushButton_clicked()
{
    close();
}

void ventanaVelocidades::on_pushButton_clicked_2()
{
    ventanaConversionKmAMS *ventanaConversionKmAMS = new class ventanaConversionKmAMS(); // Crea una nueva instancia
    ventanaConversionKmAMS->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionKmAMS->show();
}

void ventanaVelocidades::on_pushButton_clicked_3()
{
    ventanaConversionesMSAKmH *ventanaConversionesMSAKmH = new class ventanaConversionesMSAKmH(); // Crea una nueva instancia
    ventanaConversionesMSAKmH->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionesMSAKmH->show();
}

void ventanaVelocidades::on_pushButton_clicked_4()
{
    ventanaConversionKmHAMph *ventanaConversionKmHAMph = new class ventanaConversionKmHAMph(); // Crea una nueva instancia
    ventanaConversionKmHAMph->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionKmHAMph->show();
}

void ventanaVelocidades::on_pushButton_clicked_5()
{
    ventanaConversionMphAKmH *ventanaConversionMphAKmH = new class ventanaConversionMphAKmH(); // Crea una nueva instancia
    ventanaConversionMphAKmH->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionMphAKmH->show();
}

void ventanaVelocidades::on_pushButton_clicked_6()
{
    ventanaConversionKmHAKnots *ventanaConversionKmHAKnots = new class ventanaConversionKmHAKnots(); // Crea una nueva instancia
    ventanaConversionKmHAKnots->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionKmHAKnots->show();
}

void ventanaVelocidades::on_pushButton_clicked_7()
{
    ventanaConversionKnotsAKmH *ventanaConversionKnotsAKmH = new class ventanaConversionKnotsAKmH(); // Crea una nueva instancia ventanaConversionKnotsAKmH->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionKnotsAKmH->show();
}

void ventanaVelocidades::on_pushButton_clicked_8()
{
    ventanaConversionMSAFtS *ventanaConversionMSAFtS = new class ventanaConversionMSAFtS(); // Crea una nueva instancia
    ventanaConversionMSAFtS->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionMSAFtS->show();
}

void ventanaVelocidades::on_pushButton_clicked_9()
{
    ventanaConversionFtSAMS *ventanaConversionFtSAMS = new class ventanaConversionFtSAMS(); // Crea una nueva instancia
    ventanaConversionFtSAMS->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionFtSAMS->show();
}


ventanaVelocidades::~ventanaVelocidades()
{
    delete ui;
}
