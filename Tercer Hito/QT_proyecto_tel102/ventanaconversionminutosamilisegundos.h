#ifndef VENTANACONVERSIONMINUTOSAMILISEGUNDOS_H
#define VENTANACONVERSIONMINUTOSAMILISEGUNDOS_H

#include <QWidget>

namespace Ui {
class ventanaConversionMinutosAMilisegundos;
}

class ventanaConversionMinutosAMilisegundos : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionMinutosAMilisegundos(QWidget *parent = nullptr);
    ~ventanaConversionMinutosAMilisegundos();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionMinutosAMilisegundos *ui;
    long long minutesToMilliseconds(int Valor);
};

#endif // VENTANACONVERSIONMINUTOSAMILISEGUNDOS_H
