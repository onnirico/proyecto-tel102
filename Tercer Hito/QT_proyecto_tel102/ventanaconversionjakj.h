#ifndef VENTANACONVERSIONJAKJ_H
#define VENTANACONVERSIONJAKJ_H

#include <QWidget>

namespace Ui {
class ventanaConversionJAKJ;
}

class ventanaConversionJAKJ : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionJAKJ(QWidget *parent = nullptr);
    ~ventanaConversionJAKJ();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionJAKJ *ui;
};

#endif // VENTANACONVERSIONJAKJ_H
