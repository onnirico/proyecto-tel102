#include "energiapotencialelastica.h"
#include "ui_energiapotencialelastica.h"

energiaPotencialElastica::energiaPotencialElastica(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::energiaPotencialElastica)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void energiaPotencialElastica::on_pushButton_clicked()
{
    close();
}

void energiaPotencialElastica::on_pushButton_clicked_2()
{
    // Obtiene el valor de la constante elástica en N/m desde la QDoubleSpinBox_3
    double constanteElastica = ui->doubleSpinBox->value();

    // Obtiene el valor de la deformación en metros desde la QDoubleSpinBox_4
    double deformacion = ui->doubleSpinBox_2->value();

    // Calcula la energía potencial elástica utilizando la fórmula E_pe = 1/2 * k * x^2
    double energiaPotencialElastica = 0.5 * constanteElastica * deformacion * deformacion;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(energiaPotencialElastica));

    // Restablece los valores en las QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
    ui->doubleSpinBox_2->setValue(0.0);
}


energiaPotencialElastica::~energiaPotencialElastica()
{
    delete ui;
}
