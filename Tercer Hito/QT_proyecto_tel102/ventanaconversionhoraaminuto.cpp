#include "ventanaconversionhoraaminuto.h"
#include "ui_ventanaconversionhoraaminuto.h"

ventanaConversionHoraAMinuto::ventanaConversionHoraAMinuto(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionHoraAMinuto)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionHoraAMinuto::on_pushButton_clicked()
{
    close();
}

void ventanaConversionHoraAMinuto::on_pushButton_clicked_2()
{
    // Obtiene el valor en horas desde la QDoubleSpinBox
    double horas = ui->doubleSpinBox->value();

    // Convierte horas a minutos
    double minutos = horas * 60.0;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(minutos));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}


ventanaConversionHoraAMinuto::~ventanaConversionHoraAMinuto()
{
    delete ui;
}
