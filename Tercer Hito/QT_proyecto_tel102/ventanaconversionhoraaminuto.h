#ifndef VENTANACONVERSIONHORAAMINUTO_H
#define VENTANACONVERSIONHORAAMINUTO_H

#include <QWidget>

namespace Ui {
class ventanaConversionHoraAMinuto;
}

class ventanaConversionHoraAMinuto : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionHoraAMinuto(QWidget *parent = nullptr);
    ~ventanaConversionHoraAMinuto();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionHoraAMinuto *ui;
};

#endif // VENTANACONVERSIONHORAAMINUTO_H
