#ifndef VENTANACONVERSIONESMILLASCUADRADASAKILOMETROSCUADRADOS_H
#define VENTANACONVERSIONESMILLASCUADRADASAKILOMETROSCUADRADOS_H

#include <QWidget>

namespace Ui {
class ventanaConversionesMillasCuadradasAKilometrosCuadrados;
}

class ventanaConversionesMillasCuadradasAKilometrosCuadrados : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionesMillasCuadradasAKilometrosCuadrados(QWidget *parent = nullptr);
    ~ventanaConversionesMillasCuadradasAKilometrosCuadrados();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionesMillasCuadradasAKilometrosCuadrados *ui;
};

#endif // VENTANACONVERSIONESMILLASCUADRADASAKILOMETROSCUADRADOS_H
