#include "ventanaconversioneskilometroscuadradosaacre.h"
#include "ui_ventanaconversioneskilometroscuadradosaacre.h"

ventanaConversionesKilometrosCuadradosAAcre::ventanaConversionesKilometrosCuadradosAAcre(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionesKilometrosCuadradosAAcre)
{
    ui->setupUi(this);

    connect(ui->pushButton_5, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_6, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionesKilometrosCuadradosAAcre::on_pushButton_clicked()
{
    this->close(); // Cierra la ventana actual
}

void ventanaConversionesKilometrosCuadradosAAcre::on_pushButton_clicked_2()
{
    // Obtiene el valor en kilómetros cuadrados desde la QDoubleSpinBox
    double squareKilometers = ui->doubleSpinBox->value();

    // Convierte kilómetros cuadrados a acres utilizando el factor de conversión (1 km^2 = 247.105 acres)
    double acres = squareKilometers * 247.105;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(acres));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}


ventanaConversionesKilometrosCuadradosAAcre::~ventanaConversionesKilometrosCuadradosAAcre()
{
    delete ui;
}
