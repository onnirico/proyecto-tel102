#ifndef VENTANACONVERSIONLITROSAMILILITROS_H
#define VENTANACONVERSIONLITROSAMILILITROS_H

#include <QWidget>

namespace Ui {
class ventanaConversionLitrosAMililitros;
}

class ventanaConversionLitrosAMililitros : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionLitrosAMililitros(QWidget *parent = nullptr);
    ~ventanaConversionLitrosAMililitros();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionLitrosAMililitros *ui;
};

#endif // VENTANACONVERSIONLITROSAMILILITROS_H
