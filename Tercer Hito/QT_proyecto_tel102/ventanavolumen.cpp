#include "ventanavolumen.h"
#include "ui_ventanavolumen.h"
#include "ventanaconversionlitrosamililitros.h"
#include "ventanaconversionesmililitrosalitros.h"
#include "ventanaconversionesgalonesalitros.h"
#include "ventanaconversioneslitrosagalones.h"
#include "ventanaconversorpiecubicoametroscubicos.h"
#include "ventanaconversionesmetrocubicoapiecubico.h"
#include "ventanaconversionmililitrosaonzas.h"
#include "ventanaconversionesonzasamililitros.h"

ventanaVolumen::ventanaVolumen(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaVolumen)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
    connect(ui->pushButton_3, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_3()));
    connect(ui->pushButton_4, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_4()));
    connect(ui->pushButton_5, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_5()));
    connect(ui->pushButton_6, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_6()));
    connect(ui->pushButton_7, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_7()));
    connect(ui->pushButton_8, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_8()));
    connect(ui->pushButton_9, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_9()));

}

void ventanaVolumen::on_pushButton_clicked()
{
    close();
}

void ventanaVolumen::on_pushButton_clicked_2()
{
    ventanaConversionLitrosAMililitros *ventanaConversionLitrosAMililitros = new class ventanaConversionLitrosAMililitros(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionLitrosAMililitros->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionLitrosAMililitros->show(); // Muestra la ventana de física
}

void ventanaVolumen::on_pushButton_clicked_3()
{
    ventanaConversionesMililitrosALitros *ventanaConversionesMililitrosALitros = new class ventanaConversionesMililitrosALitros(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionesMililitrosALitros->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionesMililitrosALitros->show(); // Muestra la ventana de física
}

void ventanaVolumen::on_pushButton_clicked_4()
{
    ventanaConversionesGalonesALitros *ventanaConversionesGalonesALitros = new class ventanaConversionesGalonesALitros(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionesGalonesALitros->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionesGalonesALitros->show(); // Muestra la ventana de física
}

void ventanaVolumen::on_pushButton_clicked_5()
{
    ventanaConversionesLitrosAGalones *ventanaConversionesLitrosAGalones = new class ventanaConversionesLitrosAGalones(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionesLitrosAGalones->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionesLitrosAGalones->show(); // Muestra la ventana de física
}

void ventanaVolumen::on_pushButton_clicked_6()
{
    ventanaConversorPieCubicoAMetrosCubicos *ventanaConversorPieCubicoAMetrosCubicos = new class ventanaConversorPieCubicoAMetrosCubicos(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversorPieCubicoAMetrosCubicos->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversorPieCubicoAMetrosCubicos->show(); // Muestra la ventana de física
}

void ventanaVolumen::on_pushButton_clicked_7()
{
    ventanaConversionesMetroCubicoAPieCubico *ventanaConversionesMetroCubicoAPieCubico = new class ventanaConversionesMetroCubicoAPieCubico(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionesMetroCubicoAPieCubico->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionesMetroCubicoAPieCubico->show(); // Muestra la ventana de física
}

void ventanaVolumen::on_pushButton_clicked_8()
{
    ventanaConversionMililitrosAOnzas *ventanaConversionMililitrosAOnzas = new class ventanaConversionMililitrosAOnzas(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionMililitrosAOnzas->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionMililitrosAOnzas->show(); // Muestra la ventana de física
}

void ventanaVolumen::on_pushButton_clicked_9()
{
    ventanaConversionesOnzasAMililitros *ventanaConversionesOnzasAMililitros = new class ventanaConversionesOnzasAMililitros(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionesOnzasAMililitros->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionesOnzasAMililitros->show(); // Muestra la ventana de física
}


ventanaVolumen::~ventanaVolumen()
{
    delete ui;
}
