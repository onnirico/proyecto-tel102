#ifndef VENTANACONVERSIONCELSIUSAFARENHEIT_H
#define VENTANACONVERSIONCELSIUSAFARENHEIT_H

#include <QWidget>

namespace Ui {
class ventanaConversionCelsiusAFarenheit;
}

class ventanaConversionCelsiusAFarenheit : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionCelsiusAFarenheit(QWidget *parent = nullptr);
    ~ventanaConversionCelsiusAFarenheit();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionCelsiusAFarenheit *ui;
};

#endif // VENTANACONVERSIONCELSIUSAFARENHEIT_H
