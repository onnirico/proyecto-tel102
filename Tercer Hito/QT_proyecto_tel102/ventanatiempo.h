#ifndef VENTANATIEMPO_H
#define VENTANATIEMPO_H

#include <QWidget>

namespace Ui {
class ventanaTiempo;
}

class ventanaTiempo : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaTiempo(QWidget *parent = nullptr);
    ~ventanaTiempo();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();
    void on_pushButton_clicked_3();
    void on_pushButton_clicked_4();
    void on_pushButton_clicked_5();
    void on_pushButton_clicked_6();
    void on_pushButton_clicked_7();
    void on_pushButton_clicked_8();
    void on_pushButton_clicked_9();
    void on_pushButton_clicked_10();
    void on_pushButton_clicked_11();
    void on_pushButton_clicked_12();

private:
    Ui::ventanaTiempo *ui;
};

#endif // VENTANATIEMPO_H
