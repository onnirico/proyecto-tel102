#include "ventanaconversionmphakmh.h"
#include "ui_ventanaconversionmphakmh.h"

ventanaConversionMphAKmH::ventanaConversionMphAKmH(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionMphAKmH)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionMphAKmH::on_pushButton_clicked()
{
    this->close(); // Cierra la ventana actual
}

void ventanaConversionMphAKmH::on_pushButton_clicked_2()
{
    // Obtiene el valor en millas por hora desde la QDoubleSpinBox
    double mph = ui->doubleSpinBox->value();

    // Convierte millas por hora a kilómetros por hora
    double kmPerHour = mph * 1.60934;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(kmPerHour));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionMphAKmH::~ventanaConversionMphAKmH()
{
    delete ui;
}
