#ifndef VENTANACONVERSIONPAAATM_H
#define VENTANACONVERSIONPAAATM_H

#include <QWidget>

namespace Ui {
class ventanaConversionPaAAtm;
}

class ventanaConversionPaAAtm : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionPaAAtm(QWidget *parent = nullptr);
    ~ventanaConversionPaAAtm();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionPaAAtm *ui;
};

#endif // VENTANACONVERSIONPAAATM_H
