#ifndef VENTANACONVERSIONFTSAMS_H
#define VENTANACONVERSIONFTSAMS_H

#include <QWidget>

namespace Ui {
class ventanaConversionFtSAMS;
}

class ventanaConversionFtSAMS : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionFtSAMS(QWidget *parent = nullptr);
    ~ventanaConversionFtSAMS();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionFtSAMS *ui;
};

#endif // VENTANACONVERSIONFTSAMS_H
