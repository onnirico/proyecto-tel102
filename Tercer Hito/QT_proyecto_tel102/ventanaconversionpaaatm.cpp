#include "ventanaconversionpaaatm.h"
#include "ui_ventanaconversionpaaatm.h"

ventanaConversionPaAAtm::ventanaConversionPaAAtm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionPaAAtm)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionPaAAtm::on_pushButton_clicked()
{
    this->close(); // Cierra la ventana actual
}

void ventanaConversionPaAAtm::on_pushButton_clicked_2()
{
    // Obtiene el valor en pascales desde la QDoubleSpinBox
    double pascals = ui->doubleSpinBox->value();

    // Convierte pascales a atmósferas
    double atm = pascals / 101325.0; // 1 atm = 101325 Pa

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(atm));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionPaAAtm::~ventanaConversionPaAAtm()
{
    delete ui;
}
