#ifndef VENTANACONVERSIONESKILOMETROSMILLAS_H
#define VENTANACONVERSIONESKILOMETROSMILLAS_H

#include <QWidget>

namespace Ui {
class ventanaConversionesKIlometrosMillas;
}

class ventanaConversionesKIlometrosMillas : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionesKIlometrosMillas(QWidget *parent = nullptr);
    ~ventanaConversionesKIlometrosMillas();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionesKIlometrosMillas *ui;
};

#endif // VENTANACONVERSIONESKILOMETROSMILLAS_H
