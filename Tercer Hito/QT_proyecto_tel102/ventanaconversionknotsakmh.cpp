#include "ventanaconversionknotsakmh.h"
#include "ui_ventanaconversionknotsakmh.h"

ventanaConversionKnotsAKmH::ventanaConversionKnotsAKmH(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionKnotsAKmH)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionKnotsAKmH::on_pushButton_clicked()
{
    close();
}

void ventanaConversionKnotsAKmH::on_pushButton_clicked_2()
{
    // Obtiene el valor en nudos desde la QDoubleSpinBox
    double knots = ui->doubleSpinBox->value();

    // Convierte nudos a kilómetros por hora
    double kmPerHour = knots * 1.852;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(kmPerHour));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionKnotsAKmH::~ventanaConversionKnotsAKmH()
{
    delete ui;
}
