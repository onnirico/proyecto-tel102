#ifndef VENTANACONVERSIONANIOSADIAS_H
#define VENTANACONVERSIONANIOSADIAS_H

#include <QWidget>

namespace Ui {
class ventanaConversionAniosADias;
}

class ventanaConversionAniosADias : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionAniosADias(QWidget *parent = nullptr);
    ~ventanaConversionAniosADias();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionAniosADias *ui;
};

#endif // VENTANACONVERSIONANIOSADIAS_H
