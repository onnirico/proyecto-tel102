#include "ventanaconversionhorasasegundos.h"
#include "ui_ventanaconversionhorasasegundos.h"

ventanaConversionHorasASegundos::ventanaConversionHorasASegundos(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionHorasASegundos)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionHorasASegundos::on_pushButton_clicked()
{
    close();
}

void ventanaConversionHorasASegundos::on_pushButton_clicked_2()
{
    // Obtiene el valor en horas desde la QDoubleSpinBox
    double horas = ui->doubleSpinBox->value();

    // Convierte horas a segundos (1 hora = 3600 segundos)
    double segundos = horas * 3600.0;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(segundos));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionHorasASegundos::~ventanaConversionHorasASegundos()
{
    delete ui;
}
