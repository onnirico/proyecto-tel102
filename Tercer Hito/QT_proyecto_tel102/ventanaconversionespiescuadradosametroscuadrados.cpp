#include "ventanaconversionespiescuadradosametroscuadrados.h"
#include "ui_ventanaconversionespiescuadradosametroscuadrados.h"

ventanaConversionesPiesCuadradosAMetrosCuadrados::ventanaConversionesPiesCuadradosAMetrosCuadrados(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionesPiesCuadradosAMetrosCuadrados)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionesPiesCuadradosAMetrosCuadrados::on_pushButton_clicked()
{
    this->close(); // Cierra la ventana actual
}

void ventanaConversionesPiesCuadradosAMetrosCuadrados::on_pushButton_clicked_2()
{
    // Obtiene el valor en pies cuadrados desde la QDoubleSpinBox
    double squareFeet = ui->doubleSpinBox->value();

    // Convierte pies cuadrados a metros cuadrados utilizando el factor de conversión (1 ft^2 = 0.092903 m^2)
    double squareMeters = squareFeet * 0.092903;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(squareMeters));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionesPiesCuadradosAMetrosCuadrados::~ventanaConversionesPiesCuadradosAMetrosCuadrados()
{
    delete ui;
}
