#include "ventanaconversionminutosasegundos.h"
#include "ui_ventanaconversionminutosasegundos.h"

ventanaConversionMinutosASegundos::ventanaConversionMinutosASegundos(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionMinutosASegundos)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionMinutosASegundos::on_pushButton_clicked()
{
    this->close(); // Cierra la ventana actual
}

void ventanaConversionMinutosASegundos::on_pushButton_clicked_2()
{
    // Obtiene el valor en minutos desde la QDoubleSpinBox
    double minutos = ui->doubleSpinBox->value();

    // Convierte minutos a segundos
    double segundos = minutos * 60.0;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(segundos));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionMinutosASegundos::~ventanaConversionMinutosASegundos()
{
    delete ui;
}
