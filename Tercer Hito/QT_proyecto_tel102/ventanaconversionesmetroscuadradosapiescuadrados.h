#ifndef VENTANACONVERSIONESMETROSCUADRADOSAPIESCUADRADOS_H
#define VENTANACONVERSIONESMETROSCUADRADOSAPIESCUADRADOS_H

#include <QWidget>

namespace Ui {
class ventanaConversionesMetrosCuadradosAPiesCuadrados;
}

class ventanaConversionesMetrosCuadradosAPiesCuadrados : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionesMetrosCuadradosAPiesCuadrados(QWidget *parent = nullptr);
    ~ventanaConversionesMetrosCuadradosAPiesCuadrados();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionesMetrosCuadradosAPiesCuadrados *ui;
};

#endif // VENTANACONVERSIONESMETROSCUADRADOSAPIESCUADRADOS_H
