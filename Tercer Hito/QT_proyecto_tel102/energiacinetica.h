#ifndef ENERGIACINETICA_H
#define ENERGIACINETICA_H

#include <QWidget>

namespace Ui {
class energiaCinetica;
}

class energiaCinetica : public QWidget
{
    Q_OBJECT

public:
    explicit energiaCinetica(QWidget *parent = nullptr);
    ~energiaCinetica();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::energiaCinetica *ui;
};

#endif // ENERGIACINETICA_H
