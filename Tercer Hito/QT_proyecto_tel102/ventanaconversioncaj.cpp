#include "ventanaconversioncaj.h"
#include "ui_ventanaconversioncaj.h"

ventanaConversionCAJ::ventanaConversionCAJ(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionCAJ)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionCAJ::on_pushButton_clicked()
{
    close();
}

void ventanaConversionCAJ::on_pushButton_clicked_2()
{
    // Obtiene el valor en calorías desde la QDoubleSpinBox
    double calorias = ui->doubleSpinBox->value();

    // Convierte calorías a julios
    double julios = calorias * 4.184;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(julios));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionCAJ::~ventanaConversionCAJ()
{
    delete ui;
}
