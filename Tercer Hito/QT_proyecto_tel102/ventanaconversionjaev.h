#ifndef VENTANACONVERSIONJAEV_H
#define VENTANACONVERSIONJAEV_H

#include <QWidget>

namespace Ui {
class ventanaConversionJAeV;
}

class ventanaConversionJAeV : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionJAeV(QWidget *parent = nullptr);
    ~ventanaConversionJAeV();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionJAeV *ui;
};

#endif // VENTANACONVERSIONJAEV_H
