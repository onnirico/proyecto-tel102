#ifndef VENTANACONVERSIONESKILOMETROSCUADRADOSAACRE_H
#define VENTANACONVERSIONESKILOMETROSCUADRADOSAACRE_H

#include <QWidget>

namespace Ui {
class ventanaConversionesKilometrosCuadradosAAcre;
}

class ventanaConversionesKilometrosCuadradosAAcre : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionesKilometrosCuadradosAAcre(QWidget *parent = nullptr);
    ~ventanaConversionesKilometrosCuadradosAAcre();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionesKilometrosCuadradosAAcre *ui;
};

#endif // VENTANACONVERSIONESKILOMETROSCUADRADOSAACRE_H
