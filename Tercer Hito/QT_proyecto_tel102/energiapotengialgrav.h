#ifndef ENERGIAPOTENGIALGRAV_H
#define ENERGIAPOTENGIALGRAV_H

#include <QWidget>

namespace Ui {
class energiaPotengialGrav;
}

class energiaPotengialGrav : public QWidget
{
    Q_OBJECT

public:
    explicit energiaPotengialGrav(QWidget *parent = nullptr);
    ~energiaPotengialGrav();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::energiaPotengialGrav *ui;
};

#endif // ENERGIAPOTENGIALGRAV_H
