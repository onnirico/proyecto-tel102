#ifndef VENTANACONVERSIONESPULGADASCUADRADASACENTIMETROSCUADRADOS_H
#define VENTANACONVERSIONESPULGADASCUADRADASACENTIMETROSCUADRADOS_H

#include <QWidget>

namespace Ui {
class ventanaConversionesPulgadasCuadradasACentimetrosCuadrados;
}

class ventanaConversionesPulgadasCuadradasACentimetrosCuadrados : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionesPulgadasCuadradasACentimetrosCuadrados(QWidget *parent = nullptr);
    ~ventanaConversionesPulgadasCuadradasACentimetrosCuadrados();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionesPulgadasCuadradasACentimetrosCuadrados *ui;
};

#endif // VENTANACONVERSIONESPULGADASCUADRADASACENTIMETROSCUADRADOS_H
