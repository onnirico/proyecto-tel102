#ifndef VENTANACONVERSIONJACAL_H
#define VENTANACONVERSIONJACAL_H

#include <QWidget>

namespace Ui {
class ventanaConversionJACal;
}

class ventanaConversionJACal : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionJACal(QWidget *parent = nullptr);
    ~ventanaConversionJACal();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionJACal *ui;
};

#endif // VENTANACONVERSIONJACAL_H
