#include "ventanaconversionesmillaskilometros.h"
#include "ui_ventanaconversionesmillaskilometros.h"

ventanaConversionesMillasKilometros::ventanaConversionesMillasKilometros(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionesMillasKilometros)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}
void ventanaConversionesMillasKilometros::on_pushButton_clicked()
{
    close();
}

void ventanaConversionesMillasKilometros::on_pushButton_clicked_2()
{
    // Obtiene el valor en millas desde la QDoubleSpinBox
    double miles = ui->doubleSpinBox->value();

    // Convierte millas a kilómetros utilizando el factor de conversión (1 milla = 1.60934 kilómetros)
    double kilometers = miles * 1.60934;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(kilometers));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}


ventanaConversionesMillasKilometros::~ventanaConversionesMillasKilometros()
{
    delete ui;
}
