#include "ventanaconversionftsams.h"
#include "ui_ventanaconversionftsams.h"

ventanaConversionFtSAMS::ventanaConversionFtSAMS(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionFtSAMS)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionFtSAMS::on_pushButton_clicked()
{
    close();
}

void ventanaConversionFtSAMS::on_pushButton_clicked_2()
{
    // Obtiene el valor en pies por segundo desde la QDoubleSpinBox
    double ftPerSecond = ui->doubleSpinBox->value();

    // Convierte pies por segundo a metros por segundo
    double mPerSecond = ftPerSecond * 0.3048;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(mPerSecond));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}


ventanaConversionFtSAMS::~ventanaConversionFtSAMS()
{
    delete ui;
}
