#ifndef VENTANACONVERSIONESMETROCUBICOAPIECUBICO_H
#define VENTANACONVERSIONESMETROCUBICOAPIECUBICO_H

#include <QWidget>

namespace Ui {
class ventanaConversionesMetroCubicoAPieCubico;
}

class ventanaConversionesMetroCubicoAPieCubico : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionesMetroCubicoAPieCubico(QWidget *parent = nullptr);
    ~ventanaConversionesMetroCubicoAPieCubico();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionesMetroCubicoAPieCubico *ui;
};

#endif // VENTANACONVERSIONESMETROCUBICOAPIECUBICO_H
