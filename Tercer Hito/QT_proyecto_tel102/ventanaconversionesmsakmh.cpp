#include "ventanaconversionesmsakmh.h"
#include "ui_ventanaconversionesmsakmh.h"

ventanaConversionesMSAKmH::ventanaConversionesMSAKmH(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionesMSAKmH)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}
void ventanaConversionesMSAKmH::on_pushButton_clicked()
{
    close();
}

void ventanaConversionesMSAKmH::on_pushButton_clicked_2()
{
    // Obtiene el valor en metros por segundo desde la QDoubleSpinBox
    double mPerSecond = ui->doubleSpinBox->value();

    // Convierte metros por segundo a kilómetros por hora (1 m/s = 3.6 km/h)
    double kmPerHour = mPerSecond * 3.6;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(kmPerHour));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}



ventanaConversionesMSAKmH::~ventanaConversionesMSAKmH()
{
    delete ui;
}
