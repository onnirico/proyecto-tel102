#ifndef VENTANACONVERSIONATMABAR_H
#define VENTANACONVERSIONATMABAR_H

#include <QWidget>

namespace Ui {
class ventanaConversionAtmABar;
}

class ventanaConversionAtmABar : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionAtmABar(QWidget *parent = nullptr);
    ~ventanaConversionAtmABar();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionAtmABar *ui;
};

#endif // VENTANACONVERSIONATMABAR_H
