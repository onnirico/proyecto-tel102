#include "ventanaconversionsegundosamilisegundos.h"
#include "ui_ventanaconversionsegundosamilisegundos.h"

ventanaConversionSegundosAMilisegundos::ventanaConversionSegundosAMilisegundos(QWidget *parent)
    : QWidget(parent),
    ui(new Ui::ventanaConversionSegundosAMilisegundos)
{
    ui->setupUi(this);
    connect(ui->pushButton_3, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_3()));
    connect(ui->pushButton_4, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_4()));
}

void ventanaConversionSegundosAMilisegundos::on_pushButton_clicked_3()
{
    this->close(); // Cierra la ventana actual
}

void ventanaConversionSegundosAMilisegundos::on_pushButton_clicked_4()
{
    // Obtiene el valor en segundos desde la QDoubleSpinBox
    int segundos = static_cast<int>(ui->doubleSpinBox->value());

    // Convierte segundos a milisegundos utilizando la función proporcionada
    long long milisegundos = secondsToMilliseconds(segundos);

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(milisegundos));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

long long ventanaConversionSegundosAMilisegundos::secondsToMilliseconds(int Valor)
{
    return static_cast<long long>(Valor) * 1000;
}

ventanaConversionSegundosAMilisegundos::~ventanaConversionSegundosAMilisegundos()
{
    delete ui;
}
