#ifndef VENTANACONVERSIONESPIESCUADRADOSAMETROSCUADRADOS_H
#define VENTANACONVERSIONESPIESCUADRADOSAMETROSCUADRADOS_H

#include <QWidget>

namespace Ui {
class ventanaConversionesPiesCuadradosAMetrosCuadrados;
}

class ventanaConversionesPiesCuadradosAMetrosCuadrados : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionesPiesCuadradosAMetrosCuadrados(QWidget *parent = nullptr);
    ~ventanaConversionesPiesCuadradosAMetrosCuadrados();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionesPiesCuadradosAMetrosCuadrados *ui;
};

#endif // VENTANACONVERSIONESPIESCUADRADOSAMETROSCUADRADOS_H
