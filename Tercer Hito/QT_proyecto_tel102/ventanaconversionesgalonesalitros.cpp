#include "ventanaconversionesgalonesalitros.h"
#include "ui_ventanaconversionesgalonesalitros.h"

ventanaConversionesGalonesALitros::ventanaConversionesGalonesALitros(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionesGalonesALitros)
{
    ui->setupUi(this);

    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionesGalonesALitros::on_pushButton_clicked()
{
    this->close(); // Cierra la ventana actual
}

void ventanaConversionesGalonesALitros::on_pushButton_clicked_2()
{
    // Obtiene el valor en galones desde la QDoubleSpinBox
    double galones = ui->doubleSpinBox->value();

    // Convierte galones a litros utilizando el factor de conversión (1 galón = 3.78541 litros)
    double litros = galones * 3.78541;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(litros));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionesGalonesALitros::~ventanaConversionesGalonesALitros()
{
    delete ui;
}
