#include "ventanaconversionsemanasadias.h"
#include "ui_ventanaconversionsemanasadias.h"

ventanaConversionSemanasADias::ventanaConversionSemanasADias(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionSemanasADias)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionSemanasADias::on_pushButton_clicked()
{
    this->close(); // Cierra la ventana actual
}

void ventanaConversionSemanasADias::on_pushButton_clicked_2()
{
    // Obtiene el valor en semanas desde la QDoubleSpinBox
    double semanas = ui->doubleSpinBox->value();

    // Convierte semanas a días (1 semana = 7 días)
    double dias = semanas * 7.0;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(dias));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionSemanasADias::~ventanaConversionSemanasADias()
{
    delete ui;
}
