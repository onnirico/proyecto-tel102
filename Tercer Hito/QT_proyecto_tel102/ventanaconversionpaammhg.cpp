#include "ventanaconversionpaammhg.h"
#include "ui_ventanaconversionpaammhg.h"

ventanaConversionPaAmmHg::ventanaConversionPaAmmHg(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionPaAmmHg)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionPaAmmHg::on_pushButton_clicked()
{
    this->close(); // Cierra la ventana actual
}

void ventanaConversionPaAmmHg::on_pushButton_clicked_2()
{
    // Obtiene el valor en pascales desde la QDoubleSpinBox
    double pascals = ui->doubleSpinBox->value();

    // Convierte pascales a milímetros de mercurio utilizando la relación estándar
    double mmHg = pascals / 133.322; // 1 mmHg = 133.322 pascales

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(mmHg));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionPaAmmHg::~ventanaConversionPaAmmHg()
{
    delete ui;
}
