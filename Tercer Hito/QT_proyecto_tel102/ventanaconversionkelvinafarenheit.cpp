#include "ventanaconversionkelvinafarenheit.h"
#include "ui_ventanaconversionkelvinafarenheit.h"

ventanaConversionKelvinAFarenheit::ventanaConversionKelvinAFarenheit(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionKelvinAFarenheit)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionKelvinAFarenheit::on_pushButton_clicked()
{
    close();
}

void ventanaConversionKelvinAFarenheit::on_pushButton_clicked_2()
{
    // Obtiene el valor en grados Kelvin desde la QDoubleSpinBox
    double kelvin = ui->doubleSpinBox->value();

    // Convierte grados Kelvin a Fahrenheit utilizando la fórmula (F = K * 9/5 - 459.67)
    double fahrenheit = kelvin * 9.0 / 5.0 - 459.67;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(fahrenheit));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}


ventanaConversionKelvinAFarenheit::~ventanaConversionKelvinAFarenheit()
{
    delete ui;
}
