#ifndef VENTANAENERGIAS_H
#define VENTANAENERGIAS_H

#include <QWidget>

namespace Ui {
class ventanaEnergias;
}

class ventanaEnergias : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaEnergias(QWidget *parent = nullptr);
    ~ventanaEnergias();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();
    void on_pushButton_clicked_3();
    void on_pushButton_clicked_4();
    void on_pushButton_clicked_5();
    void on_pushButton_clicked_6();


private:
    Ui::ventanaEnergias *ui;
};

#endif // VENTANAENERGIAS_H
