#include "ventanalongitud.h"
#include "ui_ventanalongitud.h"
#include "ventanaconversionesmetrospies.h"
#include "ventanaconversionpiesmetros.h"
#include "ventanaconversioneskilometrosmillas.h"
#include "ventanaconversionesmillaskilometros.h"
#include "ventanaconversionescentimetrospulgadas.h"

ventanaLongitud::ventanaLongitud(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaLongitud)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
    connect(ui->pushButton_3, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_3()));
    connect(ui->pushButton_4, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_4()));
    connect(ui->pushButton_5, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_5()));
    connect(ui->pushButton_6, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_6()));
}

void ventanaLongitud::on_pushButton_clicked()
{
    close();
}

void ventanaLongitud::on_pushButton_clicked_2()
{
    ventanaConversionesMetrosPies *ventanaConversionesMetrosPies = new class ventanaConversionesMetrosPies(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionesMetrosPies->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionesMetrosPies->show(); // Muestra la ventana de física
}

void ventanaLongitud::on_pushButton_clicked_3()
{
    ventanaConversionPiesMetros *ventanaConversionPiesMetros = new class ventanaConversionPiesMetros(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionPiesMetros->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionPiesMetros->show(); // Muestra la ventana de física
}

void ventanaLongitud::on_pushButton_clicked_4()
{
    ventanaConversionesKIlometrosMillas *ventanaConversionesKIlometrosMillas = new class ventanaConversionesKIlometrosMillas(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionesKIlometrosMillas->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionesKIlometrosMillas->show(); // Muestra la ventana de física
}

void ventanaLongitud::on_pushButton_clicked_5()
{
    ventanaConversionesMillasKilometros *ventanaConversionesMillasKilometros = new class ventanaConversionesMillasKilometros(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionesMillasKilometros->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionesMillasKilometros->show(); // Muestra la ventana de física
}

void ventanaLongitud::on_pushButton_clicked_6()
{
    ventanaConversionesCentimetrosPulgadas *ventanaConversionesCentimetrosPulgadas = new class ventanaConversionesCentimetrosPulgadas(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionesCentimetrosPulgadas->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionesCentimetrosPulgadas->show(); // Muestra la ventana de física
}

ventanaLongitud::~ventanaLongitud()
{
    delete ui;
}
