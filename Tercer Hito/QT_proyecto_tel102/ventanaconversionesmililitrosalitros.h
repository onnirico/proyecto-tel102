#ifndef VENTANACONVERSIONESMILILITROSALITROS_H
#define VENTANACONVERSIONESMILILITROSALITROS_H

#include <QWidget>

namespace Ui {
class ventanaConversionesMililitrosALitros;
}

class ventanaConversionesMililitrosALitros : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionesMililitrosALitros(QWidget *parent = nullptr);
    ~ventanaConversionesMililitrosALitros();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionesMililitrosALitros *ui;
};

#endif // VENTANACONVERSIONESMILILITROSALITROS_H
