#ifndef VENTANACONVERSIONPAAPSI_H
#define VENTANACONVERSIONPAAPSI_H

#include <QWidget>

namespace Ui {
class ventanaConversionPaAPsi;
}

class ventanaConversionPaAPsi : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionPaAPsi(QWidget *parent = nullptr);
    ~ventanaConversionPaAPsi();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionPaAPsi *ui;
};

#endif // VENTANACONVERSIONPAAPSI_H
