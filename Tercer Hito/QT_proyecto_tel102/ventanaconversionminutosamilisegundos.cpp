#include "ventanaconversionminutosamilisegundos.h"
#include "ui_ventanaconversionminutosamilisegundos.h"

ventanaConversionMinutosAMilisegundos::ventanaConversionMinutosAMilisegundos(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionMinutosAMilisegundos)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionMinutosAMilisegundos::on_pushButton_clicked()
{
    this->close(); // Cierra la ventana actual
}

void ventanaConversionMinutosAMilisegundos::on_pushButton_clicked_2()
{
    // Obtiene el valor en minutos desde la QDoubleSpinBox
    int minutos = static_cast<int>(ui->doubleSpinBox->value());

    // Convierte minutos a milisegundos utilizando la función proporcionada
    long long milisegundos = minutesToMilliseconds(minutos);

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(milisegundos));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

long long ventanaConversionMinutosAMilisegundos::minutesToMilliseconds(int Valor)
{
    return static_cast<long long>(Valor) * 60000;
}

ventanaConversionMinutosAMilisegundos::~ventanaConversionMinutosAMilisegundos()
{
    delete ui;
}
