#include "ventanaconversionkmams.h"
#include "ui_ventanaconversionkmams.h"

ventanaConversionKmAMS::ventanaConversionKmAMS(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionKmAMS)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionKmAMS::on_pushButton_clicked()
{
    close();
}

void ventanaConversionKmAMS::on_pushButton_clicked_2()
{
    // Obtiene el valor en kilómetros por hora desde la QDoubleSpinBox
    double kmPerHour = ui->doubleSpinBox->value();

    // Convierte kilómetros por hora a metros por segundo (1 km/h = 1/3.6 m/s)
    double mPerSecond = kmPerHour / 3.6;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(mPerSecond));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}


ventanaConversionKmAMS::~ventanaConversionKmAMS()
{
    delete ui;
}
