#include "ventanatiempo.h"
#include "ui_ventanatiempo.h"
#include "ventanaconversionhoraaminuto.h"
#include "ventanaconversionminutosasegundos.h"
#include "ventanaconversiondiasahoras.h"
#include "ventanaconversionsemanasadias.h"
#include "ventanaconversionaniosadias.h"
#include "ventanaconversionhorasasegundos.h"
#include "ventanaconversionminutosamilisegundos.h"
#include "ventanaconversionsegundosamilisegundos.h"
#include "ventanaconversiondiasasemanas.h"
#include "ventanaconversionsemanasameses.h"
#include "ventanaconversiondiasaanios.h"

ventanaTiempo::ventanaTiempo(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaTiempo)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
    connect(ui->pushButton_3, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_3()));
    connect(ui->pushButton_4, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_4()));
    connect(ui->pushButton_5, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_5()));
    connect(ui->pushButton_6, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_6()));
    connect(ui->pushButton_7, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_7()));
    connect(ui->pushButton_8, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_8()));
    connect(ui->pushButton_9, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_9()));
    connect(ui->pushButton_10, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_10()));
    connect(ui->pushButton_11, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_11()));
    connect(ui->pushButton_12, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_12()));


}

void ventanaTiempo::on_pushButton_clicked()
{
    close();
}

void ventanaTiempo::on_pushButton_clicked_2()
{
    ventanaConversionHoraAMinuto *ventanaConversionHoraAMinuto = new class ventanaConversionHoraAMinuto(); // Crea una nueva instancia
    ventanaConversionHoraAMinuto->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionHoraAMinuto->show();
}

void ventanaTiempo::on_pushButton_clicked_3()
{
    ventanaConversionMinutosASegundos *ventanaConversionMinutosASegundos = new class ventanaConversionMinutosASegundos(); // Crea una nueva instancia
    ventanaConversionMinutosASegundos->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionMinutosASegundos->show();
}

void ventanaTiempo::on_pushButton_clicked_4()
{
    ventanaConversionDiasAHoras *ventanaConversionDiasAHoras = new class ventanaConversionDiasAHoras(); // Crea una nueva instancia
    ventanaConversionDiasAHoras->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionDiasAHoras->show();
}

void ventanaTiempo::on_pushButton_clicked_5()
{
    ventanaConversionSemanasADias *ventanaConversionSemanasADias = new class ventanaConversionSemanasADias(); // Crea una nueva instancia
    ventanaConversionSemanasADias->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionSemanasADias->show();
}

void ventanaTiempo::on_pushButton_clicked_6()
{
    ventanaConversionAniosADias *ventanaConversionAniosADias = new class ventanaConversionAniosADias(); // Crea una nueva instancia
    ventanaConversionAniosADias->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionAniosADias->show();
}

void ventanaTiempo::on_pushButton_clicked_7()
{
    ventanaConversionHorasASegundos *ventanaConversionHorasASegundos = new class ventanaConversionHorasASegundos(); // Crea una nueva instancia
    ventanaConversionHorasASegundos->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionHorasASegundos->show();
}

void ventanaTiempo::on_pushButton_clicked_8()
{
    ventanaConversionMinutosAMilisegundos *ventanaConversionMinutosAMilisegundos = new class ventanaConversionMinutosAMilisegundos(); // Crea una nueva instancia
    ventanaConversionMinutosAMilisegundos->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionMinutosAMilisegundos->show();
}

void ventanaTiempo::on_pushButton_clicked_9()
{
    ventanaConversionSegundosAMilisegundos *ventanaConversionSegundosAMilisegundos = new class ventanaConversionSegundosAMilisegundos(); // Crea una nueva instancia
    ventanaConversionSegundosAMilisegundos->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionSegundosAMilisegundos->show();
}

void ventanaTiempo::on_pushButton_clicked_10()
{
    ventanaConversionDiasASemanas *ventanaConversionDiasASemanas = new class ventanaConversionDiasASemanas(); // Crea una nueva instancia
    ventanaConversionDiasASemanas->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionDiasASemanas->show();
}

void ventanaTiempo::on_pushButton_clicked_11()
{
    ventanaConversionSemanasAMeses *ventanaConversionSemanasAMeses = new class ventanaConversionSemanasAMeses(); // Crea una nueva instancia
    ventanaConversionSemanasAMeses->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionSemanasAMeses->show();
}

void ventanaTiempo::on_pushButton_clicked_12()
{
    ventanaConversionDiasAAnios *ventanaConversionDiasAAnios = new class ventanaConversionDiasAAnios(); // Crea una nueva instancia
    ventanaConversionDiasAAnios->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionDiasAAnios->show();
}


ventanaTiempo::~ventanaTiempo()
{
    delete ui;
}
