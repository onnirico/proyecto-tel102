#include "energiamecanica.h"
#include "ui_energiamecanica.h"

energiaMecanica::energiaMecanica(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::energiaMecanica)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void energiaMecanica::on_pushButton_clicked()
{
    close();
}

void energiaMecanica::on_pushButton_clicked_2()
{
    // Obtener valores para la energía cinética
    double masaCinetica = ui->doubleSpinBox->value();
    double velocidadCinetica = ui->doubleSpinBox_2->value();

    // Calcular la energía cinética utilizando la fórmula E_k = 1/2 * m * v^2
    double energiaCinetica = 0.5 * masaCinetica * velocidadCinetica * velocidadCinetica;

    // Obtener valores para la energía potencial gravitatoria
    double masaGravitatoria = ui->doubleSpinBox_3->value();
    double alturaGravitatoria = ui->doubleSpinBox_4->value();
    const double gravedad = 9.8;  // Gravedad en la Tierra

    // Calcular la energía potencial gravitatoria utilizando la fórmula E_p = mgh
    double energiaPotencialGravitatoria = masaGravitatoria * gravedad * alturaGravitatoria;

    // Obtener valores para la energía potencial elástica
    double constanteElastica = ui->doubleSpinBox_5->value();
    double deformacionResorte = ui->doubleSpinBox_6->value();

    // Calcular la energía potencial elástica utilizando la fórmula E_pe = 1/2 * k * x^2
    double energiaPotencialElastica = 0.5 * constanteElastica * deformacionResorte * deformacionResorte;

    // Calcular la energía mecánica total sumando las tres componentes
    double energiaMecanicaTotal = energiaCinetica + energiaPotencialGravitatoria + energiaPotencialElastica;

    // Mostrar el resultado en la etiqueta
    ui->label_7->setText(QString::number(energiaMecanicaTotal));

    // Restablecer los valores en las QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
    ui->doubleSpinBox_2->setValue(0.0);
    ui->doubleSpinBox_3->setValue(0.0);
    ui->doubleSpinBox_4->setValue(0.0);
    ui->doubleSpinBox_5->setValue(0.0);
    ui->doubleSpinBox_6->setValue(0.0);
}

energiaMecanica::~energiaMecanica()
{
    delete ui;
}
