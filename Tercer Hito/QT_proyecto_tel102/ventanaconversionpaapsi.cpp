#include "ventanaconversionpaapsi.h"
#include "ui_ventanaconversionpaapsi.h"

ventanaConversionPaAPsi::ventanaConversionPaAPsi(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionPaAPsi)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionPaAPsi::on_pushButton_clicked()
{
    this->close(); // Cierra la ventana actual
}

void ventanaConversionPaAPsi::on_pushButton_clicked_2()
{
    // Obtiene el valor en pascales desde la QDoubleSpinBox
    double pascals = ui->doubleSpinBox->value();

    // Convierte pascales a libras por pulgada cuadrada (psi)
    double psi = pascals / 6894.76; // 1 psi = 6894.76 pascales

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(psi));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionPaAPsi::~ventanaConversionPaAPsi()
{
    delete ui;
}
