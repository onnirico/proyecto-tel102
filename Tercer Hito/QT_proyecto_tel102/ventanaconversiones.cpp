#include "ventanaconversiones.h"
#include "ui_ventanaconversiones.h"
#include "ventanalongitud.h"
#include "ventanaarea.h"
#include "ventanavolumen.h"
#include "ventanatemperatura.h"
#include "ventanavelocidades.h"
#include "ventanatiempo.h"
#include "ventanapresion.h"
#include "ventanaenergias.h"


ventanaConversiones::ventanaConversiones(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversiones)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
    connect(ui->pushButton_3, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_3()));
    connect(ui->pushButton_4, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_4()));
    connect(ui->pushButton_5, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_5()));
    connect(ui->pushButton_6, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_6()));
    connect(ui->pushButton_7, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_7()));
    connect(ui->pushButton_8, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_8()));
    connect(ui->pushButton_9, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_9()));


}

void ventanaConversiones::on_pushButton_clicked()
{
    close();
}

void ventanaConversiones::on_pushButton_clicked_2()
{
    ventanaLongitud *ventanaLongitud = new class ventanaLongitud(); // Crea una nueva instancia de PhysicsWindow
    ventanaLongitud->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaLongitud->show(); // Muestra la ventana de física
}

void ventanaConversiones::on_pushButton_clicked_3()
{
    ventanaArea *ventanaArea = new class ventanaArea(); // Crea una nueva instancia de PhysicsWindow
    ventanaArea->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaArea->show(); // Muestra la ventana de física
}

void ventanaConversiones::on_pushButton_clicked_4()
{
    ventanaVolumen *ventanaVolumen = new class ventanaVolumen(); // Crea una nueva instancia de PhysicsWindow
    ventanaVolumen->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaVolumen->show(); // Muestra la ventana de física
}

void ventanaConversiones::on_pushButton_clicked_5()
{
    ventanaTemperatura *ventanaTemperatura = new class ventanaTemperatura(); // Crea una nueva instancia de PhysicsWindow
    ventanaTemperatura->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaTemperatura->show(); // Muestra la ventana de física
}

void ventanaConversiones::on_pushButton_clicked_6()
{
    ventanaVelocidades *ventanaVelocidades = new class ventanaVelocidades(); // Crea una nueva instancia de PhysicsWindow
    ventanaVelocidades->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaVelocidades->show(); // Muestra la ventana de física
}

void ventanaConversiones::on_pushButton_clicked_7()
{
    ventanaTiempo *ventanaTiempo = new class ventanaTiempo(); // Crea una nueva instancia de PhysicsWindow
    ventanaTiempo->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaTiempo->show(); // Muestra la ventana de física
}

void ventanaConversiones::on_pushButton_clicked_8()
{
    ventanaPresion *ventanaPresion = new class ventanaPresion(); // Crea una nueva instancia de PhysicsWindow
    ventanaPresion->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaPresion->show(); // Muestra la ventana de física
}

void ventanaConversiones::on_pushButton_clicked_9()
{
    ventanaEnergias *ventanaEnergias = new class ventanaEnergias(); // Crea una nueva instancia de PhysicsWindow
    ventanaEnergias->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaEnergias->show(); // Muestra la ventana de física
}

ventanaConversiones::~ventanaConversiones()
{
    delete ui;
}
