#ifndef VENTANACONVERSIONESMSAKMH_H
#define VENTANACONVERSIONESMSAKMH_H

#include <QWidget>

namespace Ui {
class ventanaConversionesMSAKmH;
}

class ventanaConversionesMSAKmH : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionesMSAKmH(QWidget *parent = nullptr);
    ~ventanaConversionesMSAKmH();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionesMSAKmH *ui;
};

#endif // VENTANACONVERSIONESMSAKMH_H
