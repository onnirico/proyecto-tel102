#include "ventanaconversionatmapa.h"
#include "ui_ventanaconversionatmapa.h"

ventanaConversionAtmAPa::ventanaConversionAtmAPa(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionAtmAPa)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionAtmAPa::on_pushButton_clicked()
{
    close();
}

void ventanaConversionAtmAPa::on_pushButton_clicked_2()
{
    // Obtiene el valor en atmósferas desde la QDoubleSpinBox
    double atmospheres = ui->doubleSpinBox->value();

    // Convierte atmósferas a pascales (1 atm = 101325 Pa)
    double pascals = atmospheres * 101325.0;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(pascals));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}


ventanaConversionAtmAPa::~ventanaConversionAtmAPa()
{
    delete ui;
}
