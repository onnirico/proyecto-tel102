#include "ventanaenergias.h"
#include "ui_ventanaenergias.h"
#include "ventanaconversionjacal.h"
#include "ventanaconversionjakj.h"
#include "ventanaconversionjaev.h"
#include "ventanaconversioncaj.h"
#include "ventanaconversionkcalaj.h"

ventanaEnergias::ventanaEnergias(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaEnergias)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
    connect(ui->pushButton_3, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_3()));
    connect(ui->pushButton_4, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_4()));
    connect(ui->pushButton_5, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_5()));
    connect(ui->pushButton_6, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_6()));
}

void ventanaEnergias::on_pushButton_clicked()
{
    close();
}

void ventanaEnergias::on_pushButton_clicked_2()
{
    ventanaConversionJACal *ventanaConversionJACal = new class ventanaConversionJACal(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionJACal->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionJACal->show(); // Muestra la ventana de física
}

void ventanaEnergias::on_pushButton_clicked_3()
{
    ventanaConversionJAKJ *ventanaConversionJAKJ = new class ventanaConversionJAKJ(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionJAKJ->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionJAKJ->show(); // Muestra la ventana de física
}

void ventanaEnergias::on_pushButton_clicked_4()
{
    ventanaConversionJAeV *ventanaConversionJAeV = new class ventanaConversionJAeV(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionJAeV->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionJAeV->show(); // Muestra la ventana de física
}

void ventanaEnergias::on_pushButton_clicked_5()
{
    ventanaConversionCAJ *ventanaConversionCAJ = new class ventanaConversionCAJ(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionCAJ->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionCAJ->show(); // Muestra la ventana de física
}

void ventanaEnergias::on_pushButton_clicked_6()
{
    ventanaConversionkCalAJ *ventanaConversionkCalAJ = new class ventanaConversionkCalAJ(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionkCalAJ->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionkCalAJ->show(); // Muestra la ventana de física
}

ventanaEnergias::~ventanaEnergias()
{
    delete ui;
}
