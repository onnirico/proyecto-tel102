#include "ventanaconversionesmillascuadradasakilometroscuadrados.h"
#include "ui_ventanaconversionesmillascuadradasakilometroscuadrados.h"

ventanaConversionesMillasCuadradasAKilometrosCuadrados::ventanaConversionesMillasCuadradasAKilometrosCuadrados(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionesMillasCuadradasAKilometrosCuadrados)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionesMillasCuadradasAKilometrosCuadrados::on_pushButton_clicked()
{
    close();
}

void ventanaConversionesMillasCuadradasAKilometrosCuadrados::on_pushButton_clicked_2()
{
    // Obtiene el valor en millas cuadradas desde la QDoubleSpinBox
    double millasCuadradas = ui->doubleSpinBox->value();

    // Convierte millas cuadradas a kilómetros cuadrados utilizando el factor de conversión (1 milla cuadrada = 2.58999 km^2)
    double kilometrosCuadrados = millasCuadradas * 2.58999;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(kilometrosCuadrados));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}



ventanaConversionesMillasCuadradasAKilometrosCuadrados::~ventanaConversionesMillasCuadradasAKilometrosCuadrados()
{
    delete ui;
}
