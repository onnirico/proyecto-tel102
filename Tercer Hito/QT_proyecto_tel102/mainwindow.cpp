#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "ventanaconversiones.h"
#include "ventanafisica.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Conectar la señal del botón con la función para cerrar la aplicación
    connect(ui->pushButton_3, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_3()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    close();
}

void MainWindow::on_pushButton_clicked_2()
{
    ventanaConversiones *ventanaConversiones = new class ventanaConversiones(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversiones->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversiones->show(); // Muestra la ventana de física
}

void MainWindow::on_pushButton_clicked_3()
{
    ventanaFisica *ventanaFisica = new class ventanaFisica(); // Crea una nueva instancia de PhysicsWindow
    ventanaFisica->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaFisica->show(); // Muestra la ventana de física
}

