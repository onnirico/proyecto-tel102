#include "ventanaconversionkmhaknots.h"
#include "ui_ventanaconversionkmhaknots.h"

ventanaConversionKmHAKnots::ventanaConversionKmHAKnots(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionKmHAKnots)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionKmHAKnots::on_pushButton_clicked()
{
    close();
}

void ventanaConversionKmHAKnots::on_pushButton_clicked_2()
{
    // Obtiene el valor en kilómetros por hora desde la QDoubleSpinBox
    double kmPerHour = ui->doubleSpinBox->value();

    // Convierte kilómetros por hora a nudos
    double knots = kmPerHour * 0.539957;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(knots));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}


ventanaConversionKmHAKnots::~ventanaConversionKmHAKnots()
{
    delete ui;
}
