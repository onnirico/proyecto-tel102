#include "ventanapresion.h"
#include "ui_ventanapresion.h"
#include "ventanaconversionpaaatm.h"
#include "ventanaconversionpaabar.h"
#include "ventanaconversionpaapsi.h"
#include "ventanaconversionpaammhg.h"
#include "ventanaconversionatmapa.h"
#include "ventanaconversionatmabar.h"
#include "ventanaconversionmmhgapa.h"

ventanaPresion::ventanaPresion(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaPresion)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
    connect(ui->pushButton_3, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_3()));
    connect(ui->pushButton_4, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_4()));
    connect(ui->pushButton_5, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_5()));
    connect(ui->pushButton_6, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_6()));
    connect(ui->pushButton_7, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_7()));
    connect(ui->pushButton_8, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_8()));
}

void ventanaPresion::on_pushButton_clicked()
{
    close();
}

void ventanaPresion::on_pushButton_clicked_2()
{
    ventanaConversionPaAAtm *ventanaConversionPaAAtm = new class ventanaConversionPaAAtm(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionPaAAtm->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionPaAAtm->show(); // Muestra la ventana de física
}

void ventanaPresion::on_pushButton_clicked_3()
{
    ventanaConversionPaABar *ventanaConversionPaABar = new class ventanaConversionPaABar(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionPaABar->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionPaABar->show(); // Muestra la ventana de física
}

void ventanaPresion::on_pushButton_clicked_4()
{
    ventanaConversionPaAPsi *ventanaConversionPaAPsi = new class ventanaConversionPaAPsi(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionPaAPsi->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionPaAPsi->show(); // Muestra la ventana de física
}

void ventanaPresion::on_pushButton_clicked_5()
{
    ventanaConversionPaAmmHg *ventanaConversionPaAmmHg = new class ventanaConversionPaAmmHg(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionPaAmmHg->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionPaAmmHg->show(); // Muestra la ventana de física
}

void ventanaPresion::on_pushButton_clicked_6()
{
    ventanaConversionAtmAPa *ventanaConversionAtmAPa = new class ventanaConversionAtmAPa(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionAtmAPa->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionAtmAPa->show(); // Muestra la ventana de física
}

void ventanaPresion::on_pushButton_clicked_7()
{
    ventanaConversionAtmABar *ventanaConversionAtmABar = new class ventanaConversionAtmABar(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionAtmABar->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionAtmABar->show(); // Muestra la ventana de física
}

void ventanaPresion::on_pushButton_clicked_8()
{
    ventanaConversionmmHgAPa *ventanaConversionmmHgAPa = new class ventanaConversionmmHgAPa(); // Crea una nueva instancia de PhysicsWindow
    ventanaConversionmmHgAPa->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    ventanaConversionmmHgAPa->show(); // Muestra la ventana de física
}

ventanaPresion::~ventanaPresion()
{
    delete ui;
}
