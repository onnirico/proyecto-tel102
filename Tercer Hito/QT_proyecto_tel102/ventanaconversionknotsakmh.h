#ifndef VENTANACONVERSIONKNOTSAKMH_H
#define VENTANACONVERSIONKNOTSAKMH_H

#include <QWidget>

namespace Ui {
class ventanaConversionKnotsAKmH;
}

class ventanaConversionKnotsAKmH : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionKnotsAKmH(QWidget *parent = nullptr);
    ~ventanaConversionKnotsAKmH();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionKnotsAKmH *ui;
};

#endif // VENTANACONVERSIONKNOTSAKMH_H
