#ifndef VENTANAVELOCIDADES_H
#define VENTANAVELOCIDADES_H

#include <QWidget>

namespace Ui {
class ventanaVelocidades;
}

class ventanaVelocidades : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaVelocidades(QWidget *parent = nullptr);
    ~ventanaVelocidades();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();
    void on_pushButton_clicked_3();
    void on_pushButton_clicked_4();
    void on_pushButton_clicked_5();
    void on_pushButton_clicked_6();
    void on_pushButton_clicked_7();
    void on_pushButton_clicked_8();
    void on_pushButton_clicked_9();


private:
    Ui::ventanaVelocidades *ui;
};

#endif // VENTANAVELOCIDADES_H
