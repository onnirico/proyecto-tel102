#include "ventanaconversionsemanasameses.h"
#include "ui_ventanaconversionsemanasameses.h"

ventanaConversionSemanasAMeses::ventanaConversionSemanasAMeses(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionSemanasAMeses)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionSemanasAMeses::on_pushButton_clicked()
{
    this->close(); // Cierra la ventana actual
}

void ventanaConversionSemanasAMeses::on_pushButton_clicked_2()
{
    // Obtiene el valor en semanas desde la QDoubleSpinBox
    double semanas = ui->doubleSpinBox->value();

    // Convierte semanas a meses (aproximadamente 1 mes = 4.34524 semanas)
    double meses = semanas / 4.34524;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(meses));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionSemanasAMeses::~ventanaConversionSemanasAMeses()
{
    delete ui;
}
