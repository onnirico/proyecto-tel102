#ifndef VENTANACONVERSIONPAAMMHG_H
#define VENTANACONVERSIONPAAMMHG_H

#include <QWidget>

namespace Ui {
class ventanaConversionPaAmmHg;
}

class ventanaConversionPaAmmHg : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionPaAmmHg(QWidget *parent = nullptr);
    ~ventanaConversionPaAmmHg();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionPaAmmHg *ui;
};

#endif // VENTANACONVERSIONPAAMMHG_H
