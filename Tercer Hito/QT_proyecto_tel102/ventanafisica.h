#ifndef VENTANAFISICA_H
#define VENTANAFISICA_H

#include <QWidget>

namespace Ui {
class ventanaFisica;
}

class ventanaFisica : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaFisica(QWidget *parent = nullptr);
    ~ventanaFisica();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();
    void on_pushButton_clicked_3();
    void on_pushButton_clicked_4();
    void on_pushButton_clicked_5();
    void on_pushButton_clicked_6();

private:
    Ui::ventanaFisica *ui;
};

#endif // VENTANAFISICA_H
