#include "ventanaconversionmililitrosaonzas.h"
#include "ui_ventanaconversionmililitrosaonzas.h"

ventanaConversionMililitrosAOnzas::ventanaConversionMililitrosAOnzas(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionMililitrosAOnzas)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionMililitrosAOnzas::on_pushButton_clicked()
{
    this->close(); // Cierra la ventana actual
}

void ventanaConversionMililitrosAOnzas::on_pushButton_clicked_2()
{
    // Obtiene el valor en mililitros desde la QDoubleSpinBox
    double mililitros = ui->doubleSpinBox->value();

    // Convierte mililitros a onzas utilizando el factor de conversión (1 mililitro ≈ 0.033814 onzas)
    double onzas = mililitros * 0.033814;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(onzas));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionMililitrosAOnzas::~ventanaConversionMililitrosAOnzas()
{
    delete ui;
}
