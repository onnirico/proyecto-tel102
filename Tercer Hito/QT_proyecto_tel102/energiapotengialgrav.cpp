#include "energiapotengialgrav.h"
#include "ui_energiapotengialgrav.h"

energiaPotengialGrav::energiaPotengialGrav(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::energiaPotengialGrav)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void energiaPotengialGrav::on_pushButton_clicked()
{
    close();
}

void energiaPotengialGrav::on_pushButton_clicked_2()
{
    // Obtiene el valor en kilogramos desde la QDoubleSpinBox
    double masa = ui->doubleSpinBox->value();

    // Obtiene el valor de la altura en metros desde la QDoubleSpinBox_2
    double altura = ui->doubleSpinBox_2->value();

    // Gravedad aproximada en la Tierra (9.8 m/s^2)
    const double gravedad = 9.8;

    // Calcula la energía potencial gravitatoria utilizando la fórmula E_p = mgh
    double energiaPotencial = masa * gravedad * altura;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(energiaPotencial));

    // Restablece los valores en las QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
    ui->doubleSpinBox_2->setValue(0.0);
}

energiaPotengialGrav::~energiaPotengialGrav()
{
    delete ui;
}
