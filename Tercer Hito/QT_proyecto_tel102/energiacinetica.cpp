#include "energiacinetica.h"
#include "ui_energiacinetica.h"

energiaCinetica::energiaCinetica(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::energiaCinetica)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void energiaCinetica::on_pushButton_clicked_2()
{
    // Obtiene el valor en kilogramos desde la QDoubleSpinBox
    double masa = ui->doubleSpinBox->value();

    // Obtiene el valor de la velocidad en metros por segundo desde la QDoubleSpinBox_2
    double velocidad = ui->doubleSpinBox_2->value();

    // Calcula la energía cinética utilizando la fórmula E_k = 1/2 * m * v^2
    double energiaCinetica = 0.5 * masa * velocidad * velocidad;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(energiaCinetica));

    // Restablece los valores en las QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
    ui->doubleSpinBox_2->setValue(0.0);
}

void energiaCinetica::on_pushButton_clicked()
{
    close();
}

energiaCinetica::~energiaCinetica()
{
    delete ui;
}

