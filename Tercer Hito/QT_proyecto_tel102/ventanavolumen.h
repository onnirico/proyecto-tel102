#ifndef VENTANAVOLUMEN_H
#define VENTANAVOLUMEN_H

#include <QWidget>

namespace Ui {
class ventanaVolumen;
}

class ventanaVolumen : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaVolumen(QWidget *parent = nullptr);
    ~ventanaVolumen();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();
    void on_pushButton_clicked_3();
    void on_pushButton_clicked_4();
    void on_pushButton_clicked_5();
    void on_pushButton_clicked_6();
    void on_pushButton_clicked_7();
    void on_pushButton_clicked_8();
    void on_pushButton_clicked_9();

private:
    Ui::ventanaVolumen *ui;
};

#endif // VENTANAVOLUMEN_H
