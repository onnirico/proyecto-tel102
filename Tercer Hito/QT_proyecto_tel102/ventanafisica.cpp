#include "ventanafisica.h"
#include "ui_ventanafisica.h"
#include "energiacinetica.h"
#include "energiapotengialgrav.h"
#include "energiapotencialelastica.h"
#include "energiamecanica.h"
#include "formulas.h"

ventanaFisica::ventanaFisica(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaFisica)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
    connect(ui->pushButton_3, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_3()));
    connect(ui->pushButton_4, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_4()));
    connect(ui->pushButton_5, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_5()));
    connect(ui->pushButton_6, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_6()));
}

void ventanaFisica::on_pushButton_clicked_2()
{
    energiaCinetica *energiaCinetica = new class energiaCinetica(); // Crea una nueva instancia de PhysicsWindow
    energiaCinetica->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    energiaCinetica->show(); // Muestra la ventana de física
}

void ventanaFisica::on_pushButton_clicked_3()
{
    energiaPotengialGrav *energiaPotengialGrav = new class energiaPotengialGrav(); // Crea una nueva instancia de PhysicsWindow
    energiaPotengialGrav->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    energiaPotengialGrav->show(); // Muestra la ventana de física
}

void ventanaFisica::on_pushButton_clicked_4()
{
    energiaPotencialElastica *energiaPotencialElastica = new class energiaPotencialElastica(); // Crea una nueva instancia de PhysicsWindow
    energiaPotencialElastica->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    energiaPotencialElastica->show(); // Muestra la ventana de física
}

void ventanaFisica::on_pushButton_clicked_5()
{
    energiaMecanica *energiaMecanica = new class energiaMecanica(); // Crea una nueva instancia de PhysicsWindow
    energiaMecanica->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    energiaMecanica->show(); // Muestra la ventana de física
}

void ventanaFisica::on_pushButton_clicked_6()
{
    formulas *formulas = new class formulas(); // Crea una nueva instancia de PhysicsWindow
    formulas->setAttribute(Qt::WA_DeleteOnClose); // Borra la instancia cuando se cierre la ventana
    formulas->show(); // Muestra la ventana de física
}

void ventanaFisica::on_pushButton_clicked()
{
    close();
}

ventanaFisica::~ventanaFisica()
{
    delete ui;
}
