#include "ventanaconversionaniosadias.h"
#include "ui_ventanaconversionaniosadias.h"

ventanaConversionAniosADias::ventanaConversionAniosADias(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionAniosADias)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionAniosADias::on_pushButton_clicked()
{
    close();
}

void ventanaConversionAniosADias::on_pushButton_clicked_2()
{
    // Obtiene el valor en años desde la QDoubleSpinBox
    double anios = ui->doubleSpinBox->value();

    // Convierte años a días (1 año = 365.25 días, teniendo en cuenta años bisiestos)
    double dias = anios * 365.25;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(dias));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

ventanaConversionAniosADias::~ventanaConversionAniosADias()
{
    delete ui;
}
