#include "ventanaconversiondiasasemanas.h"
#include "ui_ventanaconversiondiasasemanas.h"

ventanaConversionDiasASemanas::ventanaConversionDiasASemanas(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionDiasASemanas)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionDiasASemanas::on_pushButton_clicked()
{
    close();
}
void ventanaConversionDiasASemanas::on_pushButton_clicked_2()
{
    // Obtiene el valor en días desde la QDoubleSpinBox
    int dias = static_cast<int>(ui->doubleSpinBox->value());

    // Convierte días a semanas utilizando la función proporcionada
    double semanas = daysToWeeks(dias);

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(semanas));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}

double ventanaConversionDiasASemanas::daysToWeeks(int Valor)
{
    return static_cast<double>(Valor) / 7.0;
}

ventanaConversionDiasASemanas::~ventanaConversionDiasASemanas()
{
    delete ui;
}
