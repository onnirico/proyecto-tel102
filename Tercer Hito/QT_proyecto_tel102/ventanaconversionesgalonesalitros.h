#ifndef VENTANACONVERSIONESGALONESALITROS_H
#define VENTANACONVERSIONESGALONESALITROS_H

#include <QWidget>

namespace Ui {
class ventanaConversionesGalonesALitros;
}

class ventanaConversionesGalonesALitros : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionesGalonesALitros(QWidget *parent = nullptr);
    ~ventanaConversionesGalonesALitros();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionesGalonesALitros *ui;
};

#endif // VENTANACONVERSIONESGALONESALITROS_H
