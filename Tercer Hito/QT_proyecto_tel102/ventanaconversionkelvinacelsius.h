#ifndef VENTANACONVERSIONKELVINACELSIUS_H
#define VENTANACONVERSIONKELVINACELSIUS_H

#include <QWidget>

namespace Ui {
class ventanaConversionKelvinACelsius;
}

class ventanaConversionKelvinACelsius : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionKelvinACelsius(QWidget *parent = nullptr);
    ~ventanaConversionKelvinACelsius();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionKelvinACelsius *ui;
};

#endif // VENTANACONVERSIONKELVINACELSIUS_H
