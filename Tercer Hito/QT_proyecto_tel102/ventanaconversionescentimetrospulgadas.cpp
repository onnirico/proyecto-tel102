#include "ventanaconversionescentimetrospulgadas.h"
#include "ui_ventanaconversionescentimetrospulgadas.h"

ventanaConversionesCentimetrosPulgadas::ventanaConversionesCentimetrosPulgadas(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ventanaConversionesCentimetrosPulgadas)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked_2()));
}

void ventanaConversionesCentimetrosPulgadas::on_pushButton_clicked()
{
    close();
}

void ventanaConversionesCentimetrosPulgadas::on_pushButton_clicked_2()
{
    // Obtiene el valor en centímetros desde la QDoubleSpinBox
    double centimeters = ui->doubleSpinBox->value();

    // Convierte centímetros a pulgadas utilizando el factor de conversión (1 cm = 0.393701 pulgadas)
    double inches = centimeters * 0.393701;

    // Muestra el resultado en la etiqueta
    ui->label_7->setText(QString::number(inches));

    // Restablece el valor en la QDoubleSpinBox a cero
    ui->doubleSpinBox->setValue(0.0);
}



ventanaConversionesCentimetrosPulgadas::~ventanaConversionesCentimetrosPulgadas()
{
    delete ui;
}
