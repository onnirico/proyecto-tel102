#ifndef ENERGIAPOTENCIALELASTICA_H
#define ENERGIAPOTENCIALELASTICA_H

#include <QWidget>

namespace Ui {
class energiaPotencialElastica;
}

class energiaPotencialElastica : public QWidget
{
    Q_OBJECT

public:
    explicit energiaPotencialElastica(QWidget *parent = nullptr);
    ~energiaPotencialElastica();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::energiaPotencialElastica *ui;
};

#endif // ENERGIAPOTENCIALELASTICA_H
