#ifndef VENTANACONVERSIONESONZASAMILILITROS_H
#define VENTANACONVERSIONESONZASAMILILITROS_H

#include <QWidget>

namespace Ui {
class ventanaConversionesOnzasAMililitros;
}

class ventanaConversionesOnzasAMililitros : public QWidget
{
    Q_OBJECT

public:
    explicit ventanaConversionesOnzasAMililitros(QWidget *parent = nullptr);
    ~ventanaConversionesOnzasAMililitros();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_clicked_2();

private:
    Ui::ventanaConversionesOnzasAMililitros *ui;
};

#endif // VENTANACONVERSIONESONZASAMILILITROS_H
