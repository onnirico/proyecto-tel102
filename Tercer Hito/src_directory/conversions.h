#include <iostream>
#include <math.h> /*Necesaria para las ecuaciones matemáticas*/

/*Para una organización más efectiva del código principal del programa, se re - organizará en
una cabecera que contendrá las funciones de conversion para implementarse de manera modular*/

/*Submenú: Conversor de unidades*/

/*Conversión [1]*/

double metersToFoot(double Valor){
    const double footPerMeter = 3.28084;
    return Valor * footPerMeter;
}

double footToMeters(double Valor){
    const double metersPerFoot = 0.3048;
    return Valor * metersPerFoot;
}

double kilometersToMiles(double Valor){
    const double milesPerKilometer = 0.621371;
    return Valor * milesPerKilometer;
}

double milesToKilometers(double Valor){
    const double kilometersPerMiles = 1.60934;
    return Valor * kilometersPerMiles;
}

double centimeterToInches(double Valor){
    const double inchesPerCentimeters = 0.393701;
    return Valor * inchesPerCentimeters;
}

/*Conversión [2]*/

double squareMetersToSquarefoot(double Valor){
    const double squarefootPerSquareMeter = 10.7639;
    return Valor * squarefootPerSquareMeter;
}

double squarefootToSquareMeters(double Valor){
    const double squareMetersPerSquareFoot = 0.092903;
    return Valor * squareMetersPerSquareFoot;
}

double squareKilometersToAcre(double Valor){
    const double acresPerSquareKilometer = 247.105;
    return Valor * acresPerSquareKilometer;
}

double acreToHectare(double Valor){
    const double hectarePerAcre = 0.404686;
    return Valor * hectarePerAcre;
}

double squareMilesToSquareKilometers(double Valor){
    const double squareKilometersPerSquareMile = 2.58999;
    return Valor * squareKilometersPerSquareMile;
}

double squareInchesToSquareCentimeters(double Valor){
    const double squareCentimetersPerSquareInch = 6.4516;
    return Valor * squareCentimetersPerSquareInch;
}

double squareYardsToSquareMeters(double Valor){
    const double squareMetersPerSquareYard = 0.836127;
    return Valor * squareMetersPerSquareYard;
}

/*Conversión [3]*/

double literToMililiter(double Valor){
    const double mililitrosPorLitro = 1000.0;
    return Valor * mililitrosPorLitro;
}

double mililiterToLiter(double Valor){
    const double litersPerMililiter = 0.001;
    return Valor * litersPerMililiter;
}

double gallonsToLiters(double Valor){
    const double litersPerGallon = 3.78541;
    return Valor * litersPerGallon;
}

double litersToGallons(double Valor){
    const double gallonPerLitters = 0.264172;
    return Valor * gallonPerLitters;
}

double cubicFootToCubicMeter(double Valor){
    const double cubicMeterPerCubicFoot = 0.0283168;
    return Valor * cubicMeterPerCubicFoot;
}

double cubicMeterToCubicFoot(double Valor){
    const double cubicFootPerCubicMeter = 35.3147;
    return Valor * cubicFootPerCubicMeter;
}

double mililiterToOunce(double Valor){
    const double ouncesPerMililiter = 29.5735;
    return Valor * ouncesPerMililiter;
}

double ounceToMililiter(double Valor){
    const double mililitersPerOunce = 29.5735;
    return Valor * mililitersPerOunce;
}

/*Conversión [4]*/

double celsiusToFahrenheit(double Valor){
    return (Valor * 9.0 / 5.0) + 32.0;
}

double fahrenheitToCelcius(double Valor){
    return (Valor - 32.0) * 5.0 / 9.0;
}

double celsiusToKelvin(double Valor){
    return Valor + 273.15;
}

double kelvinToCelcius(double Valor){
    return Valor - 273.15;
}

double kelvinToFahrenheit(double Valor){
    return (Valor - 273.15) * 9/5 + 32;
}

double fahrenheitToKelvin(double Valor){
    return (Valor - 32) * 5/9 + 273.15;

}

/*Conversión [5]*/

double kilometerPerHourToMeterPerSecond(double Valor){
    return Valor * 1000 / 3600;
}

double meterPerSecondToKilometerPerHour(double Valor){
    return Valor * 3600 / 1000;
}

double kilometerPerHourToMilesPerHour(double Valor){
    return Valor / 1.60934;
}

double milesPerHourToKilometerPerHour(double Valor){
    return Valor * 1.60934;
}

double kilometerPerHourToKnots(double Valor){
    return Valor / 1.852;
}

double knotsToKilometerPerHour(double Valor){
    return Valor * 1.852;
}

double meterPerSecondToFeetPerSecond(double Valor){
    return Valor * 3.28084; 
}

double feetPerSecondToMeterPerSecond(double Valor){
    return Valor * 0.3048;
}

/*Conversión [6]*/

int hoursToMinutes(int Valor){
    return Valor * 60;
}

int minutesToSeconds(int Valor){
    return Valor* 60;
}

int daysToHours(int Valor){
    return Valor * 24;
}

int weeksToDays(int Valor){
    return Valor * 7;
}

int yearsToDays(int Valor){
    return Valor * 365;
}

int hoursToSeconds(int Valor){
    return Valor * 3600;
}

/*Problemas con unidades de conversión de tiempo [Solucionado]*/

long long minutesToMilliseconds(int Valor){ /*Utilizacón de long long para multiplicación*/
    return static_cast<long long>(Valor) * 60000; /*Perdida de datos de precisión. Uso de static_cast y long long (LL)*/
}

long long secondsToMilliseconds(int Valor){ /*Utilizacón de long long para multiplicación*/
    return static_cast<long long>(Valor) * 1000; /*Perdida de datos de precisión. Uso de static_cast y long long (LL)*/
}

double daysToWeeks(int Valor) {
    return static_cast<double>(Valor) / 7.0; /*Perdida de datos de precisión. Uso de static_cast y long long (LL)*/
}

double weeksToMonths(double Valor){
    return Valor / 4.34812;
}

double daysToYears(int Valor){
    return static_cast<double>(Valor) / 365.0; /*Perdida de datos de precisión. Uso de static_cast y long long (LL)*/
}

/*Fin Problemas de unidades de conversión de tiempo*/

/*Conversión [7]*/

double pascalesToAtmosphere(double Valor){
    return Valor * 0.00000986923;
}

double pascalesToBares(double Valor){
    return Valor * 0.00001;
}

double pascalesToPsi(double Valor){
    return Valor * 0.00014503773773375;
}

double pascalesToMmHg(double Valor){
    return Valor * 0.00750062;
}

double pascalesToKilopascales(double Valor){
    return Valor / 1000.0;
}

double atmosphereToPascales(double Valor){
    return Valor * 101325.0;
}

double atmosphereToBares(double Valor){
    return Valor * 1.01325;
}

double mmHgToPascales(double Valor){
    return Valor * 133.322;
}

/*Conversion [8]*/

double joulesToCalories(double Valor){
    const double factorConversion = 0.239006; 
    return Valor/factorConversion;
}

double joulesToKilojoules(double Valor) {
    return Valor / 1000.0;
}

double joulesToElectronvoltios(double Valor) {
    const double factorConversion = 6.242e12;
    return Valor * factorConversion;
}

double caloriesToJoules(double Valor) {
    const double factorConversion = 4.184;
    return Valor * factorConversion;
}

double kilocaloriesToJoules(double Valor) {
    const double factorConversion = 4184.0;
    return Valor * factorConversion;
}