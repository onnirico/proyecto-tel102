#include <iostream> // Necesaria para cout y cin
#include <string>   // Necesaria para las variables

using namespace std;

void mostrarRecuadro(int ancho, int alto) {
    for (int i = 0; i < alto; ++i) {
        for (int j = 0; j < ancho; ++j) {
            if (i == 0 || i == alto - 1) {
                if (j == 0 || j == ancho - 1) {
                    cout << '+';
                } else {
                    cout << '-';
                }
            } else {
                if (j == 0 || j == ancho - 1) {
                    cout << '|';
                } else {
                    cout << ' ';
                }
            }
        }
        cout << endl;
    }
}

char askContinue() {
    char continuar;

    cout << "¿Desea realizar otra conversión? (s/n): ";
    cin >> continuar;
    mostrarRecuadro(30, 1);

    return continuar;
}

char askContinueFisico() {
    char continuar;

    cout << "¿Desea realizar otro cálculo? (s/n): ";
    cin >> continuar;
    mostrarRecuadro(30, 1);

    return continuar;
}

char askContinueForm() {
    char continuar;

    cout << "¿Desea seguir en el menú físico? (s/n): ";
    cin >> continuar;
    mostrarRecuadro(30, 1);

    return continuar;
}