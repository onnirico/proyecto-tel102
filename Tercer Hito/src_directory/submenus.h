#include <iostream>
#include "conversions.h"
#include "style_sheet.h"
#include "calculator.h"

using namespace std;

int Conversion;

void submenuLength(){
    char continuar; /*Se creará esta variable char para poder ingresar la respuesta a la consulta del realizar otra conversión-*/

    do{
        int Opcion;
        double Valor;

        cout << "\nConversor de longitud\n" << endl;
        cout << "\t[0] Volver al menú principal" << endl;
        cout << "\t[1] Metros [m] a pies [ft]" << endl;
        cout << "\t[2] Pies [ft] a metros [m]" << endl;
        cout << "\t[3] Kilometros [km] a millas [mi]" << endl;
        cout << "\t[4] Millas [mi] a kilometros [km]" << endl;
        cout << "\t[5] Centimetros [cm] a pulgadas [in]\n" << endl;

        cout << "\nIngrese su Selección: ";
        cin >> Opcion;
        mostrarRecuadro(30, 1); 

        switch (Opcion){
            case 0:
                cout << "...Volviendo al \033[1;35mmenú principal\033[0m ...\n" << endl;
                break;

            case 1:
                cout << "\n\nIngrese metros [m]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m metros [m] serán \033[0m"
                << metersToFoot(Valor) << "\033[1;35m pies [ft].\033[0m" << endl;
                break;

            case 2:
                cout << "\n\nIngrese pies [ft]:" << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pies [ft] serán \033[0m"
                << footToMeters(Valor) << "\033[1;35m metros [m].\033[0m" << endl;
                break;

            case 3:
                cout << "\n\nIngrese Kilometros [km]:" << endl;
                cin >> Valor;
                cout << "\033[1;35m\n[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m kilometros [km] serán \033[0m"
                << kilometersToMiles(Valor) << "\033[1;35m millas [mi].\033[0m";
                break;

            case 4:
                cout << "\n\nIngrese Millas [mi]:" << endl;
                cin >> Valor;
                cout << "\033[1;35m\n[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m millas [mi] serán \033[0m"
                << milesToKilometers(Valor) << "\033[1;35m kilometros [km].\033[0m";
                break;

            case 5:
                cout << "\n\nIngrese Centimetros [cm]:" << endl;
                cin >> Valor;
                cout << "\033[1;35m\n[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m centimetros [cm] serán \033[0m"
                << milesToKilometers(Valor) << "\033[1;35m pulgadas [in].\033[0m";
                break;

            default:
                cout << "\n\nOpción no valida. Por favor ingrese una opción válida." << endl;
                break;
        }

        if (Opcion == 0){ /*Cuando la opción seleccionada sea cero, se volverá a la mainWindows*/
            return;
        }

        continuar = askContinue(); /*A continuar le entregaremos el valor retornado por la funcion askContinue()*/
    
    } while (continuar == 's' || continuar == 'S');
}

void submenuArea(){
    char continuar;

    do{
        int Opcion;
        double Valor;

        cout << "\nConversor de área\n" << endl;
        cout << "\t[0] Volver al menú principal" << endl;
        cout << "\t[1] Metros cuadrados [m²] a pies cuadrados [ft²]" << endl;
        cout << "\t[2] Pies cuadrados [ft²] a metros cuadrados [m²]" << endl;
        cout << "\t[3] Kilometros cuadrados [km²] a Acre [ac]" << endl;
        cout << "\t[4] Acres [ac] a hectáreas [ha]" << endl;
        cout << "\t[5] Millas cuadradas [mi²] a Kilometros cuadrados [km²]" << endl;
        cout << "\t[6] Pulgadas cuadradas [in²] a Centímetros cuadrados [cm²]" << endl;
        cout << "\t[7] Yardas cuadradas [yd²] a Metros cuadrados [m²]\n" << endl;

        cout << "\nIngrese su Selección: ";
        cin >> Opcion;

        mostrarRecuadro(30, 1); 

        switch (Opcion){
            case 0:
                cout << "...Volviendo al \033[1;35mmenú principal\033[0m ...\n" << endl;
                break;

            case 1:
                cout << "\n\nIngrese metros cuadrados [m²]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m metros cuadrados[m²] serán \033[0m"
                << squareMetersToSquarefoot(Valor) << "\033[1;35m pies cuadrados [ft²].\033[0m" << endl;
                break;

            case 2:
                cout << "\n\nIngrese pies cuadrados [ft²]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pies cuadrados [ft²] serán \033[0m"
                << squarefootToSquareMeters(Valor) << "\033[1;35m metros cuadrados [m²].\033[0m" << endl;
                break;

            case 3:
                cout << "\n\nIngrese Kilometros cuadrados [km²]: " << endl;
                cin >> Valor;
                cout << "\033[1;35m\n[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m kilometros cuadrados [km²] serán \033[0m"
                << squareKilometersToAcre(Valor) << "\033[1;35m acre [ac].\033[0m";
                break;

            case 4:
                cout << "\n\nIngrese Acre [ac]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m acre [ac] serán \033[0m"
                << acreToHectare(Valor) << "\033[1;35m hectareas [ha].\033[0m";
                break;

            case 5:
                cout << "\n\nIngrese millas cuadradas [mi²]:" << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m millas cuadradas [mi²] serán \033[0m"
                << squareMilesToSquareKilometers(Valor) << "\033[1;35m kilometros cuadrados [km²]].\033[0m";
                break;

            case 6:
                cout << "\n\nIngrese pulgadas cuadradas [in²]:" << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pulgadas cuadradas [in²] serán \033[0m"
                << squareInchesToSquareCentimeters(Valor) << "\033[1;35m centimetros cuadrados [cm²]].\033[0m";
                break;

            case 7:
                cout << "\n\nIngrese yardas cuadradas [yd²]:" << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m yardas cuadradas [yd²] serán \033[0m"
                << squareYardsToSquareMeters(Valor) << "\033[1;35m metros cuadrados [m²]].\033[0m";
                break;

            default:
                cout << "\n\nOpción no valida. Por favor ingrese una opción válida." << endl;
                break;
        }

        if (Opcion == 0){
            return;
        }

        continuar = askContinue();

    } while (continuar == 's' || continuar == 'S');
}

void submenuVolume(){
    char continuar;

    do{
        int Opcion;
        double Valor;

        cout << "\nConversor de Volumenes\n" << endl;
        cout << "\t[0] Volver al menú principal" << endl;
        cout << "\t[1] Litros [L] a mililitros [mL]" << endl;
        cout << "\t[2] Mililitros [L] a litros [mL]" << endl;
        cout << "\t[3] Galones [gal] a litros [L]" << endl;
        cout << "\t[4] Litros [L] a galones [gal]" << endl;
        cout << "\t[5] Pies cúbicos [ft³] a metros cúbicos [m³]" << endl;
        cout << "\t[6] Metro cúbico [m³] a pie cúbico [ft³]" << endl;
        cout << "\t[7] Mililitro [mL] a Onzas fluidas [fl oz]" << endl;
        cout << "\t[8] Onzas fluidas [fl oz] a mililitros [mL]\n" << endl;

        cout << "\nIngrese su Selección: ";
        cin >> Opcion;
        mostrarRecuadro(30, 1); 

        switch (Opcion) {
            case 0:
                cout << "...Volviendo al \033[1;35mmenú principal\033[0m ...\n" << endl;
                break;

            case 1:
                cout << "\n\nIngrese Litros [L]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m litros [L] serán \033[0m"
                << literToMililiter(Valor) << "\033[1;35m mililitros [mL].\033[0m" << endl;
                break;

            case 2:
                cout << "\n\nIngrese mililitros [mL]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m mililitros [mL] serán \033[0m"
                << mililiterToLiter(Valor) << "\033[1;35m litros [L].\033[0m" << endl;
                break;

            case 3:
                cout << "\n\nIngrese galones [gal]: "<< endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m galones [gal] serán \033[0m"
                << gallonsToLiters(Valor) << "\033[1;35m litros [L].\033[0m";
                break;

            case 4:
                cout << "\n\nIngrese litros [L]: "<< endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m litros [L] serán \033[0m"
                << litersToGallons(Valor) << "\033[1;35m galones [gal].\033[0m";
                break;

            case 5:
                cout << "\n\nIngrese pies cúbicos [ft³]: "<< endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pies cúbicos [ft³] serán \033[0m"
                << cubicFootToCubicMeter(Valor) << "\033[1;35m metros cúbicos[m³].\033[0m";
                break;

            case 6:
                cout << "\n\nIngrese metro cúbico [m³]:"<< endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m metro cúbico [m³] serán \033[0m"
                << cubicMeterToCubicFoot(Valor) << "\033[1;35m pie cúbico [ft³]].\033[0m";
                break;

            case 7:
                cout << "\n\nIngrese mililitro[mL]: "<< endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m mililitro [mL] serán \033[0m"
                << mililiterToOunce(Valor) << "\033[1;35m onzas fluidas [fl oz].\033[0m";
                break;

            case 8:
                cout << "\n\nIngrese onzas fluidas [fl oz]: "<< endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m onzas fluidas [fl oz] serán \033[0m"
                << ounceToMililiter(Valor) << "\033[1;35m mililitros [mL].\033[0m";
                break;

            default:
                cout << "\n\nOpción no valida. Por favor ingrese una opción válida." << endl;
                break;
        }

        if (Opcion == 0){
            return;
        }

        continuar = askContinue();

    } while (continuar == 's' || continuar == 'S');
}

void submenuTemperature(){
    char continuar;

    do{
        int Opcion;
        double Valor;

        cout << "\nConversor de Temperatura\n" << endl;
        cout << "\t[0] Volver al menú principal" << endl;
        cout << "\t[1] Celsius [Cº] a Fahrenheit [F]" << endl;
        cout << "\t[2] Fahrenheit [F] a Celsius [Cº]" << endl;
        cout << "\t[3] Celsius [C] a Kelvin [K]" << endl;
        cout << "\t[4] Kelvin [K] a Celsius [Cº]" << endl;
        cout << "\t[5] Fahrenheit [F] a Kelvin [K]" << endl;
        cout << "\t[6] Kelvin [K] a Fahrenheit [F]\n" << endl;

        cout << "\nIngrese su Selección: ";
        cin >> Opcion;
        mostrarRecuadro(30, 1); 

        switch(Opcion){
            case 0:
                cout << "...Volviendo al \033[1;35mmenú principal\033[0m ...\n" << endl;
                break;

            case 1:
                cout << "\n\nIngrese Tº en Celsius [Cº]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m celsius [Cº] serán \033[0m" 
                << celsiusToFahrenheit(Valor) << "\033[1;35m Fahrenheit [F].\033[0m" << endl;
                break;

            case 2:
                cout << "\n\nIngrese Tº en Fahrenheit [F]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m fahrenheit [F] serán \033[0m" 
                << fahrenheitToCelcius(Valor) << "\033[1;35m Celsius [Cº].\033[0m" << endl;
                break;

            case 3:
                cout << "\n\nIngrese Tº en Celsius [Cº]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m celsius [Cº] serán \033[0m" 
                << celsiusToKelvin(Valor) << "\033[1;35m Kelvin [K].\033[0m" << endl;
                break;

            case 4:
                cout << "\n\nIngrese Tº en Kelvin [K]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m Kelvin [K] serán \033[0m" 
                << kelvinToCelcius(Valor) << "\033[1;35m Celsius [Cº].\033[0m" << endl;
                break;

            case 5:
                cout << "\n\nIngrese Tº en Fahrenheit [F]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m fahrenheit [F] serán \033[0m" 
                << fahrenheitToKelvin(Valor) << "\033[1;35m Kelvin [K].\033[0m" << endl;
                break;

            case 6:
                cout << "\n\nIngrese Tº en Kelvin [K]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m Kelvin [K] serán \033[0m" 
                << kelvinToFahrenheit(Valor) << "\033[1;35m Fahrenheit [F].\033[0m" << endl;
                break;

            default:
                cout << "\n\nOpción no válida. Por favor ingrese una opción válida." << endl;
                break;
        
        }

        if (Opcion == 0){
            return;
        }

        continuar = askContinue();

    } while (continuar == 's' || continuar == 'S');
}

void submenuVelocity(){
    char continuar;

    do{
        int Opcion;
        double Valor;

        cout << "\nConversor de velocidades\n" << endl;
        cout << "\t[0] Volver al menú principal" << endl;
        cout << "\t[1] Kilómetros por hora [km/h] a Metros por segundo [m/s]" << endl;
        cout << "\t[2] Metros por segundo [m/s] a Kilómetros por hora [km/h]" << endl;
        cout << "\t[3] Kilómetros por hora [km/h] a Millas por hora [mph]" << endl;
        cout << "\t[4] Millas por hora [mph] a Kilómetros por hora [km/h]" << endl;
        cout << "\t[5] Kilómetros por hora [km/h] a Nudos [knots]" << endl;
        cout << "\t[6] Nudos [knots] a Kilmetros por hora [km/h]" << endl;
        cout << "\t[7] Metros por segundo [m/s] a Pies por segundo [ft/s]" << endl;
        cout << "\t[8] Pies por segundo [ft/s] a Metros por segundo [m/s]\n" << endl;

        cout << "\nIngrese su Selección: ";
        cin >> Opcion;
        mostrarRecuadro(30, 1); 

        switch(Opcion){
            case 1:
                cout << "\n\nIngrese Velocidad en Kilometro por hora [km/h]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m kilometros por hora [km/h] serán \033[0m" 
                << kilometerPerHourToKnots(Valor) << "\033[1;35m metros por segundo [m/s].\033[0m" << endl;
            break;

            case 2:
                cout << "\n\nIngrese Velocidad en metro por segundo [m/s]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m metros por segundo [m/s] serán \033[0m" 
                << meterPerSecondToKilometerPerHour(Valor) << "\033[1;35m kilometros por hora [km/h].\033[0m" << endl;
            break;


            case 3:
                cout << "\n\nIngrese Velocidad en Kilometro por hora [km/h]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m kilometros por hora [km/h] serán \033[0m" 
                << kilometerPerHourToMilesPerHour(Valor) << "\033[1;35m milla por hora [mph].\033[0m" << endl;
            break;

            case 4:
                cout << "\n\nIngrese Velocidad en millas por hora [mph]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m millas por hora [mph] serán \033[0m" 
                << milesPerHourToKilometerPerHour(Valor) << "\033[1;35m kilometros por hora [km/h].\033[0m" << endl;
            break;

            case 5:
                cout << "\n\nIngrese Velocidad en kilometros por hora [km/h]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m kilometros por hora [km/h] serán \033[0m" 
                << kilometerPerHourToKnots(Valor) << "\033[1;35m nudos [knot].\033[0m" << endl;
            break;

            case 6:
                cout << "\n\nIngrese Velocidad en nudos [knot]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m nudos [knot] serán \033[0m" 
                << knotsToKilometerPerHour(Valor) << "\033[1;35m kilometros por hora [km/h].\033[0m" << endl;
            break;

            case 7:
                cout << "\n\nIngrese Velocidad en metros por segundos [m/s]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m metros por segundos [m/s] serán \033[0m" 
                << meterPerSecondToFeetPerSecond(Valor) << "\033[1;35m pie por segundos [ft/s].\033[0m" << endl;
            break;

            case 8:
                cout << "\n\nIngrese Velocidad pie por segundos [ft/s]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pie por segundos [ft/s] serán \033[0m" 
                << feetPerSecondToMeterPerSecond(Valor) << "\033[1;35m metro por segundos [m/s].\033[0m" << endl;
            break;

            case 0:
                cout << "Volviendo al menú principal" << endl;
                return;

            default:
                cout << "\n\nOpción no valida. Por favor ingrese una opción válida." << endl;
            break;
        
        }

        if (Opcion == 0){
            return;
        }

        continuar = askContinue();

    } while (continuar == 's' || continuar == 'S');
}


void submenuTime(){
    char continuar;

    do{
        int Opcion;
        double Valor;

        cout << "\nConversor de Tiempo\n" << endl;
        cout << "\t[0] Volver al menú principal" << endl;
        cout << "\t[1] Horas [hrs] a minutos [min] " << endl;
        cout << "\t[2] Minutos [min] a segundos [seg]" << endl;
        cout << "\t[3] Días [días] a horas [hrs]" << endl;
        cout << "\t[4] Semanas [semanas] a días [días]" << endl;
        cout << "\t[5] Años [años] a días [días]" << endl;
        cout << "\t[6] Horas [hrs] a segundos [seg]" << endl;
        cout << "\t[7] Minutos [min] a milisegundos [ms]" << endl;
        cout << "\t[8] Segundos [s] a milisegundos [ms]" << endl;
        cout << "\t[9] días [días] a semanas [semanas]" << endl;
        cout << "\t[10] Semanas [semanas] a meses [meses]" << endl;
        cout << "\t[11] días [días] a años [años]" << endl;
    

        cout << "\nIngrese su Selección: ";
        cin >> Opcion;
        mostrarRecuadro(30, 1);

        switch(Opcion){
            case 1:
                cout << "\n\nIngrese las horas: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m horas [hrs] serán \033[0m" 
                << hoursToMinutes(Valor) << "\033[1;35m minutos [min].\033[0m" << endl;
            break;

            case 2:
                cout << "\n\nIngrese los minutos: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m minutos [min] serán \033[0m" 
                << minutesToSeconds(Valor) << "\033[1;35m segundos [seg].\033[0m" << endl;
            break;

            case 3:
                cout << "\n\nIngrese los días: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m días [días] serán \033[0m" 
                << daysToHours(Valor) << "\033[1;35m horas [hrs].\033[0m" << endl;
            break;

            case 4:
                cout << "\n\nIngrese los semanas: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m semanas [semanas] serán \033[0m" 
                << weeksToDays(Valor) << "\033[1;35m días [días].\033[0m" << endl;
            break;

            case 5:
                cout << "\n\nIngrese los años: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m años [años] serán \033[0m" 
                << yearsToDays(Valor) << "\033[1;35m días [días].\033[0m" << endl;
            break;

            case 6:
                cout << "\n\nIngrese las horas: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m horas [horas] serán \033[0m" 
                << hoursToSeconds(Valor) << "\033[1;35m segundos [segundos].\033[0m" << endl;
            break;

            case 7:
                cout << "\n\nIngrese los minutos: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m minutos [min] serán \033[0m" 
                << hoursToSeconds(Valor) << "\033[1;35m milisegundos [ms].\033[0m" << endl;
            break;

            case 8:
                cout << "\n\nIngrese las segundos: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m segundos [s] serán \033[0m" 
                << hoursToSeconds(Valor) << "\033[1;35m milisegundos [ms].\033[0m" << endl;
            break;

            case 9:
                cout << "\n\nIngrese las días: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m días [días] serán \033[0m" 
                << hoursToSeconds(Valor) << "\033[1;35m semanas [semanas].\033[0m" << endl;
            break;

            case 10:
                cout << "\n\nIngrese las semanas: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m semanas [semanas] serán \033[0m" 
                << hoursToSeconds(Valor) << "\033[1;35m meses [meses].\033[0m" << endl;
            break;

            case 11:
                cout << "\n\nIngrese las días: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m días [días] serán \033[0m" 
                << hoursToSeconds(Valor) << "\033[1;35m años [años].\033[0m" << endl;
            break;

            case 0:
                cout << "Volviendo al menú principal" << endl;
                return;

            default:
                cout << "\n\nOpción no valida. Por favor ingrese una opción válida." << endl;
            break;
        
        }

        if (Opcion == 0){
            return;
        }

        continuar = askContinue();

    } while (continuar == 's' || continuar == 'S');
}

void submenuPressure(){
    char continuar;

    do{
        int Opcion;
        double Valor;

        cout << "\nConversor de presión\n" << endl;
        cout << "\t[0] Volver al menú principal" << endl;
        cout << "\t[1] Pascales [Pa] a Atmosferas [atm]" << endl;
        cout << "\t[2] Pascales [Pa] a Bares [bar]" << endl;
        cout << "\t[3] Pascales [Pa] a Libra sobre pulgada cuadrada [psi]" << endl;
        cout << "\t[4] Pascales [Pa] a Milimetros de mercurio [mmHg]" << endl;
        cout << "\t[5] Atmosferas [atm] a Pascales [Pa]" << endl;
        cout << "\t[6] Atmosferas [Pa] a Bares [bar]" << endl;
        cout << "\t[7] Milimetros de mercurio [mmHg] a Pascales [Pa]\n" << endl;

        cout << "\nIngrese su Selección: ";
        cin >> Opcion;
        mostrarRecuadro(30, 1); 

        switch(Opcion){
            case 1:
                cout << "\n\nIngrese presión en Pascales [Pa]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pascales [Pa] serán \033[0m" 
                << pascalesToAtmosphere(Valor) << "\033[1;35m atmosferas [atm].\033[0m" << endl;
            break;

            case 2:
                cout << "\n\nIngrese presión en Pascales [Pa]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pascales [Pa] serán \033[0m" 
                << pascalesToBares(Valor) << "\033[1;35m bares [bar].\033[0m" << endl;
            break;

            case 3:
                cout << "\n\nIngrese presión en Pascales [Pa]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pascales [Pa] serán \033[0m" 
                << pascalesToPsi(Valor) << "\033[1;35m libras sobre pulgada cuadrada [psi].\033[0m" << endl;
            break;

            case 4:
                cout << "\n\nIngrese presión en Pascales [Pa]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pascales [Pa] serán \033[0m" 
                << pascalesToMmHg(Valor) << "\033[1;35m milimetros de mercurio [mmHg].\033[0m" << endl;
            break;

            case 5:
                cout << "\n\nIngrese presión en Atmosferas [atm]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m atmosferas [atm] serán \033[0m" 
                << atmosphereToPascales(Valor) << "\033[1;35m pascales [pas].\033[0m" << endl;
            break;

            case 6:
                cout << "\n\nIngrese presión en Atmosferas [atm]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pascales [Pa] serán \033[0m" 
                << atmosphereToBares(Valor) << "\033[1;35m bares [bar].\033[0m" << endl;
            break;

            case 7:
                cout << "\n\nIngrese presión en Milimetros de mercurio [mmHg]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m milimetros de mercurio [mmHg] serán \033[0m" 
                << mmHgToPascales(Valor) << "\033[1;35m pascales [Pa].\033[0m" << endl;
            break;

            case 0:
                cout << "Volviendo al menú principal" << endl;
                return;

            default:
                cout << "\n\nOpción no valida. Por favor ingrese una opción válida." << endl;
            break;
        
        }

        if (Opcion == 0){
            return;
        }

        if (Opcion == 0){
            return;
        }

        continuar = askContinue();

    } while (continuar == 's' || continuar == 'S');
}

void submenuEnergy(){
    char continuar;

    do{
        int Opcion;
        double Valor;

        cout << "\nConversor de Energía\n" << endl;
        cout << "\t[0] Volver al menú principal" << endl;
        cout << "\t[1] Julios [J] a Calorías [cal]" << endl;
        cout << "\t[2] Julios [J] a Kilojulios [KJ]" << endl;
        cout << "\t[3] Julios [J] a Electronvoltios [eV]" << endl;
        cout << "\t[4] Calorias [cal] a Julios [J]" << endl;
        cout << "\t[5] Kilocalorias [kCal] a Julios [J]\n" << endl;

        cout << "\nIngrese su Selección: ";
        cin >> Opcion;
        mostrarRecuadro(30, 1); 

        switch(Opcion){
            case 1:
                cout << "\n\nIngrese energía en Julios [J]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m julios [J] serán \033[0m" 
                << joulesToCalories(Valor) << "\033[1;35m calorias [cal].\033[0m" << endl;
                break;

            case 2:
                cout << "\n\nIngrese energía en Julios [J]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m julios [J] serán \033[0m" 
                << joulesToKilojoules(Valor) << "\033[1;35m kilojulios [KJ].\033[0m" << endl;
                break;

            case 3:
                cout << "\n\nIngrese energía en Julios [J]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m julios [J] serán \033[0m" 
                << joulesToElectronvoltios(Valor) << "\033[1;35m electronvoltios [eV].\033[0m" << endl;
                break;

            case 4:
                cout << "\n\nIngrese energía en calorias [cal]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m julios [J] serán \033[0m" 
                << caloriesToJoules(Valor) << "\033[1;35m julios [J].\033[0m" << endl;
                break;

            case 5:
                cout << "\n\nIngrese energía en kiloCalorias [kCal]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m kiloCalorias [kCal] serán \033[0m" 
                << kilocaloriesToJoules(Valor) << "\033[1;35m julios [J].\033[0m" << endl;
                break;

            case 0:
                cout << "Volviendo al menú principal" << endl;
                break;

            default:
                cout << "\n\nOpción no valida. Por favor ingrese una opción válida." << endl;
                break;
        
        }

        if (Opcion == 0){
            return;
        }

        continuar = askContinue();

    } while (continuar == 's' || continuar == 'S');
}

void menuConversion(){

    /* Option [1] Windows */

    cout << "\n\t\033[1;35m¡Bienvenido al Conversor de Unidades!\033[0m\n" <<    endl;
    cout << "Seleccione el menú de conversión a continuación:\n" << endl;
    
    cout << "\t[0] Volver al menú principal" << endl;
    cout << "\t[1] Conversión de Longitud" << endl;
    cout << "\t[2] Conversión de Area" << endl;
    cout << "\t[3] Conversión de Volúmen" << endl;
    cout << "\t[4] Conversión de Temperatura" << endl;
    cout << "\t[5] Conversión de Velocidad" << endl;
    cout << "\t[6] Conversión de Tiempo" << endl;
    cout << "\t[7] Conversión de Presión" << endl;
    cout << "\t[8] Conversión de Energía" << endl;

    cout << "\nIngrese su selección: ";
    cin >> Conversion;
    mostrarRecuadro(30, 1); 

    /*Functions Used*/

    switch(Conversion){ /*Llamados de todas las funciones creadas antes*/

        case 0:
            cout << "...Volviendo al \033[1;35mmenú principal\033[0m ..." << endl;
            break;

        case 1:
            submenuLength();
            break;

        case 2:
            submenuArea();
            break;

        case 3:
            submenuVolume();
            break;

        case 4:
            submenuTemperature();
            break;
        
        case 5:
            submenuVelocity();
            break;
        
        case 6:
            submenuTime();
            break;

        case 7:
            submenuPressure();
            break;

        case 8:
            submenuEnergy();
            break;

        default:
            cout << "\nOpción no válida. Intente de nuevo." << endl;
            break;

    cout << "...Volviendo al \033[1;35mmenú principal\033[0m ..." << endl;

    break;
    }
}


void menuFisico(){

    char respuesta;

    /* Option [2] Windows */

    cout << "\n\t\033[1;35m¡Bienvenido a la Calculadora de Energías!\033[0m\n" <<    endl;
    cout << "Seleccione el menú de energía a continuación:\n" << endl;
    
    cout << "\t[0] Volver al menú principal" << endl;
    cout << "\t[1] Energía Cinética" << endl;
    cout << "\t[2] Energía Potencial Gravitatoria" << endl;
    cout << "\t[3] Energía Potencial Elástica" << endl;
    cout << "\t[4] Energía Total de un Sistema" << endl;
    cout << "\t[5] Formularios de Energías" << endl;

    cout << "\nIngrese su selección: ";
    cin >> Conversion;
    mostrarRecuadro(30, 1); 

    /*Functions Used*/

    switch(Conversion){

        case 0:
            cout << "...Volviendo al \033[1;35mmenú principal\033[0m ..." << endl;
            break;

        case 1: /*Llamaremos a la clase y utilizaremos las funciones definidas dentro de esta para utilizarla en el código*/
            do{
                {
                    FormulaEnergiaCinetica calculadora;
                    cout << "\nCalculadora de Energía Cinética\n" << endl;
                    cout << "\nPor favor ingrese los datos solicitados a continuación en las \033[1;35munidades correspondientes.\033[0m\n" << endl;
                    calculadora.ingresarDato();
                    calculadora.mostrarResultado();
                    respuesta = askContinueFisico();

                };
            } while (respuesta == 's' || respuesta == 'S'); /*do-while se podrá romper al terminar de ingresar los datos*/
            break;

        case 2:
            do{
                {
                    FormulaEnergiaPotencialGravitatoria calculadora;
                    cout << "\nCalculadora de Energía Potenical Gravitatoria\n" << endl;
                    cout << "\nPor favor ingrese los datos solicitados a continuación en las \033[1;35munidades correspondientes.\033[0m\n" << endl;
                    calculadora.ingresarDato();
                    calculadora.mostrarResultado();
                    respuesta = askContinueFisico();

                };
            } while (respuesta == 's' || respuesta == 'S');
            break;

        case 3:
            do{
                {
                    FormulaEnergiaPotencialElastica calculadora;
                    cout << "\nCalculadora de Energía Potenical Elastica\n" << endl;
                    cout << "\nPor favor ingrese los datos solicitados a continuación en las \033[1;35munidades correspondientes.\033[0m\n" << endl;
                    calculadora.ingresarDato();
                    calculadora.mostrarResultado();
                    respuesta = askContinueFisico();

                };
            } while (respuesta == 's' || respuesta == 'S');
            break;

        case 4:
            do{
                {
                    CalculadoraEnergiaTotal calculadora;
                    cout << "\nCalculadora de Energía Total de un Sístema\n" << endl;
                    cout << "\nPor favor ingrese los datos solicitados a continuación en las \033[1;35munidades correspondientes.\033[0m\n" << endl;
                    calculadora.ingresarDatos();
                    calculadora.mostrarResultado();
                    respuesta = askContinueFisico();

                };
            } while (respuesta == 's' || respuesta == 'S');
            break;
        
        case 5: /* Solo imprimirá por pantalla los valores, pero quedará en el bucle si sigue seleccionando la opción de seguir.*/
            do{
                {
                    cout << "\nFormulario de Energías\n" << endl;
                    cout << "\nA cotninuación se mostrarán las ecuaciones de cada uno de los cáluclos que se pueden hacer de energía en el programa:\n" << endl;
                    cout << "\t\033[1;35mEnergía Cinetica [Ec]\033[0m = (1/2) * masa * velocidad², unidad: \033[1;35m[J]\033[0m\n"<< endl;
                    cout << "\t\033[1;35mEnergía Potencial Gravitatoria [Ep] \033[0m= masa * gravedad * altura, unidad: \033[1;35m[J]\033[0m\n"<< endl;
                    cout << "\t\033[1;35mEnergía Potencial Elastica [Ee] \033[0m= (1/2) * constante * deformación², unidad: \033[1;35m[J]\033[0m\n"<< endl;
                    cout << "\t\033[1;35mEnergía Total de un Sístema [Em] \033[0m= [Ec] + [Ep] + [Ee], unidad: \033[1;35m[J]\033[0m\n"<< endl;
                    respuesta = askContinueForm();

                };
            } while (respuesta == 's' || respuesta == 'S');
            break;

    }
}