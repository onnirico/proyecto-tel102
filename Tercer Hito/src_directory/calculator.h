#include <iostream>
#include <cmath> /*Necesaria para los cálculos físicos*/

using namespace std;

/*Submenú: Caculadora de Energías Físicas*/

/*Conversión [1]*/

class FormulaEnergiaCinetica{ /*Crearemos las clases con sus constructores correspondientes*/
    private:
        double masa; /*Haremos privadas a masa y a velocidad. Esto cambiará dependiendo de qué pida la fórmula*/
        double velocidad;
    
    public:
        FormulaEnergiaCinetica(){
            masa = 0.0; /*Seteamos en ceros en caso de no ingresarse*/
            velocidad = 0.0;

        }

        void ingresarDato(){ /*Ingresar los datos*/

            cout << "Ingrese la \033[1;35mmasa\033[0m del objeto en Kilogramos \033[1;35m[kg]\033[0m: " << endl;
            cin >> masa;

            cout << "Ingrese la \033[1;35mvelocidad\033[0m del objeto en metros por segundos \033[1;35m[m/s]\033[0m: " << endl; 
            cin >> velocidad;

        }

        double calcularEnergiaCinetica(){ /*Utilización de la formula*/

            return 0.5 * masa * pow(velocidad, 2);

        }

        void mostrarResultado(){ /*Mostrar datos*/

            double energiaCinetica = calcularEnergiaCinetica();
            cout << "La \033[1;35menergía cinética\033[0m del objeto es de " << energiaCinetica << " julios\n" << endl;

        }
};

/*Conversion [2]*/

class FormulaEnergiaPotencialGravitatoria{ /*Misma lógica seguida de las clases definidas*/
    private:
        double masa;
        double altura;
        long double gravedad;
    
    public:
        FormulaEnergiaPotencialGravitatoria(){
            masa = 0.0;
            altura = 0.0;
            gravedad = 9.80665;
        }

        void ingresarDato(){

            cout << "Ingrese la \033[1;35mmasa\033[0m del objeto en Kilogramos \033[1;35m[kg]\033[0m: " << endl;
            cin >> masa;

            cout << "Ingrese la \033[1;35maltura\033[0m del objeto en metros  \033[1;35m[m]\033[0m (revisar sistema de referencia): " << endl; 
            cin >> altura;

        }

        double calcularEnergiaPotenicalGravitacional(){

            return masa * gravedad * altura;

        }

        void mostrarResultado(){

            double energiapotencialgravitatoria = calcularEnergiaPotenicalGravitacional();
            cout << "La \033[1;35menergía potencial gravitatoria\033[0m del objeto es de " << energiapotencialgravitatoria << " julios\n" << endl;

        }
};

/*Conversión [3]*/

class FormulaEnergiaPotencialElastica{
    private:
        double cteElastica;
        double deformacion;
    
    public:
        FormulaEnergiaPotencialElastica(){
            cteElastica = 0.0;
            deformacion = 0.0;
        }

        void ingresarDato(){

            cout << "Ingrese la \033[1;35m constante elástica\033[0m del objeto\033[1;35m[kg]\033[0m: " << endl;
            cin >> cteElastica;

            cout << "Ingrese la \033[1;35mdeformación sufrida por el objeto\033[0m del objeto en metros\033[1;35m[m]\033[0m (revisar sistema de referencia): " << endl; 
            cin >> deformacion;

        }

        double calcularEnergiaPotenicalElastica(){

            return 0.5 * cteElastica * pow(deformacion, 2);

        }

        void mostrarResultado(){

            double energiapotencialelastica = calcularEnergiaPotenicalElastica();
            cout << "La \033[1;35menergía potencial elastica\033[0m del objeto es de " << energiapotencialelastica << " julios\n" << endl;

        }
};

/*Conversión [4]*/

class CalculadoraEnergiaTotal{
    private: /*Utilizaremos las clases anteriores para definir esta clase*/
        FormulaEnergiaCinetica cinetica;
        FormulaEnergiaPotencialGravitatoria gravitatoria;
        FormulaEnergiaPotencialElastica elastica;

    public:
        void ingresarDatos(){
            cout << "Cálculo de la energía total del sistema" << endl;
        
            cinetica.ingresarDato();/*Se ingresarán los datos de cada una de las clases para realizar su sumatoria*/
            gravitatoria.ingresarDato();
            elastica.ingresarDato();
        }   

        double calcularEnergiaTotal(){
            double energiaCinetica = cinetica.calcularEnergiaCinetica();
            double energiaPotencialGravitatoria = gravitatoria.calcularEnergiaPotenicalGravitacional();
            double energiaPotencialElastica = elastica.calcularEnergiaPotenicalElastica();

            return energiaCinetica + energiaPotencialGravitatoria + energiaPotencialElastica;
        }  

        void mostrarResultado(){
            double energiaTotal = calcularEnergiaTotal();
            cout << "La \033[1;35menergía total\033[0m del sistema es de " << energiaTotal << " julios" << endl;
        }
    };
