/*Volume Functions*/

double literToMililiter(double Valor){
    const double mililitrosPorLitro = 1000.0;
    return Valor * mililitrosPorLitro;
}

double mililiterToLiter(double Valor){
    const double litersPerMililiter = 0.001;
    return Valor * litersPerMililiter;
}

double gallonsToLiters(double Valor){
    const double litersPerGallon = 3.78541;
    return Valor * litersPerGallon;
}

double litersToGallons(double Valor){
    const double gallonPerLitters = 0.264172;
    return Valor * gallonPerLitters;
}

double cubicFootToCubicMeter(double Valor){
    const double cubicMeterPerCubicFoot = 0.0283168;
    return Valor * cubicMeterPerCubicFoot;
}

double cubicMeterToCubicFoot(double Valor){
    const double cubicFootPerCubicMeter = 35.3147;
    return Valor * cubicFootPerCubicMeter;
}

double mililiterToOunce(double Valor){
    const double ouncesPerMililiter = 29.5735;
    return Valor * ouncesPerMililiter;
}

double ounceToMililiter(double Valor){
    const double mililitersPerOunce = 29.5735;
    return Valor * mililitersPerOunce;
}

/*Volume Submenu*/

void submenuVolume(){
    char continuar;

    do{
        int Opcion;
        double Valor;

        cout << "\nConversor de Volumenes\n" << endl;
        cout << "\t[0] Volver al menú principal" << endl;
        cout << "\t[1] Litros [L] a mililitros [mL]" << endl;
        cout << "\t[2] Mililitros [L] a litros [mL]" << endl;
        cout << "\t[3] Galones [gal] a litros [L]" << endl;
        cout << "\t[4] Litros [L] a galones [gal]" << endl;
        cout << "\t[5] Pies cúbicos [ft³] a metros cúbicos [m³]" << endl;
        cout << "\t[6] Metro cúbico [m³] a pie cúbico [ft³]" << endl;
        cout << "\t[7] Mililitro [mL] a Onzas fluidas [fl oz]" << endl;
        cout << "\t[8] Onzas fluidas [fl oz] a mililitros [mL]\n" << endl;

        cout << "\nIngrese su Selección: ";
        cin >> Opcion;
        mostrarRecuadro(30, 1); 

        switch (Opcion) {
            case 0:
                cout << "...Volviendo al \033[1;35mmenú principal\033[0m ...\n" << endl;
                break;

            case 1:
                cout << "\n\nIngrese Litros [L]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m litros [L] serán \033[0m"
                << literToMililiter(Valor) << "\033[1;35m mililitros [mL].\033[0m" << endl;
                break;

            case 2:
                cout << "\n\nIngrese mililitros [mL]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m mililitros [mL] serán \033[0m"
                << mililiterToLiter(Valor) << "\033[1;35m litros [L].\033[0m" << endl;
                break;

            case 3:
                cout << "\n\nIngrese galones [gal]: "<< endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m galones [gal] serán \033[0m"
                << gallonsToLiters(Valor) << "\033[1;35m litros [L].\033[0m";
                break;

            case 4:
                cout << "\n\nIngrese litros [L]: "<< endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m litros [L] serán \033[0m"
                << litersToGallons(Valor) << "\033[1;35m galones [gal].\033[0m";
                break;

            case 5:
                cout << "\n\nIngrese pies cúbicos [ft³]: "<< endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pies cúbicos [ft³] serán \033[0m"
                << cubicFootToCubicMeter(Valor) << "\033[1;35m metros cúbicos[m³].\033[0m";
                break;

            case 6:
                cout << "\n\nIngrese metro cúbico [m³]:"<< endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m metro cúbico [m³] serán \033[0m"
                << cubicMeterToCubicFoot(Valor) << "\033[1;35m pie cúbico [ft³]].\033[0m";
                break;

            case 7:
                cout << "\n\nIngrese mililitro[mL]: "<< endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m mililitro [mL] serán \033[0m"
                << mililiterToOunce(Valor) << "\033[1;35m onzas fluidas [fl oz].\033[0m";
                break;

            case 8:
                cout << "\n\nIngrese onzas fluidas [fl oz]: "<< endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m onzas fluidas [fl oz] serán \033[0m"
                << ounceToMililiter(Valor) << "\033[1;35m mililitros [mL].\033[0m";
                break;

            default:
                cout << "\n\nOpción no valida. Por favor ingrese una opción válida." << endl;
                break;
        }

        if (Opcion == 0){
            return;
        }

        continuar = askContinue();

    } while (continuar == 's' || continuar == 'S');
}