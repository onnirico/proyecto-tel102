/*Continue Function*/

char askContinue(){
    char continuar; /*Creación del caracter para que se pueda ingresar como "S" o "s"*/
    
    cout << "¿Desea realizar otra conversión? (s/n): "; /*Consulta al usuario. En caso de decir no, el programa volverá a la ventana ppal. ("mainWindows en main.cpp")*/
    cin >> continuar;
    mostrarRecuadro(30, 1); /*Cada mostrarRecuadro() que se encuentra en el código estará ubicado después del ingreso de datos por usuario.
    Inicará un "cambio de ventana" en la idea final, con la entrega del Hito Nº3*/

    return continuar;
}

