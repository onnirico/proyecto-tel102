/*Pressure Submenu*/

double pascalesToAtmosphere(double Valor){
    return Valor * 0.00000986923;
}

double pascalesToBares(double Valor){
    return Valor * 0.00001;
}

double pascalesToPsi(double Valor){
    return Valor * 0.00014503773773375;
}

double pascalesToMmHg(double Valor){
    return Valor * 0.00750062;
}

double pascalesToKilopascales(double Valor){
    return Valor / 1000.0;
}

double atmosphereToPascales(double Valor) {
    return Valor * 101325.0;
}

double atmosphereToBares(double Valor) {
    return Valor * 1.01325;
}

double mmHgToPascales(double Valor) {
    return Valor * 133.322;
}

void submenuPressure(){
    char continuar;

    do{
        int Opcion;
        double Valor;

        cout << "\nConversor de longitud\n" << endl;
        cout << "\t[0] Volver al menú principal" << endl;
        cout << "\t[1] Pascales [Pa] a Atmosferas [atm]" << endl;
        cout << "\t[2] Pascales [Pa] a Bares [bar]" << endl;
        cout << "\t[3] Pascales [Pa] a Libra sobre pulgada cuadrada [psi]" << endl;
        cout << "\t[4] Pascales [Pa] a Milimetros de mercurio [mmHg]" << endl;
        cout << "\t[5] Atmosferas [atm] a Pascales [Pa]" << endl;
        cout << "\t[6] Atmosferas [Pa] a Bares [bar]" << endl;
        cout << "\t[7] Milimetros de mercurio [mmHg] a Pascales [Pa]\n" << endl;

        cout << "\nIngrese su Selección: ";
        cin >> Opcion;
        mostrarRecuadro(30, 1); 

        switch(Opcion){
            case 1:
                cout << "\n\nIngrese presión en Pascales [Pa]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pascales [Pa] serán \033[0m" 
                << pascalesToAtmosphere(Valor) << "\033[1;35m atmosferas [atm].\033[0m" << endl;
            break;

            case 2:
                cout << "\n\nIngrese presión en Pascales [Pa]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pascales [Pa] serán \033[0m" 
                << pascalesToBares(Valor) << "\033[1;35m bares [bar].\033[0m" << endl;
            break;

            case 3:
                cout << "\n\nIngrese presión en Pascales [Pa]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pascales [Pa] serán \033[0m" 
                << pascalesToPsi(Valor) << "\033[1;35m libras sobre pulgada cuadrada [psi].\033[0m" << endl;
            break;

            case 4:
                cout << "\n\nIngrese presión en Pascales [Pa]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pascales [Pa] serán \033[0m" 
                << pascalesToMmHg(Valor) << "\033[1;35m milimetros de mercurio [mmHg].\033[0m" << endl;
            break;

            case 5:
                cout << "\n\nIngrese presión en Atmosferas [atm]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m atmosferas [atm] serán \033[0m" 
                << atmosphereToPascales(Valor) << "\033[1;35m pascales [pas].\033[0m" << endl;
            break;

            case 6:
                cout << "\n\nIngrese presión en Atmosferas [atm]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pascales [Pa] serán \033[0m" 
                << atmosphereToBares(Valor) << "\033[1;35m bares [bar].\033[0m" << endl;
            break;

            case 7:
                cout << "\n\nIngrese presión en Milimetros de mercurio [mmHg]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m milimetros de mercurio [mmHg] serán \033[0m" 
                << mmHgToPascales(Valor) << "\033[1;35m pascales [Pa].\033[0m" << endl;
            break;

            case 0:
                cout << "Volviendo al menú principal" << endl;
                return;

            default:
                cout << "\n\nOpción no valida. Por favor ingrese una opción válida." << endl;
            break;
        
        }

        if (Opcion == 0){
            return;
        }

        if (Opcion == 0){
            return;
        }

        continuar = askContinue();

    } while (continuar == 's' || continuar == 'S');
}

/*Energy Functions*/

double joulesToCalories(double Valor){
    const double factorConversion = 0.239006; 
    return Valor/factorConversion;
}

double joulesToKilojoules(double Valor) {
    return Valor / 1000.0;
}

double joulesToElectronvoltios(double Valor) {
    const double factorConversion = 6.242e12;
    return Valor * factorConversion;
}

double caloriesToJoules(double Valor) {
    const double factorConversion = 4.184;
    return Valor * factorConversion;
}

double kilocaloriesToJoules(double Valor) {
    const double factorConversion = 4184.0;
    return Valor * factorConversion;
}