/*Area Functions*/

double squareMetersToSquarefoot(double Valor){
    const double squarefootPerSquareMeter = 10.7639;
    return Valor * squarefootPerSquareMeter;
}

double squarefootToSquareMeters(double Valor){
    const double squareMetersPerSquareFoot = 0.092903;
    return Valor * squareMetersPerSquareFoot;
}

double squareKilometersToAcre(double Valor){
    const double acresPerSquareKilometer = 247.105;
    return Valor * acresPerSquareKilometer;
}

double acreToHectare(double Valor){
    const double hectarePerAcre = 0.404686;
    return Valor * hectarePerAcre;
}

double squareMilesToSquareKilometers(double Valor) {
    const double squareKilometersPerSquareMile = 2.58999;
    return Valor * squareKilometersPerSquareMile;
}

double squareInchesToSquareCentimeters(double Valor) {
    const double squareCentimetersPerSquareInch = 6.4516;
    return Valor * squareCentimetersPerSquareInch;
}

double squareYardsToSquareMeters(double Valor) {
    const double squareMetersPerSquareYard = 0.836127;
    return Valor * squareMetersPerSquareYard;
}

/*Area Submenu*/

void submenuArea(){
    char continuar;

    do{
        int Opcion;
        double Valor;

        cout << "\nConversor de área\n" << endl;
        cout << "\t[0] Volver al menú principal" << endl;
        cout << "\t[1] Metros cuadrados [m²] a pies cuadrados [ft²]" << endl;
        cout << "\t[2] Pies cuadrados [ft²] a metros cuadrados [m²]" << endl;
        cout << "\t[3] Kilometros cuadrados [km²] a Acre [ac]" << endl;
        cout << "\t[4] Acres [ac] a hectáreas [ha]" << endl;
        cout << "\t[5] Millas cuadradas [mi²] a Kilometros cuadrados [km²]" << endl;
        cout << "\t[6] Pulgadas cuadradas [in²] a Centímetros cuadrados [cm²]" << endl;
        cout << "\t[7] Yardas cuadradas [yd²] a Metros cuadrados [m²]\n" << endl;

        cout << "\nIngrese su Selección: ";
        cin >> Opcion;

        mostrarRecuadro(30, 1); 

        switch (Opcion){
            case 0:
                cout << "...Volviendo al \033[1;35mmenú principal\033[0m ...\n" << endl;
                break;

            case 1:
                cout << "\n\nIngrese metros cuadrados [m²]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m metros cuadrados[m²] serán \033[0m"
                << squareMetersToSquarefoot(Valor) << "\033[1;35m pies cuadrados [ft²].\033[0m" << endl;
                break;

            case 2:
                cout << "\n\nIngrese pies cuadrados [ft²]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pies cuadrados [ft²] serán \033[0m"
                << squarefootToSquareMeters(Valor) << "\033[1;35m metros cuadrados [m²].\033[0m" << endl;
                break;

            case 3:
                cout << "\n\nIngrese Kilometros cuadrados [km²]: " << endl;
                cin >> Valor;
                cout << "\033[1;35m\n[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m kilometros cuadrados [km²] serán \033[0m"
                << squareKilometersToAcre(Valor) << "\033[1;35m acre [ac].\033[0m";
                break;

            case 4:
                cout << "\n\nIngrese Acre [ac]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m acre [ac] serán \033[0m"
                << acreToHectare(Valor) << "\033[1;35m hectareas [ha].\033[0m";
                break;

            case 5:
                cout << "\n\nIngrese millas cuadradas [mi²]:" << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m millas cuadradas [mi²] serán \033[0m"
                << squareMilesToSquareKilometers(Valor) << "\033[1;35m kilometros cuadrados [km²]].\033[0m";
                break;

            case 6:
                cout << "\n\nIngrese pulgadas cuadradas [in²]:" << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pulgadas cuadradas [in²] serán \033[0m"
                << squareInchesToSquareCentimeters(Valor) << "\033[1;35m centimetros cuadrados [cm²]].\033[0m";
                break;

            case 7:
                cout << "\n\nIngrese yardas cuadradas [yd²]:" << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m yardas cuadradas [yd²] serán \033[0m"
                << squareYardsToSquareMeters(Valor) << "\033[1;35m metros cuadrados [m²]].\033[0m";
                break;

            default:
                cout << "\n\nOpción no valida. Por favor ingrese una opción válida." << endl;
                break;
        }

        if (Opcion == 0){
            return;
        }

        continuar = askContinue();

    } while (continuar == 's' || continuar == 'S');
}