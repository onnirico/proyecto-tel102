/*Velocity Functions*/

double kilometerPerHourToMeterPerSecond(double Valor){
    return Valor * 1000 / 3600;
}

double meterPerSecondToKilometermPerHour(double Valor){
    return Valor * 3600 / 1000;
}

double kilometerPerHourToMilesPerHour(double Valor){
    return Valor / 1.60934;
}

double milesPerHourToKilometerPerHour(double Valor){
    return Valor * 1.60934;
}

double kilometerPerHourToKnots(double Valor){
    return Valor / 1.852;
}

double knotsToKilometerPerHour(double Valor){
    return Valor * 1.852;
}

double meterPerSecondToFeetPerSecond(double velocity) {
    return velocity * 3.28084;
}

double feetPerSecondToMeterPerSecond(double velocity) {
    return velocity * 0.3048;
}

void submenuVelocity(){
    char continuar;

    do{
        int Opcion;
        double Valor;

        cout << "\nConversor de Volumenes\n" << endl;
        cout << "\t[0] Volver al menú principal" << endl;
        cout << "\t[1] Kilómetros por hora [km/h] a Metros por segundo [m/s]" << endl;
        cout << "\t[2] Metros por segundo [m/s] a Kilómetros por hora [km/h]" << endl;
        cout << "\t[3] Kilómetros por hora [km/h] a Millas por hora [mph]" << endl;
        cout << "\t[4] Millas por hora [mph] a Kilómetros por hora [km/h]" << endl;
        cout << "\t[5] Kilómetros por hora [km/h] a Nudos [knots]" << endl;
        cout << "\t[6] Nudos [knots] a Kilmetros por hora [km/h]" << endl;
        cout << "\t[7] Metros por segundo [m/s] a Pies por segundo [ft/s]" << endl;
        cout << "\t[8] Pies por segundo [ft/s] a Metros por segundo [m/s]\n" << endl;

        cout << "\nIngrese su Selección: ";
        cin >> Opcion;
        mostrarRecuadro(30, 1); 

        switch(Opcion){
            case 1:
                cout << "\n\nIngrese Velocidad en Kilometro por hora [km/h]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m kilometros por hora [km/h] serán \033[0m" 
                << kilometerPerHourToKnots(Valor) << "\033[1;35m metros por segundo [m/s].\033[0m" << endl;
            break;

            case 2:
                cout << "\n\nIngrese Velocidad en metro por segundo [m/s]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m metros por segundo [m/s] serán \033[0m" 
                << meterPerSecondToKilometermPerHour(Valor) << "\033[1;35m kilometros por hora [km/h].\033[0m" << endl;
            break;


            case 3:
                cout << "\n\nIngrese Velocidad en Kilometro por hora [km/h]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m kilometros por hora [km/h] serán \033[0m" 
                << kilometerPerHourToMilesPerHour(Valor) << "\033[1;35m milla por hora [mph].\033[0m" << endl;
            break;

            case 4:
                cout << "\n\nIngrese Velocidad en millas por hora [mph]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m millas por hora [mph] serán \033[0m" 
                << milesPerHourToKilometerPerHour(Valor) << "\033[1;35m kilometros por hora [km/h].\033[0m" << endl;
            break;

            case 5:
                cout << "\n\nIngrese Velocidad en kilometros por hora [km/h]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m kilometros por hora [km/h] serán \033[0m" 
                << kilometerPerHourToKnots(Valor) << "\033[1;35m nudos [knot].\033[0m" << endl;
            break;

            case 6:
                cout << "\n\nIngrese Velocidad en nudos [knot]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m nudos [knot] serán \033[0m" 
                << knotsToKilometerPerHour(Valor) << "\033[1;35m kilometros por hora [km/h].\033[0m" << endl;
            break;

            case 7:
                cout << "\n\nIngrese Velocidad en metros por segundos [m/s]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m metros por segundos [m/s] serán \033[0m" 
                << meterPerSecondToFeetPerSecond(Valor) << "\033[1;35m pie por segundos [ft/s].\033[0m" << endl;
            break;

            case 8:
                cout << "\n\nIngrese Velocidad pie por segundos [ft/s]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pie por segundos [ft/s] serán \033[0m" 
                << feetPerSecondToMeterPerSecond(Valor) << "\033[1;35m metro por segundos [m/s].\033[0m" << endl;
            break;

            case 0:
                cout << "Volviendo al menú principal" << endl;
                return;

            default:
                cout << "\n\nOpción no valida. Por favor ingrese una opción válida." << endl;
            break;
        
        }

        if (Opcion == 0){
            return;
        }

        continuar = askContinue();

    } while (continuar == 's' || continuar == 'S');
}