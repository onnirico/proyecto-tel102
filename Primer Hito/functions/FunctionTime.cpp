/*Time Submenu*/

int hoursToMinutes(int Valor) {
    return Valor * 60;
}

int minutesToSeconds(int Valor) {
    return Valor* 60;
}

int daysToHours(int Valor) {
    return Valor * 24;
}

int weeksToDays(int Valor) {
    return Valor * 7;
}

int yearsToDays(int Valor) {
    return Valor * 365;
}

int hoursToSeconds(int Valor) {
    return Valor * 3600;
}

/*Problemas de precisión [Solucionado]*/

long long minutesToMilliseconds(int Valor) { /*Utilizacón de long long para multiplicación*/
    return static_cast<long long>(Valor) * 60000; /*Perdida de datos de precisión. Uso de static_cast y long long (LL)*/
}

long long secondsToMilliseconds(int Valor) { /*Utilizacón de long long para multiplicación*/
    return static_cast<long long>(Valor) * 1000; /*Perdida de datos de precisión. Uso de static_cast y long long (LL)*/
}

double daysToWeeks(int Valor) {
    return static_cast<double>(Valor) / 7.0; /*Perdida de datos de precisión. Uso de static_cast y long long (LL)*/
}

double weeksToMonths(double Valor) {
    return Valor / 4.34812;
}

double daysToYears(int Valor) {
    return static_cast<double>(Valor) / 365.0; /*Perdida de datos de precisión. Uso de static_cast y long long (LL)*/
}

/*------ Fin problemas de precisión ------*/

void submenuTime(){
    char continuar;

    do{
        int Opcion;
        double Valor;

        cout << "\nConversor de Tiempo\n" << endl;
        cout << "\t[0] Volver al menú principal" << endl;
        cout << "\t[1] Horas [hrs] a minutos [min] " << endl;
        cout << "\t[2] Minutos [min] a segundos [seg]" << endl;
        cout << "\t[3] Días [días] a horas [hrs]" << endl;
        cout << "\t[4] Semanas [semanas] a días [días]" << endl;
        cout << "\t[5] Años [años] a días [días]" << endl;
        cout << "\t[6] Horas [hrs] a segundos [seg]" << endl;
        cout << "\t[7] Minutos [min] a milisegundos [ms]" << endl;
        cout << "\t[8] Segundos [s] a milisegundos [ms]" << endl;
        cout << "\t[9] días [días] a semanas [semanas]" << endl;
        cout << "\t[10] Semanas [semanas] a meses [meses]" << endl;
        cout << "\t[11] días [días] a años [años]" << endl;
    

        cout << "\nIngrese su Selección: ";
        cin >> Opcion;
        mostrarRecuadro(30, 1);

        switch(Opcion){
            case 1:
                cout << "\n\nIngrese las horas: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m horas [hrs] serán \033[0m" 
                << hoursToMinutes(Valor) << "\033[1;35m minutos [min].\033[0m" << endl;
            break;

            case 2:
                cout << "\n\nIngrese los minutos: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m minutos [min] serán \033[0m" 
                << minutesToSeconds(Valor) << "\033[1;35m segundos [seg].\033[0m" << endl;
            break;

            case 3:
                cout << "\n\nIngrese los días: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m días [días] serán \033[0m" 
                << daysToHours(Valor) << "\033[1;35m horas [hrs].\033[0m" << endl;
            break;

            case 4:
                cout << "\n\nIngrese los semanas: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m semanas [semanas] serán \033[0m" 
                << weeksToDays(Valor) << "\033[1;35m días [días].\033[0m" << endl;
            break;

            case 5:
                cout << "\n\nIngrese los años: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m años [años] serán \033[0m" 
                << yearsToDays(Valor) << "\033[1;35m días [días].\033[0m" << endl;
            break;

            case 6:
                cout << "\n\nIngrese las horas: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m horas [horas] serán \033[0m" 
                << hoursToSeconds(Valor) << "\033[1;35m segundos [segundos].\033[0m" << endl;
            break;

            case 7:
                cout << "\n\nIngrese los minutos: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m minutos [min] serán \033[0m" 
                << hoursToSeconds(Valor) << "\033[1;35m milisegundos [ms].\033[0m" << endl;
            break;

            case 8:
                cout << "\n\nIngrese las segundos: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m segundos [s] serán \033[0m" 
                << hoursToSeconds(Valor) << "\033[1;35m milisegundos [ms].\033[0m" << endl;
            break;

            case 9:
                cout << "\n\nIngrese las días: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m días [días] serán \033[0m" 
                << hoursToSeconds(Valor) << "\033[1;35m semanas [semanas].\033[0m" << endl;
            break;

            case 10:
                cout << "\n\nIngrese las semanas: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m semanas [semanas] serán \033[0m" 
                << hoursToSeconds(Valor) << "\033[1;35m meses [meses].\033[0m" << endl;
            break;

            case 11:
                cout << "\n\nIngrese las días: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m días [días] serán \033[0m" 
                << hoursToSeconds(Valor) << "\033[1;35m años [años].\033[0m" << endl;
            break;

            case 0:
                cout << "Volviendo al menú principal" << endl;
                return;

            default:
                cout << "\n\nOpción no valida. Por favor ingrese una opción válida." << endl;
            break;
        
        }

        if (Opcion == 0){
            return;
        }

        continuar = askContinue();

    } while (continuar == 's' || continuar == 'S');
}