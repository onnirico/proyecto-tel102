void submenuEnergy(){
    char continuar;

    do{
        int Opcion;
        double Valor;

        cout << "\nConversor de longitud\n" << endl;
        cout << "\t[0] Volver al menú principal" << endl;
        cout << "\t[1] Julios [J] a Calorías [cal]" << endl;
        cout << "\t[2] Julios [J] a Kilojulios [KJ]" << endl;
        cout << "\t[3] Julios [J] a Electronvoltios [eV]" << endl;
        cout << "\t[4] Calorias [cal] a Julios [J]" << endl;
        cout << "\t[5] Kilocalorias [kCal] a Julios [J]\n" << endl;

        cout << "\nIngrese su Selección: ";
        cin >> Opcion;
        mostrarRecuadro(30, 1); 

        switch(Opcion){
            case 1:
                cout << "\n\nIngrese energía en Julios [J]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m julios [J] serán \033[0m" 
                << joulesToCalories(Valor) << "\033[1;35m calorias [cal].\033[0m" << endl;
                break;

            case 2:
                cout << "\n\nIngrese energía en Julios [J]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m julios [J] serán \033[0m" 
                << joulesToKilojoules(Valor) << "\033[1;35m kilojulios [KJ].\033[0m" << endl;
                break;

            case 3:
                cout << "\n\nIngrese energía en Julios [J]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m julios [J] serán \033[0m" 
                << joulesToElectronvoltios(Valor) << "\033[1;35m electronvoltios [eV].\033[0m" << endl;
                break;

            case 4:
                cout << "\n\nIngrese energía en calorias [cal]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m julios [J] serán \033[0m" 
                << caloriesToJoules(Valor) << "\033[1;35m julios [J].\033[0m" << endl;
                break;

            case 5:
                cout << "\n\nIngrese energía en kiloCalorias [kCal]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m kiloCalorias [kCal] serán \033[0m" 
                << kilocaloriesToJoules(Valor) << "\033[1;35m julios [J].\033[0m" << endl;
                break;

            case 0:
                cout << "Volviendo al menú principal" << endl;
                break;

            default:
                cout << "\n\nOpción no valida. Por favor ingrese una opción válida." << endl;
                break;
        
        }

        if (Opcion == 0){
            return;
        }

        continuar = askContinue();

    } while (continuar == 's' || continuar == 'S');
}