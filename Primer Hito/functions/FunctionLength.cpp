/*Length Functions.*/

/*Todas las funciones del código funcionarán bajo la misma lógica de interacción entre ventanas. Asimismo, las declaraciones
de sus funciones se realizarán acorde a las transformaciones, con un valor asociado que será el "coeficiente de trnasformación"
para cada uno de los casos. Los nombres de las funciones indican qué se está transformando.*/

double metersToFoot(double Valor){
    const double footPerMeter = 3.28084;
    return Valor * footPerMeter;
}

double footToMeters(double Valor){
    const double metersPerFoot = 0.3048;
    return Valor * metersPerFoot;
}

double kilometersToMiles(double Valor){
    const double milesPerKilometer = 0.621371;
    return Valor * milesPerKilometer;
}

double milesToKilometers(double Valor){
    const double kilometersPerMiles = 1.60934;
    return Valor * kilometersPerMiles;
}

double centimeterToInches(double Valor){
    const double inchesPerCentimeters = 0.393701;
    return Valor * inchesPerCentimeters;
}

/*Length Submenu*/

void submenuLength(){
    char continuar; /*Se creará esta variable char para poder ingresar la respuesta a la consulta del realizar otra conversión-*/

    do{
        int Opcion;
        double Valor;

        cout << "\nConversor de longitud\n" << endl;
        cout << "\t[0] Volver al menú principal" << endl;
        cout << "\t[1] Metros [m] a pies [ft]" << endl;
        cout << "\t[2] Pies [ft] a metros [m]" << endl;
        cout << "\t[3] Kilometros [km] a millas [mi]" << endl;
        cout << "\t[4] Millas [mi] a kilometros [km]" << endl;
        cout << "\t[5] Centimetros [cm] a pulgadas [in]\n" << endl;

        cout << "\nIngrese su Selección: ";
        cin >> Opcion;
        mostrarRecuadro(30, 1); 

        switch (Opcion){
            case 0:
                cout << "...Volviendo al \033[1;35mmenú principal\033[0m ...\n" << endl;
                break;

            case 1:
                cout << "\n\nIngrese metros [m]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m metros [m] serán \033[0m"
                << metersToFoot(Valor) << "\033[1;35m pies [ft].\033[0m" << endl;
                break;

            case 2:
                cout << "\n\nIngrese pies [ft]:" << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m pies [ft] serán \033[0m"
                << footToMeters(Valor) << "\033[1;35m metros [m].\033[0m" << endl;
                break;

            case 3:
                cout << "\n\nIngrese Kilometros [km]:" << endl;
                cin >> Valor;
                cout << "\033[1;35m\n[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m kilometros [km] serán \033[0m"
                << kilometersToMiles(Valor) << "\033[1;35m millas [mi].\033[0m";
                break;

            case 4:
                cout << "\n\nIngrese Millas [mi]:" << endl;
                cin >> Valor;
                cout << "\033[1;35m\n[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m millas [mi] serán \033[0m"
                << milesToKilometers(Valor) << "\033[1;35m kilometros [km].\033[0m";
                break;

            case 5:
                cout << "\n\nIngrese Centimetros [cm]:" << endl;
                cin >> Valor;
                cout << "\033[1;35m\n[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m centimetros [cm] serán \033[0m"
                << milesToKilometers(Valor) << "\033[1;35m pulgadas [in].\033[0m";
                break;

            default:
                cout << "\n\nOpción no valida. Por favor ingrese una opción válida." << endl;
                break;
        }

        if (Opcion == 0){ /*Cuando la opción seleccionada sea cero, se volverá a la mainWindows*/
            return;
        }

        continuar = askContinue(); /*A continuar le entregaremos el valor retornado por la funcion askContinue()*/
    
    } while (continuar == 's' || continuar == 'S');
}