/*Tempreature Functions*/

double celsiusToFahrenheit(double Valor){
    return (Valor * 9.0 / 5.0) + 32.0;
}

double fahrenheitToCelcius(double Valor) {
    return (Valor - 32.0) * 5.0 / 9.0;
}

double celsiusToKelvin(double Valor) {
    return Valor + 273.15;
}

double kelvinToCelcius(double Valor){
    return Valor - 273.15;
}

double kelvinToFahrenheit(double Valor){
    return (Valor - 273.15) * 9/5 + 32;
}

double fahrenheitToKelvin(double Valor){
    return (Valor - 32) * 5/9 + 273.15;

}

void submenuTemperature(){
    char continuar;

    do{
        int Opcion;
        double Valor;

        cout << "\nConversor de Volumenes\n" << endl;
        cout << "\t[0] Volver al menú principal" << endl;
        cout << "\t[1] Celsius [Cº] a Fahrenheit [F]" << endl;
        cout << "\t[2] Fahrenheit [F] a Celsius [Cº]" << endl;
        cout << "\t[3] Celsius [C] a Kelvin [K]" << endl;
        cout << "\t[4] Kelvin [K] a Celsius [Cº]" << endl;
        cout << "\t[5] Fahrenheit [F] a Kelvin [K]" << endl;
        cout << "\t[6] Kelvin [K] a Fahrenheit [F]\n" << endl;

        cout << "\nIngrese su Selección: ";
        cin >> Opcion;
        mostrarRecuadro(30, 1); 

        switch(Opcion){
            case 0:
                cout << "...Volviendo al \033[1;35mmenú principal\033[0m ...\n" << endl;
                break;

            case 1:
                cout << "\n\nIngrese Tº en Celsius [Cº]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m celsius [Cº] serán \033[0m" 
                << celsiusToFahrenheit(Valor) << "\033[1;35m Fahrenheit [F].\033[0m" << endl;
            break;

            case 2:
                cout << "\n\nIngrese Tº en Fahrenheit [F]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m fahrenheit [Fº] serán \033[0m" 
                << fahrenheitToCelcius(Valor) << "\033[1;35m Celsius [Cº].\033[0m" << endl;
            break;

            case 3:
                cout << "\n\nIngrese Tº en Celsius [Cº]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m celsius [Cº] serán \033[0m" 
                << celsiusToKelvin(Valor) << "\033[1;35m Kelvin [K].\033[0m" << endl;
            break;

            case 4:
                cout << "\n\nIngrese Tº en Kelvin [K]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m Klevin [K] serán \033[0m" 
                << kelvinToCelcius(Valor) << "\033[1;35m Celsius [Cº].\033[0m" << endl;
            break;

            case 5:
                cout << "\n\nIngrese Tº en Fahrenheit [F]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m fahrenheit [F] serán \033[0m" 
                << fahrenheitToKelvin(Valor) << "\033[1;35m Kelvin [K].\033[0m" << endl;
            break;

            case 6:
                cout << "\n\nIngrese Tº en Fahrenheit [F]: " << endl;
                cin >> Valor;
                cout << "\n\033[1;35m[CONVERSIÓN] = \033[0m\t" << Valor << "\033[1;35m celsius [Cº] serán \033[0m" 
                << fahrenheitToCelcius(Valor) << "\033[1;35m Celsius [Cº].\033[0m" << endl;
            break;

            default:
                cout << "\n\nOpción no valida. Por favor ingrese una opción válida." << endl;
            break;
        
        }

        if (Opcion == 0){
            return;
        }

        continuar = askContinue();

    } while (continuar == 's' || continuar == 'S');
}