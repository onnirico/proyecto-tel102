#include <iostream>
#include <math.h>
#include <string>


using namespace std;

int Conversion;

/*Print Design*/

void mostrarRecuadro(int ancho, int alto) {
    for (int i = 0; i < alto; ++i) {
        for (int j = 0; j < ancho; ++j) {
            if (i == 0 || i == alto - 1) {
                if (j == 0 || j == ancho - 1) {
                    cout << '+';
                } else {
                    cout << '-';
                }
            } else {
                if (j == 0 || j == ancho - 1) {
                    cout << '|';
                } else {
                    cout << ' ';
                }
            }
        }
        cout << endl;
    }
}

/*Error Declaration*/

void continueFunction(char &Continuar){
    cout << "\n\n¿Desea realizar otra conversión? (S/N): ";
    cin >> Continuar;
}

/*Length Functions. Fifth approximate decimal place*/

double metersToFeet(double Valor){
    const double feetPerMeter = 3.28084;
    return Valor * feetPerMeter;
}

double feetToMeters(double Valor){
    const double metersPerFoot = 0.3048;
    return Valor * metersPerFoot;
}

double kilometersToMiles(double Valor){
    const double milesPerKilometer = 0.621371;
    return Valor * milesPerKilometer;
}

double milesToKilometers(double Valor){
    const double kilometersPerMiles = 1.60934;
    return Valor * kilometersPerMiles;
}

double centimeterToInches(double Valor){
    const double inchesPerCentimeters = 0.393701;
    return Valor * inchesPerCentimeters;
}

void submenuLength(){
    char Continuar;

    do{
        int Opcion;
        double Valor;


        mostrarRecuadro(30, 1); 

        cout << "\nConversor de longitud\n" << endl;
        cout << "\t[1] Metros [m] a pies [ft]" << endl;
        cout << "\t[2] Pies [ft] a metros [m]" << endl;
        cout << "\t[3] Kilometros [km] a millas [mi]" << endl;
        cout << "\t[4] Millas [mi] a kilometros [km]" << endl;
        cout << "\t[5] Centimetros [cm] a pulgadas [in]]" << endl;
        /*cout << "\t[6] Volver al menú principal\n" << endl;*/

    
        mostrarRecuadro(30, 1); 

        cout << "\nSeleccione una opción:";
        cin >> Opcion;

        switch(Opcion){
            case 1:
                cout << "\n\nIngrese metros [m]:" << endl;
                cin >> Valor;
                cout << "\n[CONVERSIÓN] = \t" << Valor << " metros [m] serán " 
                << metersToFeet(Valor) << " pies [ft]." << endl;
            break;

            case 2:
                cout << "\n\nIngrese pies [ft]:" << endl;
                cin >> Valor;
                cout << "\n[CONVERSIÓN] = \t" << Valor << " pies [ft] serán " 
                << feetToMeters(Valor) << " metros [m]." << endl;
            break;

            case 3:
                cout << "\n\nIngrese Kilometros [km]:"<< endl;
                cin >> Valor;
                cout << "\n[CONVERSIÓN] = \t" << Valor << " kilometros [km] serán "
                << kilometersToMiles(Valor) << " millas [mi].";
            break;

            case 4:
                cout << "\n\nIngrese Millas [mi]:"<< endl;
                cin >> Valor;
                cout << "\n[CONVERSIÓN] = \t" << Valor << " millas [mi] serán "
                << milesToKilometers(Valor) << " kilometros [km].";
            break;

            case 5:
                cout << "\n\nIngrese Centimetros [cm]:"<< endl;
                cin >> Valor;
                cout << "\n[CONVERSIÓN] = \t" << Valor << " centimetros [cm] serán "
                << milesToKilometers(Valor) << " pulgadas [in].";
            break;

            /*case 6:
                cout << "Volviendo al menu anterior" << endl;
            return;*/

            default:
                cout << "\n\nOpción no valida. Por favor ingrese una opción válida." << endl;
            break;
        
        }

        continueFunction(Continuar);

    }  while (Continuar == 'S' || Continuar == 's');
}